﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlLater : MonoBehaviour {

	public void StartShow()
	{
		Invoke("Show", .2f);//Delay to show later animation
	}
	public void EndShow(bool iswin = true) //to hide later
	{
		if (!lt_obj_.gameObject.activeSelf)
			lt_obj_.gameObject.SetActive(true);
		if (iswin)
			lt_obj_.Play("Mission_Com");
		else
			lt_obj_.Play("Mission_Failed");
		//is_win_ = iswin;//define it win later or lose later
		//Vector3 pos = text_win_.transform.position;	//the position of target
		//ar_down_later_ = new List<GameObject>();//store the down line
		//ar_up_later_ = new List<GameObject>();	//store the top line
		//GameObject temp;						//store clone object
		//int add = 22;							//space of each later
		//string[] la = new string[7] { "M", "I", "S", "S", "I", "O", "N" };//store each later
		//for (int i = 0; i < la.Length; i++)	//use one by one later
		//{
		//	Vector3 tar = new Vector3(pos.x + add, pos.y, -3); // the postion of target to move for each later
		//	temp = GetTextClone(la[i]);	// get the One text from clone
		//	ar_up_later_.Add(temp);	// add to list Up line
		//	SetLaterMove(temp, tar); //Set later and position to move
		//	add += 4;				 //encrease space
		//	temp = null;			 //clear variable
		//}
		//pos.y = pos.y - 10;// Decrease for down line
		//add = 25;			// initailize first space position of x
		//if (iswin)	 //to show win later
		//{
		//	la = new string[8] { "C", "O", "M", "P", "L", "E", "T", "E" }; //store word up line
		//	pos.x = pos.x - 5;	//decrease x to align the text
		//}
		//else
		//{
		//	la = new string[6] { "F", "A", "I", "L", "E", "D" };//store word down line
		//	pos.x = pos.x - 1; //Decrease the x space
		//}
		//for (int i = 0; i < la.Length; i++)//get one by one of later
		//{
		//	Vector3 tar = new Vector3(pos.x + add, pos.y, -3);//generate position of target
		//	temp = GetTextClone(la[i]);//get clone of text gameobject
		//	ar_down_later_.Add(temp);//add to Down line	list
		//	SetLaterMove(temp, tar); //Set to move to target
		//	add += 4;  //Encrease space later
		//	temp = null;  //clear variable
		//}
	}
	public void HideLaterStart()//Hide later on First start Mission
	{
		lt_obj_.gameObject.SetActive(false);
		//StartCoroutine(HideDownLater());//to dalay to show each later
		//StartCoroutine(HideUpLater()); //to delay to hide each later
		//Invoke("DestroyLater", 1.5f);//to delay to destroy the Text later
	}
	private void DestroyLater()
	{
		foreach (GameObject g in ar_down_later_)//Destroy all later in Down line list
			Destroy(g);
		foreach (GameObject g in ar_up_later_) //Destroy all later in up line list
			Destroy(g);
		ar_up_later_ = null; //clear varialble
		ar_down_later_ = null; //clear variable
	}
	private IEnumerator HideDownLater()//to hide the downline list later
	{
		Vector3 tar = gameObject.transform.position;//the postion of each later to move to
		tar.x = tar.x + 30;
		tar.y = tar.y - 7;
		tar.z = tar.z - 3;
		foreach (GameObject g in ar_down_later_)//get one by one later in list down line
		{
			SetLaterMove(g, tar);//set to move to hide later from screen
			tar.z += 1;	//encrease target.z to make animation rotrate
			yield return new WaitForSeconds(.14f);//move one by one showing
		}
	}
	private IEnumerator HideUpLater()//to hide the upline list later
	{
		Vector3 tar = gameObject.transform.position;// theo position of each later to move to
		tar.x = tar.x + 30;
		tar.z = tar.z - 3;
		foreach (GameObject g in ar_up_later_) //get one by one later in list up line
		{
			SetLaterMove(g, tar);//set to move to hide later from screen
			tar.z += 1f; //encrease target.z to make animation in rotrate
			yield return new WaitForSeconds(.14f); //move on by one showing
		}
	}
	private void Show()	//Show start mission
	{
		if(!lt_obj_.gameObject.activeSelf)
		lt_obj_.gameObject.SetActive(true);
		lt_obj_.Play("Mission_Start");

		//Vector3 pos = text_win_.transform.position;	//the start position
		//ar_down_later_ = new List<GameObject>(); //list of downline later
		//ar_up_later_ = new List<GameObject>();	//list of upline later
		//GameObject temp;//for stor each later from clone
		//int add = 22; //space each later
		//string[] la = new string[7] { "M", "I", "S", "S", "I", "O", "N" };	//for use in loop calling
		//for (int i = 0; i < la.Length; i++)	//get one by one later
		//{
		//	Vector3 tar = new Vector3(pos.x + add, pos.y, -3); //generate the target position
		//	temp =GetTextClone(la[i]); //get Text from clone gameobject
		//	ar_up_later_.Add(temp);	   // add to list upline
		//	SetLaterMove(temp, tar);	//Set to move later
		//	add += 4;					//encrease space
		//	temp = null;				//clear the variable
		//}
		//pos.y = pos.y - 10;				//decrease postion.y for downline
		//add = 25;						//initialize start space position.x
		//la = new string[5] { "S", "T", "A", "R", "T" };//for use in loop calling
		//for (int i = 0; i < la.Length; i++)	//get the one by one of each later
		//{
		//	Vector3 tar = new Vector3(pos.x + add, pos.y, -3);//generate the target position
		//	temp = GetTextClone(la[i]);//get clone text object
		//	ar_down_later_.Add(temp); //add to downline list
		//	SetLaterMove(temp, tar);  //Set to move later
		//	add += 4; //encrease spacing position
		//	temp = null;	 //clear variable
		//}
	}
	private GameObject GetTextClone(string s)//give the clone text gameobject
	{
		tk2dTextMesh t;
		if(is_win_)
			t = (tk2dTextMesh)Instantiate(text_win_); //give the win style later
		else
			t = (tk2dTextMesh)Instantiate(text_lose_);//give the lose style later
		t.text = s;	//set text
		t.Commit();	//Commit text
		t.gameObject.transform.position = text_win_.transform.position;//set position
		t.transform.parent = gameObject.transform;//set parent
		return t.gameObject;//get
	}
	private void SetLaterMove(GameObject g,Vector3 target) //Move later by using iTween
	{
		iTween.MoveTo(g, iTween.Hash("position", target
									, "speed", 50
									, "easetype", iTween.EaseType.linear));
	}
	#region Variable
	[SerializeField]
	private tk2dTextMesh text_win_ = null; //template Text in Win style
	[SerializeField]
	private tk2dTextMesh text_lose_ = null;	//template text in lose style
	private List<GameObject> ar_up_later_;	// the list store Upper line latter
	private List<GameObject> ar_down_later_;// the list store Lowwer line latter
	private bool is_win_ = true;			// to generate later style

	[SerializeField]
	private tk2dSpriteAnimator lt_obj_ = null;
	#endregion
}
