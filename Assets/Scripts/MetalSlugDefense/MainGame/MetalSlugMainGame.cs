﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YNinja.MetalSlug
{
	public class MetalSlugMainGame : MonoBehaviour
	{
		#region Soilder Control
        void Awake()
        {
            total_ap_ = 100; //initialize the limited of ap
            current_ap_ = 0; //set start value of ap
        }
        // Use this for initialization
        void Start()
        {
            Initialize();//to initialize the Sound and Camera controling
            if (bgname_ != "")
            {
                ChangeBackground(bgname_);
            }
			pause_pos_ = pause_box_.transform.localPosition;
			pause_bt_.SetActive(false);
			obj_lv_pos_ = lv_up_.transform.position;
        }
        void OnEnable()
        {
            ClickControl.OnClick += OnClickButton;//Deleget function of click event
        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton; //Deleget function of click event
        }
		// Update is called once per frame
		void Update()
		{

			if (!is_end_) //in playing state
			{
				IncreaseAp(); //Encrease the Ap one by one
				if (is_danger_)
				{
					BlinkLife();
				}
			}
			else if (is_count_up_fund_)
			{
				int fun = int.Parse(text_found_fund_.text);
				if (fun <= total_reward_found_ - 25)//1000 - 25)
				{
					fun += 25;
					text_found_fund_.text = "" + fun;
				}
				else
				{
					text_found_fund_.text = "" + total_reward_found_;
					is_count_up_fund_ = false;
				}
			}
			if (Input.GetMouseButtonDown(0)) //on mouse click
			{
				Transform hit = GeneralSystem.ClickOnObject(Camera.main); //Get Item from click
				if (hit != null && hit.transform.name == "Soldier")	 //Click on Soldier to make skill
				{
					ClickOnSoldier(hit);//Calling event
				}
			}
			else if (Input.GetKeyDown(KeyCode.Escape))
			{
				GetEscKey();
			}
			
		}
        void LateUpdate()
        {
            if (isdrag)
            {
                CheckDragLimited();//check the limited of drag position
            }
        }
        public void ShowDialog(Side side)//Show dialog after end battle
		{
			if (!is_end_)//to call in only one time
			{
				//string s=""; //store the statement for show in dialog
				bool iswin = true;
                switch (side)
                {
                    case Side.PLAYER: //is player calling
                        {
							SaveData();//Encrease fund and save
                            FinishMission(true,"MISSION COMPLETE", enemy_soldier_, player_soldier_);//Store statement
							if (is_boss_scene_)//is boss scene or normal scene
                                BossControl.instance.Dead(); //make Boss to die
							iswin = true;
							break;
                        }
                    case Side.ENEMY: //is enemy calling
                        {
                            FinishMission(false,"MISSION FAILED", player_soldier_, enemy_soldier_);//Store statement
							iswin = false;
							break;
                        }
                }
				is_end_ = true;	//Disable this function calling in second time
				StartCoroutine(ShowCompleteMission(iswin));//Show and Hide Later Complete
				DisableSkill();//to disable the specail skill attack of player soldier
				pause_bt_.SetActive(false); // Diable Pause
				if (bt_spedd_on.activeSelf) bt_spedd_on.SetActive(false);
				if (bt_speed_off.activeSelf) bt_speed_off.SetActive(false);
				if(blink_life_.gameObject.activeSelf)
					blink_life_.gameObject.SetActive(false);
			}
		}
		public void ShowDanger()
		{
			if (!dager_.activeSelf)
			{
				dager_.transform.position = obj_lv_pos_;
				iTween.MoveFrom(dager_, iTween.Hash("x", 10, "speed", 70, "easetype", iTween.EaseType.linear, "oncomplete", "HideDanger", "oncompletetarget", gameObject));
				dager_.SetActive(true);
				is_danger_ = true;
				if (!blink_life_.gameObject.activeSelf)
					blink_life_.gameObject.SetActive(true);
			}
		}
		private void ShoWindowComplete(bool iswin)
		{
			TextMesh text;
			text = com_dialog_.transform.GetChild(1).GetComponent<TextMesh>();
			GameObject prisoner = com_dialog_.transform.GetChild(5).gameObject;
			string s;
			if (iswin)
			{
				s = "MISSION COMPLETE";
				is_count_up_fund_ = true;
				prisoner.GetComponent<tk2dSpriteAnimator>().Play("Hyakutaro_Thank");
			}
			else
			{
				s = "MISSION FAILED";
				text.color = Color.red;
				prisoner.GetComponent<tk2dSpriteAnimator>().Play();
				prisoner.GetComponent<tk2dSprite>().color = Color.gray;
			}
			text.text = s;
			text_found_fund_ = com_dialog_.transform.GetChild(2).GetComponent<TextMesh>();
			text_found_fund_.text = "" + 0;
			AnimationSystem.ScaleButton(com_dialog_.transform.GetChild(3).gameObject);
			AnimationSystem.ScaleButton(com_dialog_.transform.GetChild(4).gameObject);
		}
		
		/// <summary>
		/// To generate the level of Enemy
		/// </summary>
		private void GenerateLevel()
		{
			LevelControl lvcontrol = new LevelControl(AreaSelectControl.area_index_, StageControl.stage_num_);
			if (!lvcontrol.EnemyDescription.IsBoss)
			{
				enemy_name_ = lvcontrol.EnemyDescription.TypeSoldier;//new string[3] { "Marco","Fio","Eri" };
				float[] time = lvcontrol.EnemyDescription.SoldierInterval;//new float[3] { 15, 22, 8 };
				int[] hp = lvcontrol.EnemyDescription.SoldierHp;//new int[3]{1,2,3};
				for (int i = 0; i < enemy_name_.Length; i++)
				{
					if (enemy_name_[i] != "Tank")
					{
						StartCoroutine(InitializeEnemy(enemy_name_[i], time[i], hp[i]));
					}
					else
					{
						tank_time_interwal_ = lvcontrol.EnemyDescription.SoldierInterval[i];
						Invoke("GenerateTankEnemy", tank_time_interwal_);
					}
				}
			}
			else
			{
				BossControl.instance.time_interwal_ = lvcontrol.EnemyDescription.BossInterval;
				enemy_home_.GetComponent<BossBase>().SetHealth(lvcontrol.EnemyDescription.BossHp);
			}
			//LevelControl lvcontrol;
			//lvcontrol = new LevelControl(AreaSelectControl.area_index_, StageControl.stage_num_);
			//if (lvcontrol.IsShowEnemy) //to enable the Enemy soldier
			//{
			//	enemy_time_interwal_ = lvcontrol.EnemyTimeInterwal;
			//	Invoke("GenerateEnemySoldier", enemy_time_interwal_);//Generate the Enemy solider when Normal Battle
			//}
			//if (lvcontrol.IsShowTank) // to enable the tank of soldier
			//{
			//	tank_time_interwal_ = lvcontrol.TankTimeInterwal;
			//	Invoke("GenerateTankEnemy", tank_time_interwal_);
			//}
			//if (lvcontrol.IsShowBoss)
			//{
			//	BossControl.instance.time_interwal_ = lvcontrol.BossTimeInterwal;
			//}
		}
		private IEnumerator InitializeEnemy(string name, float timeinterwal,int hp)
		{
			if (!is_end_)
			{
				yield return new WaitForSeconds(timeinterwal);
				if (!is_end_)
				{
					if (enemy_home_ != null && player_home_ != null && !enemy_home_.GetComponent<SoldierHome>().IsDie())
					{
						CreateEnemy(name, hp);
						StartCoroutine(InitializeEnemy(name, timeinterwal, hp));
					}
				}
			}
		}
		private void CreateEnemy(string name,int hp)
		{
			Debug.Log(name);
			GameObject temp = InstancePrefab("Enemy", enemy_home_);
			Vector3 p_ps = temp.transform.position;
			p_ps = new Vector3(p_ps.x, p_ps.y - 1, p_ps.z - 1);
			temp.transform.position = p_ps;
			temp.AddComponent<SoldierControl>();
			if (enemy_soldier_ == null) enemy_soldier_ = new List<GameObject>();
			enemy_soldier_.Add(temp);
			p_ps.x = player_home_.transform.position.x;
			SoldierType type=SoldierType.GetType(name,true);
			type.Health=hp;
			if (name == "Sarubia")
			{
				Vector3 ps = temp.transform.position;
				ps.y = ps.y + 2.8f;
				p_ps.y = ps.y;
				p_ps.z = ps.z + 1;
				temp.transform.position = ps;
				Destroy(temp.GetComponent<SkillControl>());
				Destroy(temp.GetComponent<BoxCollider2D>());
				//temp.transform.GetChild(1).gameObject.SetActive(false);
				//temp.transform.GetChild(2).gameObject.SetActive(false);
				ps = temp.transform.GetChild(0).transform.position;
				ps.y += 2.5f;
				temp.transform.GetChild(0).transform.position = ps;
			}
			temp.GetComponent<SoldierControl>().SetToMove(p_ps,type, Side.ENEMY);
			
		}
		private void SaveData()
		{
			//DataSaving.SaveFund(DataSaving.LoadFund() + 1000);
            DataSavingXml data = DataSavingXml.Instance();
			total_reward_found_ = 500 * (StageControl.stage_num_ + 1);
			data.fund += total_reward_found_;
            bool[] isclear;
			int stagenum = StageControl.stage_num_;
			int areanum = AreaSelectControl.area_index_;
			isclear = data.areas.area[areanum].savedstage;
			isclear[stagenum] = true;
			data.areas.area[areanum].savedstage = isclear;
			//if (is_boss_scene_)
			//{
			//	//isclear = DataSaving.LoadStage2();
			//	isclear = data.areas.area[1].savedstage;
			//	isclear[stagenum] = true;
			//	data.areas.area[1].savedstage = isclear;
			//	//DataSaving.SaveStage2(isclear);
			//}
			//else
			//{
			//	//isclear = DataSaving.LoadStage1();
			//	isclear = data.areas.area[0].savedstage;
			//	isclear[stagenum] = true;
			//	data.areas.area[0].savedstage = isclear;
			//	//DataSaving.SaveStage1(isclear);
			//}
            data.SaveData();
		}
        private string FinishMission(bool iswin, string text, List<GameObject> lose, List<GameObject> win)	//control on Finist Interface showing
        {
			//To generate later and Showing it in Screen
            Time.timeScale = 1;
            later_.EndShow(iswin);//Show Latter on complete mission
            string s =text;	
            MissionCompleteWin(win);
            MissionCompleteLose(lose);
            return s;
        }
		/// <summary>
		/// Method to Initalize Camera and Sound
		/// </summary>
        private void Initialize()
        {
            if (isdrag)		//if allow drag screen
            {
                ClickControl.Initialize(uicam_);
            }
            else
            {
                ClickControl.Initialize(Camera.main); //is normal screen (non drag)
            }
            is_end_ = true;	//to disable ap and Generate butto of soldier while some animation had finish
            AnimationSystem.HideNavigation("ShowLater", gameObject); //Show the Navigation
            if (is_boss_scene_)
            {
                AudioSingleton.Instance().PlayBackground(1);  //Play sound in boss screen
            }
            else
            {
                AudioSingleton.Instance().PlayBackground(2);   //Play normal battle screen
            }
            AudioSingleton.Instance().transform.parent = Camera.main.gameObject.transform;	//Set Sound gameobject to the child of camera to Hear all place while dragging
		
        }
        private void OnClickButton(Transform hit)
        {
            if (hit != null)
            {
                if (hit.transform.gameObject.collider2D != null)
                {
                    GetButton(hit);//Click on Button
                }
            }
        }
        private void ShowLater() //Show Later Mission Start
        {
            later_.StartShow();//Use LatterSystem to generate the latter
            Invoke("StartLatterComplete", 2.1f);
        }
        private void StartLatterComplete()//Complete show Mission Start latter
        {
            later_.HideLaterStart();//use LatterSystem to hide it
            if (!is_boss_scene_)  //Work on Boss battle
                enemy_home_.GetComponent<tk2dSpriteAnimator>().Play();//Animation Destroy Boss
            player_home_.GetComponent<tk2dSpriteAnimator>().Play();	
            is_end_ = false;//set to end some functon
            RefreshApText();//Refresh to start counting
            upgrade_anim_.GetComponent<tk2dSpriteAnimator>().Play("Girl_Walk");//Set animation
            instand_ = this;//Allow other class can access directory
			//if (!is_boss_scene_)
			//	Invoke("GenerateEnemySoldier", 2);//Generate the Enemy solider when Normal Battle
			GenerateLevel();
			MiniMapControl.instance.Initialize(player_home_.transform.position, enemy_home_.transform.position);//set home for MinimapControl
            list_btn_solider = new List<GameObject>();	//Declair the list of button Soldier
            list_btn_solider.Add(GameObject.Find("bt_stank"));//Add button tank to list
			//string[] itemoder = DataSaving.LoadOrder(); //Load the button that allow to generate
            string[] itemoder = DataSavingXml.Instance().order.orders;
            for (int i = 0; i < itemoder.Length; i++)  //Generate the Button follow data saving
			{
                if (itemoder[i] != "")
                {
                    AddToList(test_, itemoder[i]);
                }
                //else
                //{
                //    AddToList(test_, "bt_empty");  //Nothing
                //}
            }
			Invoke("EnablePauseBT", 1);
        }
		/// <summary>
		/// To hide the Complete mission
		/// </summary>
        private void HideLoseLater()
		{
			later_.HideLaterStart();	//Hide lose later show
		}
		private IEnumerator ShowCompleteMission(bool iswin)//Start Display screen on Complete mission
		{
			yield return new WaitForSeconds(2f);//2);
			HideLoseLater();	 //Hide Mission Complete
			yield return new WaitForSeconds(1f);
			ShoWindowComplete(iswin);
			com_dialog_.SetActive(true); //start show the text gameobject
		}
		private void MissionCompleteWin(List<GameObject> l)	//Action for win soldier
		{
			foreach (GameObject g in l)
			{
				g.GetComponent<SoldierControl>().MissionComplete();//Set Win animation
			}
		}
		private void MissionCompleteLose(List<GameObject> l)// Action for lose soldier
		{
            if (l != null)
                for (int i = 0; i < l.Count;i++ )
                {
					l[i].GetComponent<SoldierControl>().Die(); //Set Die animation
                }
		}
		/// <summary>
		/// Cotrol on event of button
		/// </summary>
		/// <param name="hit"></param>
		private void GetButton(Transform hit) //Button control event
		{
			if (hit.transform.name == "bt_Area")	  //Title button
			{
				AnimationSystem.ScaleButton(hit.gameObject);
				AnimationSystem.ShowNavigation("GoToArea", gameObject);
			}
			else if (hit.transform.name == "btBack" || hit.transform.name == "bt_leave")
			{
				Time.timeScale = 1;
				AnimationSystem.ScaleButton(hit.gameObject);
				AnimationSystem.ShowNavigation("AreaSelect",gameObject);
			}
			else if (hit.transform.name == "bt_pause")
			{
				PauseGame();
			}
			else if (hit.transform.name == "bt_resume")
			{
				ResaumeGame();
            }
            else if (!is_end_ && hit.transform.name == "bt_speed_off")
            {
                bt_speed_off.SetActive(false);
                bt_spedd_on.SetActive(true);
                Time.timeScale = 2.5f;
            }
			else if (!is_end_ && hit.transform.name == "bt_speed_on")
            {
                bt_speed_off.SetActive(true);
                bt_spedd_on.SetActive(false);
                Time.timeScale = 1;
            }
			else if (!is_end_)
			{
				if (hit.gameObject == bt_last_obj_)
				{
					AddToList(test_, "bt_s1");
				}
				else if (hit.transform.name == "bt_stank")
				{
					if (!player_home_.GetComponent<SoldierHome>().IsDie())
					{
						if (hit.GetChild(0).GetComponent<ButtonSoldier>().IsAvailable())
							GenerateTank();
					}
				}
				else if (hit.transform.name == "bt_upgrade")
				{
					StartCoroutine(ClickUpgrade());
				}
			} 
		}
		private void BlinkLife()
		{
			blink_life_.color = new Color(blink_life_.color.r, blink_life_.color.g, blink_life_.color.b, Mathf.PingPong(Time.time * 2f, .7f));
		}
		private void EnablePauseBT()
		{
			pause_bt_.SetActive(true); // Show button Pause
            bt_speed_off.SetActive(true);
            bt_spedd_on.SetActive(false);
		}
		private void GetEscKey()
		{
			if (pause_bt_.activeSelf)
			{
				if (!pause_box_.activeSelf)
				{
					PauseGame();
				}
				else
				{
					ResaumeGame();
				}
			}
		}
		private void PauseGame()
		{
			isdrag = false;
			is_end_ = true;
			Time.timeScale = 0;
			if (!pause_box_.activeSelf)
			{
				pause_box_.SetActive(true);
				iTween.MoveFrom(pause_box_, iTween.Hash("y", 35, "speed", 50, "ignoretimescale", true));
			}
		}
		private void ResaumeGame()
		{
			iTween.MoveTo(pause_box_, iTween.Hash("y", 35, "speed", 50, "oncomplete", "EnableGame", "ignoretimescale", true, "oncompletetarget", gameObject));
		    
        }
		private void EnableGame()
		{
			is_end_ = false;
			isdrag = true;
			Time.timeScale = 1;
			pause_box_.SetActive(false);
			pause_box_.transform.localPosition = pause_pos_;
            if (bt_spedd_on.activeSelf)
            {
                Time.timeScale = 2.5f;
            }
		}
		private void HideLevelObject()
		{
			iTween.MoveAdd(lv_up_, iTween.Hash("x", -10, "speed", 70,"delay",2, "easetype", iTween.EaseType.linear));
		}
		private void HideDanger()
		{
			iTween.MoveAdd(dager_, iTween.Hash("x", -10, "speed", 70,"delay",2.5f, "easetype", iTween.EaseType.linear));
		}
        private IEnumerator ClickUpgrade()
        {
			lv_up_.transform.position = obj_lv_pos_;
			iTween.MoveFrom(lv_up_, iTween.Hash("x", 10, "speed", 70, "easetype", iTween.EaseType.linear,"oncomplete","HideLevelObject","oncompletetarget",gameObject));
			if(!lv_up_.activeSelf)
				lv_up_.SetActive(true);
			for (int i = 0; i < coin_rotate_.Length; i++)
			{
				if (!coin_rotate_[i].activeSelf) coin_rotate_[i].SetActive(true);
				coin_rotate_[i].GetComponent<CoinMovement>().StartAnimation();
			}
			if (count_num_ap_ <= 9)
			{
				count_point_ap_[count_num_ap_].SetActive(true);
				count_num_ap_++;
			}
            current_ap_ -= float.Parse(upgrade_value_.text);
            upgrade_value_.text = (float.Parse(upgrade_value_.text) + 20) + "";
			total_ap_ += 50;
            upgrade_anim_.GetComponent<tk2dSpriteAnimator>().Play("Girl_Sit");
            yield return new WaitForSeconds(1.5f);
            upgrade_anim_.GetComponent<tk2dSpriteAnimator>().Play("Girl_Walk");
			
			if(count_num_ap_>9)
			{
				Destroy(upgrade_anim_.transform.parent.GetComponent<BoxCollider2D>());
				upgrade_anim_.GetComponent<tk2dSpriteAnimator>().Stop();
				upgrade_value_.text = "MAX";
				upgrade_value_.color = Color.white;
			}
        }
		private void GoToArea()
		{
            Camera cam = Camera.main;
            if (!is_boss_scene_) cam = uicam_;
            // shake camera
            iTween.ShakePosition(cam.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("NewLevelArea", .4f);    // call method NewLevel after .4f delay
			
		}
		private void AreaSelect()
		{

            Camera cam = Camera.main;
            if (!is_boss_scene_) cam = uicam_;
            // shake camera
            iTween.ShakePosition(cam.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("NewLevelBack", .4f);    // call method NewLevel after .4f delay
			
		}
        private void NewLevelArea()
        {
			AreaSelectControl.is_from_menu_ = false;
            Application.LoadLevel("World");
        }
        private void NewLevelBack()
        {
			NewLevelArea();
        }
        private void GenerateTank()
        {
            GameObject temp = InstancePrefab("Tank", player_home_);
            temp.transform.GetChild(0).gameObject.SetActive(false);
            temp.AddComponent<SoldierControl>();
			//temp.transform.position = new Vector3(temp.transform.position.x, temp.transform.position.y, temp.transform.position.z);
            if (player_soldier_ == null) player_soldier_ = new List<GameObject>();
			player_soldier_.Add(temp);
			Vector3 tar_ps = temp.transform.position;
			if (is_boss_scene_)
				tar_ps.x = enemy_home_.transform.position.x - 10;
			else
				tar_ps.x = enemy_home_.transform.position.x;
            temp.GetComponent<SoldierControl>().SetToMove(tar_ps,new Tank(),Side.PLAYER);
		
        }
		private void GenerateTankEnemy()
		{
			if (!is_end_)
			{
				if (enemy_home_ != null && player_home_ != null && !enemy_home_.GetComponent<SoldierHome>().IsDie())
				{
					GameObject temp = InstancePrefab("Tank", enemy_home_);
					temp.transform.GetComponent<tk2dSprite>().scale = new Vector3(10, 10, 0);
					temp.transform.GetChild(0).gameObject.SetActive(false);
					temp.AddComponent<SoldierControl>();
					//temp.transform.position = new Vector3(temp.transform.position.x, temp.transform.position.y, temp.transform.position.z);
					if (enemy_soldier_ == null) enemy_soldier_ = new List<GameObject>();
					enemy_soldier_.Add(temp);
					Vector3 tar_ps = temp.transform.position;
					if (is_boss_scene_)
						tar_ps.x = player_home_.transform.position.x - 4;
					else
						tar_ps.x = player_home_.transform.position.x;
					temp.GetComponent<SoldierControl>().SetToMove(tar_ps, new Tank(true), Side.ENEMY);
					Invoke("GenerateTankEnemy", tank_time_interwal_);
				}
			}
		
		}
        private void GeneratePlayerSoldier(string solname)
        {
            GameObject temp = InstancePrefab("Soldier", player_home_);
            temp.AddComponent<SoldierControl>();
            Vector3 p_ps = temp.transform.position;
            p_ps = new Vector3(p_ps.x, p_ps.y - 1, p_ps.z - 1);
            temp.transform.position = p_ps;
            if (player_soldier_ == null) player_soldier_ = new List<GameObject>();
            player_soldier_.Add(temp);
            p_ps.x = enemy_home_.transform.position.x;
			SoldierType type = SoldierType.GetType(solname);
			if (!type.IsHasSkill)
			{
				Destroy(temp.transform.GetChild(1).gameObject);
				Destroy(temp.GetComponent<BoxCollider2D>());
				Destroy(temp.GetComponent<SkillControl>());
			} Debug.Log(solname);
			if (solname == "Sarubia")
			{
				Vector3 ps = temp.transform.position;
				ps.y = ps.y + 2.8f;
				p_ps.y = ps.y;
				temp.transform.position = ps;
				ps = temp.transform.GetChild(0).transform.position;
				ps.y += 2.5f;
				temp.transform.GetChild(0).transform.position = ps;
				Destroy(temp.transform.GetChild(2).gameObject);
			}
			if (solname == "Miner")
            {
                temp.GetComponent<SoldierControl>().SetToMove(p_ps, type, Side.PLAYER);
            }
			else
			{
				temp.GetComponent<SoldierControl>().SetToMove(p_ps, type, Side.PLAYER);
				temp.GetComponent<SkillControl>().StartReload();
			}
        }
		//private void GenerateEnemySoldier()
		//{
		//	//if (!is_end_)
		//	//{
		//	//	if (enemy_home_ != null && player_home_ != null && !enemy_home_.GetComponent<SoldierHome>().IsDie())
		//	//	{
		//	//		GameObject temp = InstancePrefab("Enemy", enemy_home_);
		//	//		Vector3 p_ps = temp.transform.position;
		//	//		p_ps = new Vector3(p_ps.x, p_ps.y - 1, p_ps.z - 1);
		//	//		temp.transform.position = p_ps;
		//	//		temp.AddComponent<SoldierControl>();
		//	//		if (enemy_soldier_ == null) enemy_soldier_ = new List<GameObject>();
		//	//		enemy_soldier_.Add(temp);
		//	//		p_ps.x = player_home_.transform.position.x;
		//	//		temp.GetComponent<SoldierControl>().SetToMove(p_ps, new Soldier(), Side.ENEMY);
		//	//		Invoke("GenerateEnemySoldier", enemy_time_interwal_);
                    
		//	//	}
		//	//}
		//}
		private void GenerateSarubia()
		{
			GameObject temp = InstancePrefab("Sarubia", player_home_);
			temp.transform.GetChild(0).gameObject.SetActive(false);
			temp.AddComponent<SoldierControl>();
			//temp.transform.position = new Vector3(temp.transform.position.x, temp.transform.position.y, temp.transform.position.z);
			if (player_soldier_ == null) player_soldier_ = new List<GameObject>();
			player_soldier_.Add(temp);
			Vector3 tar_ps = temp.transform.position;
			temp.GetComponent<SoldierControl>().SetToMove(tar_ps, new Sarubia(), Side.PLAYER);
		}
        private GameObject InstancePrefab(string name, GameObject parent)
        {
            GameObject temp;
			if (name == "Soldier") temp = (GameObject)Instantiate(templete_player_.transform.GetChild(0).gameObject);
			else temp = (GameObject)Instantiate(templete_.transform.GetChild(0).gameObject);
			temp.transform.position = parent.transform.position;
            if (!temp.activeSelf) temp.SetActive(true);
            temp.name = name;
            return temp;
        }
        private void IncreaseAp()
        {
            if (current_ap_ <= total_ap_)
            {
                current_ap_ += (Time.deltaTime * 6 +(count_num_ap_/10f));
            }
            RefreshApText();
            CheckBalanceUpgrade();
            CheckAvailableButton();
        }
        private void RefreshApText()
        {
			if (current_ap_ > total_ap_) current_ap_ = total_ap_;
            ap_text_.text = "AP " + current_ap_.ToString("F0") + "/" + total_ap_;
        }
        private void CheckBalanceUpgrade()
        {
			tk2dSprite upgrade_sprite = upgrade_anim_.transform.parent.GetComponent<tk2dSprite>();

			if (upgrade_sprite.gameObject.collider2D != null)
			{
				float upgrade = float.Parse(upgrade_value_.text);
				// Debug.Log(upgrade);
				
				if (upgrade > current_ap_)
				{
					upgrade_sprite.spriteId = upgrade_sprite.GetSpriteIdByName("bt_red");
					upgrade_sprite.gameObject.collider2D.enabled = false;
					upgrade_value_.color = Color.red;
				}
				else
				{
					upgrade_sprite.spriteId = upgrade_sprite.GetSpriteIdByName("bt_green");
					upgrade_sprite.gameObject.collider2D.enabled = true;
					upgrade_value_.color = Color.green;
				}
			}
        }
        private void CheckAvailableButton()
        {
            foreach (GameObject g in list_btn_solider)
			{
				g.transform.GetChild(0).GetComponent<ButtonSoldier>().CheckAvailable();
            }
        }
        private void CheckDragLimited()
        {
            Vector3 cam = Camera.main.transform.position;
            if (cam.x > maxX_)
            {
                cam.x = maxX_;
            }
            else if (cam.x < minX_)
            {
                cam.x = minX_;
            }
            Camera.main.transform.position = cam;
            MiniMapControl.instance.RefreshPositionRadar(minX_, maxX_, Camera.main.transform.position.x);
        }
        #endregion

        private Sprite GetSpriteByName(string name)
        {
            foreach (Sprite img in background_sprite_)
            {
                if (img.name == name)
                {
                    return img;
                }
            }
            return null;
        }
        private void ChangeBackground(string name)
        {
            background_[0].sprite = GetSpriteByName(name);
            if (background_.Length > 1)
            {
                background_[1].sprite = GetSpriteByName(name + "_1");
            }
        }

        #region Finger Guesture
        void OnDrag(DragGesture gesture)  // Drag screen
        {
            if (isdrag)
            {
                if (gesture.Phase == ContinuousGesturePhase.Updated && gesture.Fingers[0].Index == 0)
                {
                    if (gesture.Selection != null && gesture.Selection.name=="Main Camera")
                    {
                        Camera.main.transform.Translate(gesture.DeltaMove.x * 0.02f * -1 * drag_speed_, 0, 0);
                        
                    }
                }
            }
        }
        #endregion

        #region Scroll Area
        private IEnumerator OnClick()
		{
			if (!is_end_)
			{
                Transform hit = null;
                if (!isdrag)
                {
                    hit = GeneralSystem.ClickOnObject3D(Camera.main);
                }
                else
                {
                    hit = GeneralSystem.ClickOnObject3D(uicam_);
                }
				if (hit != null)
				{
                    if (!player_home_.GetComponent<SoldierHome>().IsDie())
                    {
                        if (hit.name != "bt_empty")
                        {
                            if (hit.GetChild(0).GetComponent<ButtonSoldier>().IsAvailable())
                            {
                                GeneratePlayerSoldier(hit.transform.name);
                                //if (hit.transform.name == "bt_s1")
                                //{
                                //    GeneratePlayerSoldier("");
                                //}
                                //else if (hit.transform.name == "bt_s2")
                                //{
                                //    GeneratePlayerSoldier("bt_s2");
                                //}
                                //else if (hit.transform.name == "bt_s3")
                                //{
                                //    GeneratePlayerSoldier("bt_s3");
                                //}
                                //else if (hit.transform.name == "bt_s4")
                                //{
                                //    GeneratePlayerSoldier("bt_s4");
                                //}
                            }
                        }
                    }
				}
			}
			yield return null;
		}
		private GameObject AddToList(GameObject g,string name)
		{
			GameObject temp = (GameObject)Instantiate(g);
			temp.name = name;
			if (name != "bt_empty")
				list_btn_solider.Add(temp);
			else
				temp.transform.GetChild(0).GetComponent<ButtonSoldier>().SetToEmpty();
			if (!temp.activeSelf) temp.SetActive(true);
			temp.transform.parent = contiant_.transform;
			temp.transform.localScale = new Vector3(.7f, 1, -1);
			if (last_stand_obj_ != null) last_x_ = last_stand_obj_.position.x;
			SetLastPosition(temp);
			last_stand_obj_ = temp.transform;
			scroll_area_.ContentLength = scroll_area_.ContentLength + .5f;//0.45f;
            return temp;
		}
		private void SetLastPosition(GameObject g)
		{
			last_x_ += 2;//1.5f;
			g.transform.position = new Vector3(last_x_ + 3, -14.5f, 3);
		}
		#endregion

		#region Skill
		private void ClickOnSoldier(Transform g)
		{
			skill_ = g.GetComponent<SkillControl>();
			if (!skill_.IsAvailableSkill())
			{
				//if (player_soldier_.Count > 1)
				//{
				//	for (int i = 0; i < player_soldier_.Count; i++)
				//	{
				//		if (!player_soldier_.Contains(g.gameObject))
				//		{
				//			skill_ = player_soldier_[i].GetComponent<SkillControl>();
				//			if (skill_ != null)
				//			{
				//				if (skill_.IsAvailableSkill())
				//					break;
				//			}
				//		}
				//	}
				//}
			}
		}
		private void DisableSkill()
		{
			if (player_soldier_ != null)
			{
				SkillControl obj;
				BoxCollider2D col;
				for (int i = 0; i < player_soldier_.Count; i++)
				{
					obj = player_soldier_[i].GetComponent<SkillControl>();
					if (obj != null)
					{
						obj.StopSkill();
						Destroy(obj);
					}
					col = player_soldier_[i].GetComponent<BoxCollider2D>();
					if (col != null)
						Destroy(col);
					for (int j = 0; j < player_soldier_[i].transform.childCount; j++)
					{
						Destroy(player_soldier_[i].transform.GetChild(j).gameObject);
					}
				}
			}
			if (enemy_soldier_ != null)
			{
				DisableLifeDisplay(enemy_soldier_);
			}
		}
		private void DisableLifeDisplay(List<GameObject> lsitem)
		{
			for (int i = 0; i < lsitem.Count; i++)
			{
				for (int j = 0; j < lsitem[i].transform.childCount; j++)
				{
					Destroy(lsitem[i].transform.GetChild(j).gameObject);
				}
			}
		}
		private void Test()
		{
			Debug.Log("MetalMainGame Calling");
		}
		#endregion

		#region Variable
		[HideInInspector]
		public static MetalSlugMainGame instand_ = null;
		[HideInInspector]
		public List<GameObject> player_soldier_ = null;
		[HideInInspector]
		public List<GameObject> enemy_soldier_ = null;
		[HideInInspector]
		public bool is_end_ = false;
        public static string bgname_ = "";
        public GameObject player_home_ = null;
		public GameObject enemy_home_ = null;
		public bool is_boss_scene_ = false;
		[SerializeField]
		private GameObject templete_ = null;
		[SerializeField]
		private GameObject templete_player_ = null;
		[SerializeField]
		private tk2dUIScrollableArea scroll_area_ = null;
		[SerializeField]
		private GameObject com_dialog_ = null;
		[SerializeField]
		private GameObject test_ = null;
		[SerializeField]
		private GameObject bt_last_obj_ = null;
		[SerializeField]
		private GameObject contiant_ = null;
        [SerializeField]
        private TextMesh ap_text_ = null;
        [SerializeField]
        private GameObject upgrade_anim_ = null;
        [SerializeField]
        private TextMesh upgrade_value_ = null;
		[SerializeField]
		private ControlLater later_ = null;
        [SerializeField]
        private bool isdrag = false;
        [SerializeField]
        private float maxX_ = 60;
        [SerializeField]
        private float minX_ = 6;
        [SerializeField]
        private float drag_speed_ = 10;
        [SerializeField]
        private Camera uicam_ = null;
        [SerializeField]
        private Sprite[] background_sprite_ = null;
        [SerializeField]
        private SpriteRenderer[] background_ = null;
		[SerializeField]
		private GameObject pause_box_ = null;
		[SerializeField]
		private GameObject pause_bt_ = null;
        [SerializeField]
        private GameObject bt_speed_off = null;
        [SerializeField]
        private GameObject bt_spedd_on = null;
		[SerializeField]
		private SpriteRenderer blink_life_ = null;

		private SkillControl skill_ = null;
		private Vector3 last_item_pos_;
		[SerializeField]
		private float last_x_ = -18.6f;//-15;
		private Transform last_stand_obj_ = null;
		private float total_ap_ = 0;
		public float current_ap_ = 0;							
		private List<GameObject> list_btn_solider = null;
		//private float enemy_time_interwal_;
		private float tank_time_interwal_;
		private Vector3 pause_pos_;
		private bool is_count_up_fund_ = false;
		private string[] enemy_name_ = null;
		private TextMesh text_found_fund_;
		[SerializeField]
		private GameObject[] coin_rotate_ = null;
		[SerializeField]
		private GameObject[] count_point_ap_ = null;
		private int count_num_ap_ = 0;
		[SerializeField]
		private GameObject dager_ = null;
		[SerializeField]
		private GameObject lv_up_ = null;
		private Vector3 obj_lv_pos_ = Vector3.zero;
		private bool is_danger_ = false;
		private int total_reward_found_ = 0;
		#endregion
	}
}
