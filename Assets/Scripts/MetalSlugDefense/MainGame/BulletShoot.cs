﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YNinja.MetalSlug
{
	public class BulletShoot : MonoBehaviour
	{
		// Update is called once per frame
		void Update()
		{
            
			if (is_attack_ && Time.timeScale>0)	   //enable attack
			{
				CheckHit();	   //find the available to hit
                if (is_move_toward_)// for Bazooka soldier
                {
                    if (sol_cont_.side_ == Side.PLAYER)
                    {
                        transform.Translate(Vector3.right * speed_); //Time.deltaTime * 10f); //Calculate the rigibody
                        if (sol_cont_.type_.GetType() != typeof(Sarubia))
                        {
                            transform.localRotation = Quaternion.Euler(0, 180, 0);
                        }
						else if(sol_cont_.type_.GetType() != typeof(Mummy))
						{
							transform.localRotation = Quaternion.Euler(0, 0, 0);
						}
                    }
                    else
                    {
                        transform.Translate(Vector3.left * speed_); //Time.deltaTime * 10f);
                    }
                }
                else if (is_drop_down_)
                {
                    transform.Translate(Vector3.down * speed_);
                    if (gameObject.transform.position.y <= y_target_)
                        DropSarFinish();
                }
                else   //General Soldier
                {
                    if (gameObject.transform.position.y <= y_target_)	//arrive the target
                    {
                        is_attack_ = false;	 //disable this attacking
                        if (gameObject.transform.childCount > 0)  //has fire object
                            BoomParticle();		   //Show fire particle
                        gameObject.SetActive(false);  //Disable And stop attacking
                    }
                }
			}
		}
		private void CheckHit()	//Find available hit soldier
		{
			List<GameObject> lsenemy;
			GameObject home;
			if (sol_cont_.side_ == Side.PLAYER)
			{
				lsenemy = MetalSlugMainGame.instand_.enemy_soldier_;
				home = MetalSlugMainGame.instand_.enemy_home_;
			}
			else
			{
				lsenemy = MetalSlugMainGame.instand_.player_soldier_;
				home = MetalSlugMainGame.instand_.player_home_;
			}
			if (home != null)	 //Soldier of Player
			{
				float dis;//store the distand of position for comparation
				float hitdis=2.7f;
				GameObject g;//Stor the soldier of Enemy forcomaration
				for (int i = 0; i < lsenemy.Count;i++ )//Use all enemy for compare with
				{
					g = lsenemy[i].gameObject;  //the soldier of enemy one by one
					dis = Vector2.Distance(g.transform.position, gameObject.transform.position); //get the distand of Soldier and Enemy
					if (g.GetComponent<SoldierControl>().type_.GetType() == typeof(Sarubia))
					{
						hitdis = 3f;
						if (sol_cont_.type_.GetType() == typeof(Sarubia) )
							hitdis = 5f;
					}
					if (dis <= hitdis)  //the nearest or available to hit
					{
						SoldierControl obj = g.GetComponent<SoldierControl>(); //Get componet of the hit bullet
						obj.RefreshLife(pow_cut_life_);//sol_cont_.type_.Damage); //cut down life
						obj.IsDie();  //Check die or not
						if (gameObject.transform.childCount > 0)  //has fire object
						{
							BoomParticle();		   //Show fire particle
							gameObject.SetActive(false);	 //Disable and stop attaccking
						}
						is_attack_ = false;	//Stop attack
					}

				}
				if (home != null) //not destroy
				{
					dis = Vector2.Distance(gameObject.transform.position, home.transform.position);//Check distand of Soldier and Tower
					if (dis <= 4f)//available to Hit
					{
						SoldierHome obj = home.GetComponent<SoldierHome>();//Get component of Tower for calcualte
						obj.RefreshLife(pow_cut_life_);//sol_cont_.type_.Damage); // Cut down life
						if (obj.IsDie()) //Check it die or not
						{
							MetalSlugMainGame.instand_.ShowDialog(sol_cont_.side_);
							sol_cont_.MissionComplete();
						}
						is_attack_ = false;//Disable attack
						if (gameObject.transform.childCount > 0)  //has fire object
						{
							BoomParticle();		   //Show fire particle
							gameObject.SetActive(false);	 //Disable and stop attaccking
						}
					}
				}
			}
		}
		private void MoveOnItween()//Start to enable move bullet
		{
			is_attack_ = true;
		}
		public void Shoot(SoldierControl compsoldier)
		{
            sol_cont_ = compsoldier; //store the component of Soldier for calcualte
			if (sol_cont_.side_ == Side.PLAYER)
				rigidbody2D.velocity = new Vector2(9, 12);//(7,15);//Throw item up
			else
				rigidbody2D.velocity = new Vector2(-9, 12);//Throw item up
            is_attack_ = true;//enable hit
			Destroy(gameObject, 1.8f );//Wait to destroy in second time
			y_target_ = gameObject.transform.position.y - 2;//the axis of land limited
			pow_cut_life_ = sol_cont_.type_.Damage;
		}
		public void SarubiaShoot(SoldierControl compsoldier)
		{
			Destroy(gameObject.GetComponent<Rigidbody2D>());//Disble rigidbody for shoot toward
			sol_cont_ = compsoldier; //store the component of Soldier for calcualte
			sol_cont_.StartCoroutine(DropSar());
		}
        public void BazookaShot(SoldierControl compsoldier)
        {
            Destroy(gameObject.GetComponent<Rigidbody2D>());//Disble rigidbody for shoot toward
			Vector3 pos = gameObject.transform.position;//to enrease bullet position compare with gun
			pos.y = pos.y + 1;
			gameObject.transform.position = pos;
            sol_cont_ = compsoldier;//get component of Soldier for calculate the life
            is_attack_ = true;// enable find hit
            is_move_toward_ = true; //Define this bullet it's Bazooka soldier
			Destroy(gameObject, 2.2f );//Delay to destroy or limited of Bullet
			speed_ = Time.deltaTime * 10;
			pow_cut_life_ = sol_cont_.type_.Damage;
        }
		public void MarcoShot(SoldierControl compsoldier,bool isskill)
		{
			Destroy(gameObject.GetComponent<Rigidbody2D>());//Disble rigidbody for shoot toward
			Vector3 pos = gameObject.transform.position;//to enrease bullet position compare with gun
			pos.y = pos.y + 1;
			gameObject.transform.position = pos;
			sol_cont_ = compsoldier;//get component of Soldier for calculate the life
			is_attack_ = true;// enable find hit
			is_move_toward_ = true; //Define this bullet it's Bazooka soldier
			if (isskill)
			{
				pow_cut_life_ = sol_cont_.type_.SkillDemage;
				speed_ = Time.deltaTime * 20;
				Destroy(gameObject, .8f);//Delay to destroy or limited of Bullet
			}
			else
			{
				pow_cut_life_ = sol_cont_.type_.Damage;
				speed_ = Time.deltaTime * 10;
				Destroy(gameObject, 1.8f );//Delay to destroy or limited of Bullet
			}
		}
		public void FioShootS(SoldierControl compsoldier)
		{
			Destroy(gameObject.GetComponent<Rigidbody2D>());
			sol_cont_ = compsoldier;
			is_attack_ = true;
			is_move_toward_ = true;
			Destroy(gameObject, 2f );
			speed_ = Time.deltaTime * 12;
			pow_cut_life_ = sol_cont_.type_.SkillDemage;
		}
		public void TarmaShootS(SoldierControl compsoldier)
		{
			Destroy(gameObject.GetComponent<Rigidbody2D>());
			sol_cont_ = compsoldier;
			is_attack_ = true;
			is_move_toward_ = true;
			Destroy(gameObject, .7f );
			speed_ = Time.deltaTime * 10;
			pow_cut_life_ = sol_cont_.type_.SkillDemage;
		}
		public void MummyFire(SoldierControl compsolier)
		{
			Destroy(gameObject.GetComponent<Rigidbody2D>());
			sol_cont_ = compsolier;
			is_attack_ = true;
			is_move_toward_ = true;
			Destroy(gameObject, 1f );
			speed_ = Time.deltaTime * 7;
			pow_cut_life_ = sol_cont_.type_.Damage;
		}
		private void BoomParticle()
		{
			GameObject fires_;//to store the Clone bullet object
			fires_ = (GameObject)Instantiate(gameObject.transform.GetChild(0).gameObject);//clone from the child of gameobject
			fires_.transform.position = gameObject.transform.position; //set position
			if (!fires_.activeSelf) fires_.SetActive(true);	  //to enanble it hiden
			fires_.GetComponent<tk2dSpriteAnimator>().Play("Fires_Shoot_Big");//Show animation boom
			Destroy(fires_, .6f); //delay to destroy
		}
		private IEnumerator DropSar()
		{
			yield return new WaitForSeconds(.3f);
			gameObject.SetActive(true);
			is_attack_ = true;//enable hit
			Destroy(gameObject, 2.5f );//Wait to destroy in second time
			y_target_ = gameObject.transform.position.y - 6.3f;//the axis of land limited
			speed_ = Time.deltaTime * 7;
			pow_cut_life_ = sol_cont_.type_.Damage;
			is_drop_down_ = true;
		}
		private void DropSarFinish()
		{
			gameObject.GetComponent<tk2dSpriteAnimator>().Play("Bullet_sar_move");
			is_move_toward_ = true;
		}
		#region variable
		private bool is_attack_ = false;   //enable and disable find hit enemy
		private SoldierControl sol_cont_ = null;// the component of hit enemy for calculate life
		private bool is_move_toward_ = false;//define that this bullet is of Bazooka Soldier shooter
		private float y_target_;//the limited of ground
		private float speed_;
		private float pow_cut_life_;
		private bool is_drop_down_=false;
		#endregion
	}
}