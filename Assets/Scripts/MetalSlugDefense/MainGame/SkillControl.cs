﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class SkillControl : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{
			scale_x_ = gameObject.transform.localScale.x;
		}

		// Update is called once per frame
		void Update()
		{

		}
		public bool IsAvailableSkill()
		{
			if (is_available_)
			{
				ShowSkill();
				StartReload();
				return true;
			}
			return false;
		}
		public void StopSkill()
		{
			gameObject.GetComponent<BoxCollider2D>().enabled = false;
			is_available_ = false;
			progress_bar_.SetActive(false);
			iTween.Stop(gameObject);
		}
		public void StartReload()
		{
			is_available_ = false;
			progress_bar_.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.red;
			iTween.ScaleFrom(progress_bar_, iTween.Hash("x", 0
														, "time", 10
														, "Oncomplete", "Available"
														, "easetype", iTween.EaseType.linear
														, "oncompletetarget", gameObject
														));
		}
		private void ShowSkill()
		{
			sold_cont_ = gameObject.GetComponent<SoldierControl>();
			sold_cont_.SkillAction();
            if (sold_cont_.GetNameSoldier() == "Soldier" || sold_cont_.GetNameSoldier() == "SoldierShield")
			{
				Invoke("ThrowBoom", .59f);
				Invoke("CompleteSkill", 0.94f);
			}
			else if (sold_cont_.GetNameSoldier() == "Bazooka")
			{
				//BazookaShoot();
                NormalSkill();
				float delay = 0;
				for (int i = 0; i < 2; i++)
				{
					Invoke("NormalSkill", .3f+delay);
					delay += .3f;
				}
				Invoke("CompleteSkill", 1.2f);
			}
            else if (sold_cont_.GetNameSoldier() == "Marco")
            {
                NormalSkill();
                float delay = 0;
                for (int i = 0; i < 4; i++)
                {
                    Invoke("NormalSkill", .15f + delay);
                    delay += .15f;
                }
                Invoke("CompleteSkill", .6f);
            }
            else
            {
                NormalSkill();
                Invoke("CompleteSkill", .15f);
            }
            //else if (sold_cont_.GetNameSoldier() == "Tarma")
            //{
            //    NormalSkill();
            //    Invoke("CompleteSkill", .15f);
            //}
            //else if (sold_cont_.GetNameSoldier() == "Eri")
            //{
            //    NormalSkill();
            //    Invoke("CompleteSkill", .15f);
            //}
            //else if (sold_cont_.GetNameSoldier() == "Fio")
            //{
            //    NormalSkill();
            //    Invoke("CompleteSkill", .15f);
            //}

            //else
            //{
            //    Debug.Log(sold_cont_.GetNameSoldier());
            //}
		}
        private void NormalSkill()
        {
            if (sold_cont_ != null && !sold_cont_.IsDie())
            {
                sold_cont_.type_.Skill(gameObject);
            }
        }
        //private void BazookaShoot()
        //{
        //    if (sold_cont_ != null && !sold_cont_.IsDie())
        //    {
        //        sold_cont_.BazookaShoot();
        //    }
        //}
        //private void MarcoShoot()
        //{
        //    if (sold_cont_ != null && !sold_cont_.IsDie())
        //    {
        //        sold_cont_.MarcoShoot(true);
        //    }
        //}
		private void ThrowBoom()
		{
			if (sold_cont_ != null && !sold_cont_.IsDie())
			{
                //GameObject bullet;
                //bullet = (GameObject)Instantiate(Resources.Load("Prefabs/Bullets/HandBoom"));
                //bullet.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + 1);
                //bullet.GetComponent<BulletShoot>().Shoot(sold_cont_);
                sold_cont_.type_.Skill(gameObject);
			}
		}
        //private void TarmaShoot()
        //{
        //    if (sold_cont_ != null && !sold_cont_.IsDie())
        //    {
        //        sold_cont_.TarmaShoot(true);
        //    }
        //}
        //private void EriShoot()
        //{
        //    if (sold_cont_ != null && !sold_cont_.IsDie())
        //    {
        //        sold_cont_.EriShoot(true);
        //    }
        //}
        //private void FioShoot()
        //{
        //    if (sold_cont_ != null && !sold_cont_.IsDie())
        //    {
        //        sold_cont_.FioShoot(true);
        //    }
        //}
		
        private void CompleteSkill()
		{
			if (sold_cont_ != null && !sold_cont_.IsDie())
			{
				sold_cont_.FinishSkill();
			}
		}
		private void Available()
		{
			progress_bar_.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.green;
			is_available_ = true;
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
		}
		private void SetToDefaultScale()
		{
			progress_bar_.transform.localScale = new Vector3(scale_x_, progress_bar_.transform.localScale.y, 0);
		}

		#region	Variable
		[SerializeField]
		private GameObject progress_bar_ = null;
		private bool is_available_ = false;
		private float scale_x_;
		private SoldierControl sold_cont_ = null;
		#endregion
	}
}
