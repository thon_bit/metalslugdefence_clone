﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody2D))]
public class CoinMovement : MonoBehaviour {

   
    public void StartAnimation()
    {
        this.gameObject.transform.position = default_pos;
        this.rigidbody2D.velocity = velocity;
    }
	
    [SerializeField]
    private Vector2 velocity = Vector2.zero;
    [SerializeField]
    private Vector3 default_pos = Vector3.zero;
}
