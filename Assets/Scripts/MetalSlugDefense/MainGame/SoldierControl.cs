﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using YNinja.MainGame;

namespace YNinja.MetalSlug
{
    public enum Side
    {
        PLAYER,
        ENEMY							            
    }
	public class SoldierControl : MonoBehaviour
	{
        // Update is called once per frame
        void Update()
        {
            if (isstartmoving_)// moving
            {
                if (!istankskill)
                {
					if (!CheckAvailableAttack())
					{
						transform.position = Vector3.MoveTowards(transform.position, target_, Time.deltaTime * speed_moving_);
						if (Vector3.Distance(transform.position, target_) <= distance_)//distance near end path
						{
							isstartmoving_ = false;
							Complete();
						}
					}
                }
                #region TankSkill
                else
                {
                    GameObject enemy = GetFristEnemy();
                    if (enemy == null)
                    {
                        enemy = MetalSlugMainGame.instand_.enemy_home_;
                    }
					if (enemy != null)
					{
						Vector3 target = enemy.transform.position;
						target.z = target.z - 1;
                        target.y = MetalSlugMainGame.instand_.enemy_home_.transform.position.y;
						transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed_moving_);
						if (transform.position.x >= target.x)//distance near end path
						{
							isstartmoving_ = false;	 
							if (enemy != MetalSlugMainGame.instand_.enemy_home_)
							{
								enemy.GetComponent<SoldierControl>().Die();
							}
							else
							{
								enemy.GetComponent<SoldierHome>().RefreshLife(2f);
								if (enemy.GetComponent<SoldierHome>().IsDie())
								{
									MetalSlugMainGame.instand_.ShowDialog(Side.PLAYER);
								}
							}
							gameObject.GetComponent<tk2dSprite>().color = Color.white;
							gameObject.GetComponent<tk2dSpriteAnimator>().Play("Tank_Explode");
							Destroy(minipoint_);
							Destroy(gameObject, 1f);
						}
					}
					else
					{
						gameObject.GetComponent<tk2dSprite>().color = Color.white;
						gameObject.GetComponent<tk2dSpriteAnimator>().Play("Tank_Explode");
						Destroy(minipoint_);
						Destroy(gameObject, 1f);
					}
                }
                MiniMapControl.instance.RefreshLocation(this.transform.position, minipoint_);
            }
                #endregion
        }
        public  void SetToMove(Vector3 target, SoldierType type, Side side)
        {
            isstartmoving_ = true;
            target_ = target;
            type_ = type;
            SetAnimation("Move");
			side_ = side; 
            distance_ = type.AttackDistance;
			health_ =total_health_= type.Health;
			speed_moving_ = type_.Speed;
            if (type.GetType() == typeof(Tank))
            {
                distance_ -= 2;
                if (side == Side.PLAYER)
                {
					target_.z = target_.z - 2;
					target_.x = target_.x - 3;
                    speed_moving_ *= 8;
                    TankSkill();
                    istankskill = true;
                    MetalSlugMainGame.instand_.player_soldier_.Remove(gameObject);
                }
            }
            if (type.GetType() == typeof(Miner)) { speed_moving_ *= 2; }
            
            if (side == Side.PLAYER)
            {
                //enemy_list_ = MetalSlugMainGame.instand_.enemy_soldier_;
                //minipoint_ = MiniMapControl.instance.GetMiniPoint(Color.white);
                //own_home_ = MetalSlugMainGame.instand_.player_home_;
                //enemy_home_ = MetalSlugMainGame.instand_.enemy_home_;
                SetValue(MetalSlugMainGame.instand_.enemy_soldier_, Color.white, MetalSlugMainGame.instand_.player_home_, MetalSlugMainGame.instand_.enemy_home_);
            }
            else
            {
                //enemy_list_ = MetalSlugMainGame.instand_.player_soldier_;
                //minipoint_ = MiniMapControl.instance.GetMiniPoint(Color.red);
                //own_home_ = MetalSlugMainGame.instand_.enemy_home_;
                //enemy_home_ = MetalSlugMainGame.instand_.player_home_;
                SetValue(MetalSlugMainGame.instand_.player_soldier_, Color.red, MetalSlugMainGame.instand_.enemy_home_, MetalSlugMainGame.instand_.player_home_);
            }
            SetHealthBar();
            
        }
        
        public  void RefreshLife(float amount)
		{
           health_ -= amount;
           RefreshHealthBar();
			//SetAlspa();
        }
        public  bool IsDie()
		{
            if (health_ <= 0 || own_home_ == null)
            {
                Die();
                return true;
            }
            return false;
        }
		public float Health
		{
			set
			{
				health_=total_health_ = value;
			}
		}
		public void MissionComplete()
		{
			try
			{
				if (own_home_ != null)
				{
					if (this != null && gameObject.GetComponent<tk2dSpriteAnimator>() != null)
					{
						if (type_.WinAnimation != "")
							SetAnimation(type_.WinAnimation);
						else
							Destroy(gameObject.transform.GetChild(0).gameObject);
					}
				}
				else
				{
					Die();
				}
			}
			catch { Debug.Log("Die in the same time "); }
			isstartmoving_ = false;
		}
		public void SkillAction()
		{
			isstartmoving_ = false;
			SetAnimation(type_.SkillAnimation);
			is_skill_ = true;
		}
		public void FinishSkill()
		{
			SetAnimation("Move");//type_.MoveAnimation);
			is_skill_ = false;
			isstartmoving_ = true;
		}
		public string GetNameSoldier()
		{
			string sold = type_.ToString().Substring(17);
			return sold;
		}
        public GameObject GenerateBullet(string name)
        {
            GameObject bullet;
            Vector3 pos;
            bullet = (GameObject)Instantiate(Resources.Load("Prefabs/Bullets/" + name));
            bullet.transform.parent = gameObject.transform;
            pos = gameObject.transform.position;
            pos.y = pos.y - .2f;
            pos.x = pos.x + .7f;
            bullet.transform.position = pos;
            bullet.GetComponent<tk2dSpriteAnimator>().Play();
            return bullet;
        }
        
        public void SetValue(List<GameObject> enemy, Color point, GameObject own, GameObject enemyhome)
        {
            enemy_list_ = enemy;
            minipoint_ = MiniMapControl.instance.GetMiniPoint(point);
            own_home_ = own;
            enemy_home_ = enemyhome;
        }
        private void ShowWall()
        {
			//Wall
            GameObject temp = (GameObject)Instantiate(gameObject);
            temp.transform.GetChild(1).gameObject.SetActive(false);
            temp.transform.parent = null;
            SoldierControl sc = temp.GetComponent<SoldierControl>();
            sc.SetHealthBar();
            sc.type_ = new Miner(false);
            sc.Health = sc.type_.Health;
            temp.GetComponent<tk2dSpriteAnimator>().Stop();
            temp.GetComponent<tk2dSprite>().spriteId = temp.GetComponent<tk2dSprite>().GetSpriteIdByName("wall1");

			sc.side_ = Side.PLAYER;
			sc.SetValue(MetalSlugMainGame.instand_.enemy_soldier_, Color.white, MetalSlugMainGame.instand_.player_home_, MetalSlugMainGame.instand_.enemy_home_);
			
			//Miner
			target_ = MetalSlugMainGame.instand_.player_home_.transform.position;
            enemy_list_ = new List<GameObject>();
            MetalSlugMainGame.instand_.player_soldier_.Add(temp);
            gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            gameObject.GetComponent<tk2dSpriteAnimator>().Play(type_.MoveAnimation);
            isstartmoving_ = true;
            distance_ = 0;
            speed_moving_ *= 2;
        }
		private bool CheckAvailableAttack()
        {
            Vector2 posthis = gameObject.transform.position;
            Vector2 possoldier;
            foreach (GameObject soldier in enemy_list_)
            {
                possoldier = soldier.transform.position;
                if (Vector2.Distance(possoldier, posthis) <= type_.AttackDistance)
                {
                    if (!IsDie())
                    {
                        SetAttack(soldier);
                        return true;
                    }
                }
            }
			return false;
        }
        private void SetAnimation(string action)
        {
            if (type_.GetType() == typeof(Marco))
            {
                //Debug.Log("MOve");
                gameObject.GetComponent<tk2dSprite>().scale = new Vector3(5, 5, 0);
            }
            if (action == "Move")
            {
                gameObject.GetComponent<tk2dSpriteAnimator>().Play(type_.MoveAnimation);
                if (gameObject.transform.position.x < target_.x)
                {
                    gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
                }

            }
            else
            {
                gameObject.GetComponent<tk2dSpriteAnimator>().Play(action);
            }

            if (type_.GetType() == typeof(Wolf) || type_.GetType() == typeof(YoungGirl) || type_.GetType() == typeof(YoungGirlGreen))
            {
                gameObject.GetComponent<tk2dSprite>().scale = new Vector3(5, 5, 0);
            }
        }
		private void SetAttack(GameObject g)
		{
			if (!is_skill_)
			{
				isstartmoving_ = false;
				SetAnimation(type_.AttackAnimation);
				obj_attack_ = g;
				enemy_ = obj_attack_.GetComponent<SoldierControl>();
                if (type_.AttackType== AtkType.RANGE)
                {
                    type_.Shoot(this.gameObject, false);
                }
				if (type_.GetType() != typeof(Miner))
				{
					Invoke("Attack", type_.TimeInterwal);//1);							
                }
			}
		}
        private void Attack()
		{

			if (!MetalSlugMainGame.instand_.is_end_)
			{
				if (IsDie())
				{
					this.enabled = false;
				}
				else if (!is_skill_)
				{
					if (!enemy_.IsDie() && own_home_ != null && enemy_home_ != null)
					{
						if (type_.AttackType== AtkType.RANGE)
						{
							type_.Shoot(this.gameObject, false);
							Invoke("ShootComplete", type_.TimeInterwal);
						}
						else
						{
							enemy_.RefreshLife(type_.Damage);
							if (enemy_.IsDie())
							{
								SetAnimation("Move");//type_.MoveAnimation);
								isstartmoving_ = true;
							}
							else
							{
								Invoke("Attack", type_.TimeInterwal);
							}
						}
					}
					else
					{
						if (enemy_home_ != null)
						{
                            SetAnimation("Move");//type_.MoveAnimation);
							isstartmoving_ = true;
						}
						else
						{
							MissionComplete();
						}
					}
				}
			}
			else
			{
				if (IsDie())
				{
					this.enabled = false;
				}
				else if (own_home_ == null)
				{
					Die();
				}
		
			}
        }
		private void ShootComplete()
		{
			if (MetalSlugMainGame.instand_.is_end_)
			{
				MissionComplete();
			}
			else
			{
				if (!CheckAvailableAttack())
				{
                    SetAnimation("Move");//type_.MoveAnimation);
					isstartmoving_ = true;
				}
				else if(enemy_home_==null)
				{
					MetalSlugMainGame.instand_.ShowDialog(side_);
				}
			}
		}
        private void Complete()
        { 
			isstartmoving_ = false;
			SetAnimation(type_.AttackAnimation);
			if (side_ == Side.PLAYER)
			{
				obj_attack_ = MetalSlugMainGame.instand_.enemy_home_;
			}
			else
			{
				obj_attack_ = MetalSlugMainGame.instand_.player_home_;
			}
			if (obj_attack_ != null)
			{
                if (type_.AttackType == AtkType.RANGE || type_.GetType() == typeof(Miner))
                {
                    if (type_.GetType() == typeof(Miner))
                    {
                        if (target_ == MetalSlugMainGame.instand_.player_home_.transform.position)
                        {
                            Destroy(minipoint_);
                            Destroy(gameObject);
                        }
                        else
                        {
                            type_.Shoot(gameObject, false);
                        }
                    }
                    else
                    {		   
                        type_.Shoot(gameObject, false);
                    }
                }
				if (type_.GetType() != typeof(Miner))
					Invoke("HomeAttack", type_.TimeInterwal);//1f);
			}
			else
			{
				MissionComplete();
			}
        }
        private void HomeAttack()
        {
			if (!MetalSlugMainGame.instand_.is_end_)
			{
				if (IsDie())
				{
					this.enabled = false;
				}
				else if (side_ == Side.ENEMY && MetalSlugMainGame.instand_.player_home_ != null && MetalSlugMainGame.instand_.player_soldier_.Count > 0)
				{
					SetAttack(MetalSlugMainGame.instand_.player_soldier_[0].gameObject);
				}
				else if (side_ == Side.PLAYER && MetalSlugMainGame.instand_.enemy_home_ != null && MetalSlugMainGame.instand_.enemy_soldier_.Count > 0)
				{
					SetAttack(MetalSlugMainGame.instand_.enemy_soldier_[0].gameObject);
				}
				else if (enemy_home_ != null && !enemy_home_.GetComponent<SoldierHome>().IsDie())
				{
					if (type_.AttackType== AtkType.RANGE)
					{
						type_.Shoot(this.gameObject, false);
						Invoke("ShootComplete", type_.TimeInterwal);
					}
					else
					{
						// enemy_.RefreshLife(type_.Damage);
						enemy_home_.GetComponent<SoldierHome>().RefreshLife(type_.Damage);
						// }
						if (enemy_home_.GetComponent<SoldierHome>().IsDie())
						{
							MetalSlugMainGame.instand_.ShowDialog(side_);
							//SetAnimation(type_.MoveAnimation);
							//isstartmoving_ = true;
						}
						else
						{
							Invoke("HomeAttack", type_.TimeInterwal);//1f);
						}
					}
				}
				else
				{
					MetalSlugMainGame.instand_.ShowDialog(side_);
				}
			}
        }
        public void Die()
        {
            isstartmoving_ = false;
			try
			{
                if (healthbar != null) healthbar.SetActive(false);
				if (side_ == Side.PLAYER)
				{
					if (gameObject != null)
					{
						MetalSlugMainGame.instand_.player_soldier_.Remove(gameObject);
						if(gameObject.collider2D!=null)
                            gameObject.GetComponent<BoxCollider2D>().enabled = false;
						for (int i = 0; i < gameObject.transform.childCount; i++)
						{
							Destroy(gameObject.transform.GetChild(i).gameObject);
						}
					}
					if (MetalSlugMainGame.instand_.player_home_ == null)
						MetalSlugMainGame.instand_.ShowDialog(Side.ENEMY);
				}
				else
				{
					if (gameObject != null)
						MetalSlugMainGame.instand_.enemy_soldier_.Remove(gameObject);
					if(MetalSlugMainGame.instand_.enemy_home_==null)
						MetalSlugMainGame.instand_.ShowDialog(Side.PLAYER);
				} 
				SetAnimation(type_.DieAnimation);
                CancelInvoke();
				Destroy(minipoint_);
				Destroy(gameObject, .9f);
				//GeneralSystem.ChangeAlpha(gameObject, Color.white, 1);
				StopAllCoroutines();
			}
			catch (System.Exception e)
			{
                Debug.Log(e.Message);
			}
        }
		private void SetAlspa()
		{
			if (gameObject != null)
			{
				GeneralSystem.ChangeAlpha(gameObject, Color.red, 1);
				Invoke("ReSetAlspa", .2f);
			}
		}
		private void ReSetAlspa()
		{
			if(gameObject != null)
				GeneralSystem.ChangeAlpha(gameObject, Color.white, 1);
		}
        private void SetHealthBar()
        {
            foreach (Transform g in transform)
            {
                if (g.name == "health")
                {
                    healthbar = g.gameObject;
                    bar_length_ = healthbar.transform.localScale.x;
                }
            }
        }
        private void RefreshHealthBar()
        {
            if (healthbar != null && !MetalSlugMainGame.instand_.is_end_)
            {
                float scale = 0;
                if (health_ >= 0)
                {
                    scale = (health_ * bar_length_) / total_health_;
                }
                else
                {
                    scale = 0;
                }
                Vector3 health_scale = healthbar.transform.localScale;
                healthbar.transform.localScale = new Vector3(scale, health_scale.y, health_scale.z);
            }
        }

        #region Tank
        private void TankSkill()
        {
            if (isstartmoving_)
            {
                tk2dSprite sprite = gameObject.GetComponent<tk2dSprite>();
                if (sprite.color == Color.white) sprite.color = Color.red;
                else sprite.color = Color.white;
                Invoke("TankSkill", .1f);
            }
        }
        private GameObject GetFristEnemy()
        {
            if (enemy_list_.Count > 0)
            {
                GameObject temp = enemy_list_[0];
                for (int i = 0; i < enemy_list_.Count; i++)
                {
                    if (Vector3.Distance(gameObject.transform.position, enemy_list_[i].transform.position) < Vector3.Distance(gameObject.transform.position, temp.transform.position))
                    {
                        temp = enemy_list_[i];
                    }
                }
                return temp;
            }
                return null;
        }
		private void TestDebug(string s)
		{
			if (type_.GetType() == typeof(Sarubia))
				Debug.Log(s);
		} 
        #endregion
		#region Variable
		public Side side_;
		public SoldierType type_ = null;
        public bool isstartmoving_;
        private Vector3 target_;
        private float total_health_ = 0;
		private float speed_moving_=3;
		private GameObject obj_attack_;
        private SoldierControl enemy_;
        private GameObject minipoint_ = null;
        private List<GameObject> enemy_list_ = null;
        private float distance_ = 0;
		private float health_ = 0;
        private GameObject healthbar = null;
        private float bar_length_ = 0;
		private bool is_skill_ = false;
		private GameObject own_home_ = null;
		private GameObject enemy_home_ = null;
        private bool istankskill = false;
		#endregion
	}
}