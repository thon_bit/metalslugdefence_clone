﻿using UnityEngine;
using System.Collections;

public class MiniMapControl : MonoBehaviour {

    void Awake()
    {
        instance = this;
    }
    public void Initialize(Vector3 ostart, Vector3 oend)
    {
        origin_start_ = ostart;
        origin_end_ = oend;
    }
    public void RefreshLocation(Vector3 origin,GameObject mini)
    {
        float origin_length = origin_end_.x - origin_start_.x;
        float mini_length = mini_end_.x - mini_start_.x;
        float origin_percent = (origin.x - origin_start_.x) / origin_length;
        float mini_point = ((origin_percent * mini_length))+mini_start_.x;
        if(mini!=null)
        mini.transform.localPosition = new Vector3(mini_point, 0, -1);
    }
    public void RefreshPositionRadar(float minX,float maxX,float current)
    {
        float origin_length = maxX - minX;
        float mini_length = mini_end_.x - mini_start_.x;
        float origin_percent = (current-minX) / origin_length;
        float mini_point = ((origin_percent * mini_length)) + mini_start_.x;
        if (radar_ != null)
            radar_.transform.localPosition = new Vector3(mini_point, 0, -1);
    }
    public GameObject GetMiniPoint(Color color)
    {
        GameObject temp = (GameObject)Instantiate(point_prefab_);
        temp.GetComponent<SpriteRenderer>().color = color;
        temp.transform.parent=this.transform;
        temp.transform.localScale = new Vector3(.4f, .4f, 1f);
       // temp.transform.localPosition=Vector3.zero;
        return temp;
    }
    public static MiniMapControl instance = null;
	[SerializeField]
    private GameObject point_prefab_ = null;
    private Vector3 origin_end_ = Vector3.zero;
    [SerializeField]
    private Vector3 mini_start_ = Vector3.zero;
    [SerializeField]
    private Vector3 mini_end_ = Vector3.zero;
    [SerializeField]
    private GameObject radar_=null;
	private Vector3 origin_start_ = Vector3.zero;
}
