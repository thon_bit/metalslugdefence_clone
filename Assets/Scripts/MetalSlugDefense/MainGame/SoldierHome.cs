﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class SoldierHome : MonoBehaviour
	{
        void Awake()
        {
            fullhealth_ = health_bar_.transform.localScale.x;
        }
		public void RefreshLife(float amount)
		{
			life_ -= amount;
			float scale;
			if (life_ >= 0)
			{
				scale = (life_ * fullhealth_) / fulllife_;
				if (gameObject.name == "s_home_L" && life_ <= fulllife_/2f)
				{
					MetalSlugMainGame.instand_.ShowDanger();
				}
				if (!MetalSlugMainGame.instand_.is_boss_scene_ && this != null)
					StartCoroutine(SetAlspa());
			}
			else
				scale = 0; 
			health_bar_.transform.localScale = new Vector3(scale, health_bar_.transform.localScale.y, health_bar_.transform.localScale.z);
			
		}
		public void SetHealth(float life)
		{
			fulllife_= life_ = life;
		}
		public bool IsDie()
		{
			if (life_ <= 0)
			{
				Die();
				return true;
			}
			return false;
		}
		protected virtual void Die()
		{
			
			StopAllCoroutines();
            if (this != null)
            {
				StopAllCoroutines();
                this.enabled = false; // disable this component
                GameObject particle = (GameObject)Instantiate(Resources.Load("Prefabs/Explode"));// the fire of destroyed tower
				particle.transform.localScale = new Vector3(3, 3, 0);
				//particle.GetComponent<ParticleSystem>().startSize = 15;
                particle.transform.position = gameObject.transform.position; // set the position of fire
                Destroy(gameObject, 1.2f); // Destroy the tower
                Destroy(particle, 1f); // Destroy the fire
                Destroy(gameObject, 1);
                this.gameObject.SetActive(false);
            }
		}
		private IEnumerator SetAlspa()
		{
			if (!this.gameObject.activeSelf)
				yield return null;
			if (this != null)
			{
				GeneralSystem.ChangeAlpha(gameObject, Color.red, 1);
				yield return new WaitForSeconds(.2f);
				if (!this.gameObject.activeSelf && this != null)
					GeneralSystem.ChangeAlpha(gameObject, Color.white, 1);
			}
		}
		#region Variable
		[SerializeField]
        private GameObject health_bar_ = null;
        private float fullhealth_;
		private float fulllife_ = 10;
		private float life_ = 10;
		#endregion
	}
}
