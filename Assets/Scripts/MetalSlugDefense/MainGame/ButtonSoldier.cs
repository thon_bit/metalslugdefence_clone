﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class ButtonSoldier : MonoBehaviour
	{

        void Awake()
        {
            if (transform.parent.name != "bt_empty" && transform.parent.name!="bt_stank")
            {
                green_name_ = SoldierType.GetType(transform.parent.name).BtnGreen;
                red_name_ = SoldierType.GetType(transform.parent.name).BtnRed;
				value_ = SoldierType.GetType(transform.parent.name).BtnValue;
			}
			else if (transform.parent.name == "bt_empty")
			{
				green_name_ = "bt_empty";//Eneable
				red_name_ = "bt_empty";	//Disable
			}
            ////Generate the button sprite for Generate soldier
            //if (transform.parent.name == "bt_s2")//it's button of Bazooka soldier
            //{
            //    green_name_ = "bt_s2_a"; //Bazooka enable
            //    red_name_ = "bt_s2";   //Bazooka disble
            //}
            //else if (transform.parent.name == "bt_s3") //it's button of Miner
            //{
            //    green_name_ = "bt_s3_a";//Miner enable
            //    red_name_ = "bt_s3";//Miner disable
            //}
            //else if (transform.parent.name == "bt_empty")//Non Soldier button to generate
            //{
            //    green_name_ = "bt_empty";//Eneable
            //    red_name_ = "bt_empty";	//Disable
            //}
            //else if (transform.parent.name == "bt_s4")
            //{
            //    green_name_ = "bt_green_marco";
            //    red_name_ = "bt_red_marco";
            //}
        }
		// Use this for initialization
		void Start()
		{
			ori_scale_ = transform.localScale;
            if(setvalue_)
            value_label_.text = value_ + "";// the price of Soldier to generate
            value_label_.gameObject.SetActive(true);//show price
            gameObject.transform.GetChild(0).gameObject.SetActive(false);//Disable the progress bar
            if (transform.parent.name == "bt_stank")
			{
				time_reload_ = 25;
				value_label_.gameObject.SetActive(false);
				gameObject.transform.GetChild(0).gameObject.SetActive(true);
				available_ = false;
				transform.localScale = new Vector2(0, ori_scale_.y);
				Invoke("Reload",3f);
			}
        }
		public void SetToEmpty()
		{
			SetSprite("bt_empty");
			value_label_.color = Color.red;
		}
        public void CheckAvailable()//to check it complete progress or not
        {
            if (available_)
            {
                if (MetalSlugMainGame.instand_.current_ap_ >= value_)//the funds is upper than price
                {
                    SetSprite(green_name_);//show Enable bt
                    if (gameObject.transform.parent.collider == null)//parent is the box
                    {
                        gameObject.transform.parent.collider2D.enabled = true;//allow click event  on 2D
                    }
                    else
                    {
                        gameObject.transform.parent.collider.enabled = true; //allow click event in 3D
                    }
					value_label_.color = Color.green;
                }
                else
                {
                    SetSprite(red_name_); //Show disable
                    if (gameObject.transform.parent.collider == null)
                    {
                        gameObject.transform.parent.collider2D.enabled = false;	 //allow click event in 2D
                    }
                    else
                    {
                        gameObject.transform.parent.collider.enabled = false;  //allow click event in 3D
                    }
					value_label_.color = Color.red;
                }
            }
        }
		public bool IsAvailable() // to interface when allow or not allow
		{
			if (available_)
			{	
				available_ = false;//allow one time
                value_label_.gameObject.SetActive(false);//Hide the price
                gameObject.transform.GetChild(0).gameObject.SetActive(true);//Show the Progress
				Reload(); //Re build progress
                MetalSlugMainGame.instand_.current_ap_ -= value_;//Cut down the funds
				return true;
			}
			return false;
		}
		private void Reload()//to Re build progress
		{
			transform.localScale = ori_scale_;
			SetSprite(red_name_);//Show disable
			//Progress animation //change from to ScaleTo
			if (transform.parent.name != "bt_stank")
			{
				iTween.ScaleTo(gameObject, iTween.Hash("x", 0
														, "time", time_reload_
														, "Oncomplete", "Available"
														, "easetype", iTween.EaseType.linear
														, "oncompletetarget", gameObject
														));
			}
			else
			{
				iTween.ScaleFrom(gameObject, iTween.Hash("x", 0
														, "time", time_reload_
														, "Oncomplete", "Available"
														, "easetype", iTween.EaseType.linear
														, "oncompletetarget", gameObject
														));
			}
		}
		private void Available()//set available interface of button
		{
			SetSprite(green_name_);//Show enable
			available_ = true;	//allow click on
            value_label_.gameObject.SetActive(true); //Show the price
            gameObject.transform.GetChild(0).gameObject.SetActive(false);//Hide the progress
		}
		private void SetSprite(string name)	//Set textures
		{
			tk2dSprite tksp = gameObject.transform.parent.GetComponent<tk2dSprite>();
			tksp.spriteId = tksp.GetSpriteIdByName(name);
		}
		#region Variable
		[SerializeField]
		private float time_reload_ = 3;//the time to delay for new reload
        [SerializeField]
        private string green_name_=""; //store sprite name in Avaliable
        [SerializeField]
        private string red_name_ = "";//tore sprite name in Unavaliable
        [SerializeField]
        private int value_ = 0;//the value of price show
        [SerializeField]
        private tk2dTextMesh value_label_ = null;	//Price text
        [SerializeField]
        private bool setvalue_ = true;
		private bool available_ = true;	//enable and disable the click event
		private Vector2 ori_scale_ = Vector2.zero;
		#endregion
	}
}