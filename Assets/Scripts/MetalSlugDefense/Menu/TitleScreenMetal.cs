﻿using UnityEngine;
using System.Collections;
//using System.Windows.Forms;

namespace YNinja.MetalSlug
{
    public class TitleScreenMetal : MonoBehaviour
    {
        /*
         * used in Title Scene
         */ 
        // Use this for initialization
        void Start()
        {
            ClickControl.Initialize(Camera.main);   // initialize instance of ClickControl with main camera
            Initialize();                           // call method to initialize
        }
        void OnEnable()
        {
            ClickControl.OnClick+=OnClickButton;    //register method OnClickButton with event OnClick 
        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton;  //unregister method OnClickButton with event OnClick
        }
        void Update()
        {
            ChangeColorText();
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				QuitDialog();
			}
        }
		private void QuitDialog()
		{
			Application.Quit();
		}
        /// <summary>
        /// method to initialize
        /// </summary>
        private void Initialize()
        {
            if (start_)     // first startup
            {
                start_ = false;                             // set start_ to false
                GeneralSystem.CameraFadeIn(.2f);
                DataSavingXml.DeleteAll();                       // delete all data saved// fade camera in 
            }
            else            // not first startup
            {
                AnimationSystem.HideNavigation();           // animation of hide navigation
            }
            AudioSingleton.Instance().PlayBackground(0);    // play background music index 0
            CheckMusicState();                              // call method to check music state
            CheckSaveData();                                // call method to check on saved data 
        }
        /// <summary>
        /// method used when player click on button
        /// </summary>
        /// <param name="hit">object of button</param>
        private void OnClickButton(Transform hit)
        {
            if (hit != null)                // hit not null
            {
                switch (hit.name)           // condition button's name
                {
                    //case "btPlay": AnimationSystem.ScaleButton(hit.gameObject, "NewGameOperation", gameObject);
                    //                break;
                    //case "btStage2": levelname_ = "BattleBoss";
                    //                AnimationSystem.ShowNavigation("LoadLevel", gameObject);
                    //                break;
                    case "btMusic": AnimationSystem.ScaleButton(hit.gameObject, "SwitchMusic", gameObject); break;                                       // click on Button music to switch icon and state of backgrund music of the game
                }
            }
            else
            {
                NewGameOperation();                   
            }
        }
        private void NewGameOperation()
        {
			levelname_ = "Menu";//"AreaSelect";                                 //click on button New
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);    // animation of show navigation and call method LoadLevel after complete animation
                                    
        }
        /// <summary>
        /// method to switch button music's icon and state of music 
        /// </summary>
        private void SwitchMusic()
        {
            if (AudioSingleton.Instance().bgaudio.mute)         // current state of music is mute
            {
                AudioSingleton.Instance().MusicOn();            // turn volumn on of music
            }
            else                                                // current state of music is not mute
            {
                AudioSingleton.Instance().MusicOff();           // turn volumn off 
            }
            CheckMusicState();                                  // call method to check state of button music
        }
        /// <summary>
        /// method to check state of music
        /// </summary>
        private void CheckMusicState()
        {
            if (AudioSingleton.Instance().bgaudio.mute)         // current music is mute
            {
                btmusic_.sprite = music_[1];                    // change sprite of button to mute music
            }
            else                                                // current music is not mute
            {
                btmusic_.sprite = music_[0];                    // change sprite of button to on music
            }
        }
        /// <summary>
        /// method load new scene
        /// </summary>
        private void LoadLevel()
        {
            // shake camera
            iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("NewLevel", .4f);        // call method NewLevel after .4f delay
        }
        /// <summary>
        /// method to LoadLevel
        /// </summary>
        private void NewLevel()
        {
            Application.LoadLevel(levelname_);      // load level depend of levelname_
        }
        /// <summary>
        ///  method to check save data
        /// </summary>
        private void CheckSaveData()
        {
            //if (DataSaving.HasData())               // has data saved
            //{
            //    btn_text_.text = "Continue";        // change name button to Continue
            //}
            //else                                    // not yet saved
            //{
            //    btn_text_.text = "New Game";        // change name button to New Game
            //}
            //if (!DataSaving.HasData())
            //{
            //    DataSaving.SaveFund(3000);
            //    DataSaving.SaveArmyHp(2, 2, 2, 2);
            //    DataSaving.SaveArmyLevel(0, 0, 0, 0);
            //    DataSaving.SaveShopingArmy(true, false, false, false);
            //    DataSaving.SaveCustomize(false, false, false);
            //    DataSaving.SaveOrder("Soldier", "", "", "", "", "", "", "", "", "");
            //    DataSaving.SaveStage1(false, false, false, false);
            //    DataSaving.SaveStage2(false, false, false, false);
            //    DataSaving.SaveNewStage1(true, true, true, true);
            //    DataSaving.SaveNewStage2(true, true, true, true);
            //}
            if (!DataSavingXml.HasData())
            {
                Debug.Log("no data");
                DataSavingXml data = new DataSavingXml
                {
                    fund = 3000,
                    army = new ArmyData
                    {
                        //soldier,bazooka,miner,marco,tarma,fio,eric,SoldierShield,Sarubia,Arabian,Berserker,Mummy,SwordMan,Licker,Scientist,YoungGirl,Wolf,YoungGirlGreen,Salvaldor
                        hp = new float[] { 2, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,2,2 },
                        level = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0 },
                        price = new int[] { 400, 450, 500, 550, 600, 600, 600, 600, 600, 400, 500, 550, 510, 400, 500, 550, 510,600,350 }
                    },
                    shop = new ShopData
                    {
                        army = new bool[] { true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,false,false }
                    },
                    customize = new CustomizeData
                    {
                        items = new bool[] { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,false,false }
                    },
                    order = new DeckOrderData
                    {
                        orders = new string[] { "Soldier", "", "", "", "", "", "", "", "", "","","","" }
                        ,remains=new string[]{}
                    },
                    areas = new AreaData
                    {
                        area = new StageData[] { 
                            new StageData{
                                isnewarea=true,
                                savedstage=new bool[]{false, false, false, false},
                                newstage=new bool[]{true, true, true, true}
                            },
                            new StageData{
                                isnewarea=true,
                                savedstage=new bool[]{false, false, false, false},
                                newstage=new bool[]{true, true, true, true}
                            },
                            new StageData{
                                isnewarea=true,
                                savedstage=new bool[]{false, false, false, false},
                                newstage=new bool[]{true, true, true, true}
                            },
                            new StageData{
                                isnewarea=true,
                                savedstage=new bool[]{false, false, false, false},
                                newstage=new bool[]{true, true, true, true}
                            },
                            new StageData{
                                isnewarea=true,
                                savedstage=new bool[]{false, false, false, false},
                                newstage=new bool[]{true, true, true, true}
                            }
                        }
                    }
                };
                data.SaveData();
                //DataSavingXml.Set(data);
            }
        }
        private void ChangeColorText()
        {
            // ping pong alpha of the text 
            label_.color = new Color(label_.color.r, label_.color.g, label_.color.b, Mathf.PingPong(Time.time * .5f, 1f));

        }

        #region Variables
		[SerializeField]
        private SpriteRenderer btmusic_ = null;         // button music
        [SerializeField]
        private Sprite[] music_ = null;                 // sprite of button music
        [SerializeField]
        private TextMesh label_ = null;
        //[SerializeField]
        //private TextMesh btn_text_ = null;              // label of button new game
		private string levelname_;                      // name of scene
		private static bool start_ = true;              // boolean to check startup
		
        #endregion
	}
}