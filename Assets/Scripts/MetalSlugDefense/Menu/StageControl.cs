﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace YNinja.MetalSlug
{
    public class StageControl : MonoBehaviour
    {

        void Awake()
        {
            bgname_ = new string[][]{new string[] { "bg_3","bg_1","bg_2","bg_4"},
                                      new string[] { "bg_5", "bg_6", "bg_7", "bg_5_1" },
                                       new string[] {"bg_3","bg_5","bg_7","bg_2"},
                                        new string[] { "bg_8","bg_9","bg_2","bg_4"},
                                      new string[] { "bg_5", "bg_6", "bg_7", "bg_5_1" }};
           // bgname_boss_ = new string[] { "bg_5", "bg_6", "bg_7", "bg_5_1" };
        }
        /*
         * used in StageSelect Scene
         */ 
        // Use this for initialization
        void Start()
		{
            		
        }
        void OnEnable()
        {
            ClickControl.Initialize(Camera.main);               // initialize object of ClickControl
            
            ClickControl.OnClick += OnClickButton;              // register event
            // AnimationSystem.HideNavigation();                   // animation of hidding navigation
            AudioSingleton.Instance().PlayBackground(0);        // play music background
            Invoke("ShowStageBox", .3f);                        // invoke method ShowStageBox in .7s
            foreach (GameObject g in stage_box_)               // loop in stage_box_
                g.transform.parent.gameObject.SetActive(false); // disable all box
            //ResizeStageBox();
            AllowClick();
            //ShowStageBox();
            CheckClear();		
        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton;              // unregister event
        }
        /// <summary>
        /// method to show all box
        /// </summary>
		private void ShowStageBox()
		{
			foreach (GameObject g in stage_box_)                                // loop in stage_box
			{
				g.transform.parent.gameObject.SetActive(true);                  //enable all the box
				HideChild(g.transform.parent.transform);                        // hide child
				AnimationSystem.ShowDialogByScale(g,"CompShoStage",gameObject); // animation of show dialog
            }
            CheckNewStage();
		}
        private void CheckNewStage()
        {
            bool[] isnew;
            //if (levelname_ == "BattleNormal")
            //{
            //    //isnew = DataSaving.LoadNewStage1();
            //    isnew = DataSavingXml.Instance().areas.area[0].newstage;
            //}
            //else
            //{
            //    //isnew = DataSaving.LoadNewStage2();
            //    isnew = DataSavingXml.Instance().areas.area[1].newstage;
            //}
            isnew = DataSavingXml.Instance().areas.area[AreaSelectControl.area_index_].newstage;
            for (int i = 0; i < isnew.Length; i++)
            {
                if (isnew[i])
                {
                    GameObject temp = new GameObject("znew");
                    temp.transform.parent = stage_box_[i].transform.parent.GetChild(0);
                    temp.transform.localPosition = new Vector3(-.3f, 0f, -1f);
                    temp.transform.localScale = new Vector3(2, 2, 1);
                    temp.AddComponent<SpriteRenderer>().sprite = new_icon_;
                }
                else
                {
                    GameObject g=GetChildObject(stage_box_[i].transform.parent.GetChild(0),"znew");
                    if (g != null)
                    {
                        Destroy(g.gameObject);
                    }
                }
            }
        }
        private GameObject GetChildObject(Transform parent, string name)
        {
            foreach (Transform g in parent)
            {
                if (g.name == name)
                {
                    return g.gameObject;
                }
            }
            return null;
        }
        private void ChangeNewState(int index)
        {
            bool[] isnew;
            DataSavingXml data = DataSavingXml.Instance();
            //if (levelname_=="BattleNormal")
            //{
            //    //isnew = DataSaving.LoadNewStage1();
            //    isnew = data.areas.area[0].newstage;
            //    isnew[index] = false;
            //    data.areas.area[0].newstage=isnew;
            //    data.SaveData();
            //    //DataSaving.SaveNewStage1(isnew);
            //    MetalSlugMainGame.bgname_ = bgname_normal_[index];
            //}
            //else
            //{
            //    //isnew = DataSaving.LoadNewStage2();
            //    isnew = data.areas.area[1].newstage;
            //    isnew[index] = false;
            //    data.areas.area[1].newstage = isnew;
            //    data.SaveData();
            //    MetalSlugMainGame.bgname_ = bgname_boss_[index];
            //    //DataSaving.SaveNewStage2(isnew);
            //}
            isnew = data.areas.area[AreaSelectControl.area_index_].newstage;
            isnew[index] = false;
            data.areas.area[AreaSelectControl.area_index_].newstage = isnew;
            data.SaveData();
            //DataSaving.SaveNewStage1(isnew);
            MetalSlugMainGame.bgname_ = bgname_[AreaSelectControl.area_index_][index];

        }
        /// <summary>
        /// method to show box
        /// </summary>
		private void CompShoStage()
		{
			foreach (GameObject g in stage_box_)
			{												   
				ShowChild(g.transform.parent.transform);    // call method showchild 
			}
		}
        /// <summary>
        /// method to show all child
        /// </summary>
        /// <param name="parent"></param>
		private void ShowChild(Transform parent)
		{
			foreach (Transform g in parent)
			{
				if (!stage_box_.Contains(g.gameObject))     // show all child except gameobject in stage_box
				{
					g.gameObject.SetActive(true);
				}
			}
		}
        /// <summary>
        /// method to hide child 
        /// </summary>
        /// <param name="parent"></param>
		private void HideChild(Transform parent)
		{
			foreach (Transform g in parent)
			{
				if (!stage_box_.Contains(g.gameObject))     // hide all child except gameobject in stage_box
				{
					g.gameObject.SetActive(false);
				}
			}
		}
        /// <summary>
        /// method used when player click on button
        /// </summary>
        /// <param name="hit"></param>
		private void OnClickButton(Transform hit)
        {
            if (is_allow_ && hit != null)
            {
                if (hit.name.StartsWith("bt"))                                  // click on button
                {
                    if (hit.name == "btBack")                                   // on button back
                    {
                        if (!start_box_.transform.parent.gameObject.activeSelf)         // mission start dialog not enable
                        {
                            AnimationSystem.ScaleButton(hit.gameObject, "BackOperation", gameObject);   // animation scale and call method BackOperation after complete animation
                        }
                        else                                   // mission start dialog enable
                        {
                            is_allow_ = false;                  
							AnimationSystem.ScaleButton(hit.gameObject);    // animation scale button
                            StartCoroutine(HideStartBox());                 // hide mission start dialog
                        }
                    }
                    else if (hit.name == "btShop")
                    {
                        AnimationSystem.ScaleButton(hit.gameObject, "ShopOperation", gameObject);
                    }
                }
				if(hit.name.StartsWith("Box_Stage"))            // click on Box_stage
				{
					stage_num_ =int.Parse( hit.name.Substring(9))-1;
					is_allow_ = false;
					title_ = hit.transform.parent.transform.GetChild(0).transform.GetChild(0).GetComponent<TextMesh>().text;    // get text of title 
					ShowStartBox();                             //  method to show mission start dialog
				}
                else if (hit.name == "btStart")                 // click on button start in mission start dialog
                {
                    AnimationSystem.ScaleButton(hit.gameObject, "StartOperation", gameObject);          // animation scale and call method StartOperation after animation complete
                }
            }
        }
        private void ShopOperation()
        {
            levelname_ = "Shop";                                  // next scene to AreaSelect
            ShopScreen.back_name_ = "World";
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);    // animation show navigation and call loadlevel
            //is_allow_ = false;
            //StartCoroutine(GoToArea());
        }
        /// <summary>
        /// method called after click on button back
        /// </summary>
        private void BackOperation()
        {
            //levelname_ = "AreaSelect";                                  // next scene to AreaSelect
            //AnimationSystem.ShowNavigation("LoadLevel", gameObject);    // animation show navigation and call loadlevel
            is_allow_ = false;
            StartCoroutine(GoToArea());
        }
        private IEnumerator GoToArea()
        {
            HideStageBox();
            yield return new WaitForSeconds(.3f);
            if (area_select_ != null)
            {
                area_select_.SetActive(true);
                area_select_.GetComponent<AreaSelectControl>().EnableAreas(true);
                gameObject.SetActive(false);
            }	
        }
        /// <summary>
        /// method called after click on button Start
        /// </summary>
        private void StartOperation()
        {				
            ChangeNewState(stage_num_);
			//MenuControl.levelname_ = GetSceneName();//levelname_;                    // assign battle scene to MenuControl
			levelname_ = GetSceneName();//"Menu";                                    // next scene to Menu
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);    // animation show navigation and call loadlevel
				
        }
		private string GetSceneName()
		{
			LevelControl lv = new LevelControl(AreaSelectControl.area_index_, stage_num_);
			if (lv.EnemyDescription.IsBoss)
				return "BattleBoss";
			else
				return "BattleNormal";
		}
        /// <summary>
        /// method to show mission start dialog
        /// </summary>
        private void HideStageBox()
        {
            foreach (GameObject g in stage_box_)            // animation to hide Stage Box
            {
                HideChild(g.transform.parent.transform);
                AnimationSystem.HideDialogByScale(g);
            }
        }
        private void ShowStartBox()
		{
            HideStageBox();
			start_box_.transform.parent.gameObject.SetActive(true);                         //show mission start dialog
			start_box_.transform.parent.transform.GetChild(0).gameObject.SetActive(false);  
			AnimationSystem.ShowDialogByScale(start_box_, "AllowClick", gameObject);
			//Change image
            //if (levelname_ == "BattleBoss")
            //{
            start_box_.transform.parent.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().sprite = GetSpriteByName(bgname_[AreaSelectControl.area_index_][stage_num_]);//bg_stage_boss_[stage_num_]; //boss_sprite_bg_;	
            //}
            //else
            //{
            //    start_box_.transform.parent.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().sprite = bg_stage_normal_[stage_num_]; //boss_sprite_bg_;	
            //}
			//Show level log
			lv_logo_.GetComponent<SpriteRenderer>().sprite = lv_sprite_[stage_num_];
			lv_logo_.SetActive(true);
			Vector3 sc = new Vector3(30,30,0);
			iTween.ScaleFrom(lv_logo_, iTween.Hash("scale", sc, "time", .4f));
		}
		
		private IEnumerator HideStartBox()
		{
			lv_logo_.SetActive(false);
			start_box_.transform.parent.transform.GetChild(0).gameObject.SetActive(false);
			AnimationSystem.HideDialogByScale(start_box_);
			yield return new WaitForSeconds(.2f);
			ShowStageBox();
			start_box_.transform.parent.gameObject.SetActive(false);
			start_box_.transform.localScale = new Vector3(25, 20, 0);
			yield return new WaitForSeconds(.4f);
			is_allow_ = true;

		}
		private void CheckClear()
		{
			bool[] clear;
            clear=DataSavingXml.Instance().areas.area[AreaSelectControl.area_index_].savedstage;
			for (int i = 0; i < clear.Length; i++)
			{
                //if (clear[i])
                //{
                //    //GetClearLogo(stage_box_[i]);
					stage_box_[i].transform.GetChild(0).gameObject.SetActive(clear[i]);
                //}
                //else
                //{
                //    stage_box_[i].transform.GetChild(0).gameObject.SetActive(fa);
                //}
			}
		}
		private GameObject GetClearLogo(GameObject parent)
		{
			GameObject temp = (GameObject)Instantiate(clear_logo_);
			temp.transform.parent = parent.transform;
			temp.transform.position = new Vector3(14,-.5f,-3);
			temp.SetActive(true);
			return temp;
		}
        private void ResizeStageBox()
        {
            foreach (GameObject g in stage_box_)
            {
                g.transform.localScale = new Vector3(7, 7, 0);
                g.transform.parent.gameObject.SetActive(false);
            }
        }
        private void AllowClick()
		{
            ResizeStageBox();
			start_box_.transform.parent.transform.GetChild(0).gameObject.SetActive(true);
			start_box_.transform.parent.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<TextMesh>().text = title_;
			is_allow_ = true;
		}
        private void LoadLevel()
        {
            iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("NewLevel", .4f);
        }
        private void NewLevel()
		{
            Application.LoadLevel(levelname_);
        }
        private Sprite GetSpriteByName(string name)
        {
            foreach (Sprite img in lst_stage_bg_)
            {
                if (img.name == name)
                {
                    return img;
                }
            }
            return null;
        } 
		#region Variables
		public static string levelname_ = "";
		public static int stage_num_ = 0;
     
        private string[][] bgname_ = null;
        //private string[] bgname_boss_ = null;
		[SerializeField]
		private List<GameObject> stage_box_ = null;
		[SerializeField]
		private GameObject start_box_ = null;
        //[SerializeField]
        //private Sprite boss_sprite_bg_ = null;
		[SerializeField]
		private GameObject clear_logo_ = null;
        [SerializeField]
        private Sprite new_icon_ = null;
		[SerializeField]
		private GameObject lv_logo_ = null;
		[SerializeField]
		private List<Sprite> lv_sprite_ = null;
		[SerializeField]
		private List<Sprite> lst_stage_bg_ = null;
        [SerializeField]
        private GameObject area_select_ = null;
        //[SerializeField]
        //private List<Sprite> bg_stage_normal_ = null;
		private bool is_allow_ = true;
		private string title_;
		#endregion
	}
}