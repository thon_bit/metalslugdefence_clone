﻿using UnityEngine;
using System.Collections;

public class BoxLevelControl : MonoBehaviour {
    public GameObject Item { get { return sprite_.gameObject; } }
    public TextMesh Price { get { return price_.transform.GetChild(0).GetComponent<TextMesh>(); } }
    public TextMesh Level { get { return level_.transform.GetChild(0).GetComponent<TextMesh>(); } }
    public GameObject PopUp { get { return popup_; } }
    public GameObject ButtonUp { get { return btn_up_; } }
    public void Initialize(GameObject target,string methodname,string title,int level,int price,string spritename,string animationname)
    {
        title_.text = title;
        level_.transform.GetChild(0).GetComponent<TextMesh>().text = level + "";
        price_.transform.GetChild(0).GetComponent<TextMesh>().text = price + "";
        sprite_.spriteId = sprite_.GetSpriteIdByName(spritename);
        sprite_.gameObject.GetComponent<tk2dSpriteAnimator>().Play(animationname);
        btn_up_.GetComponent<tk2dUIItem>().sendMessageTarget = target;
        btn_up_.GetComponent<tk2dUIItem>().SendMessageOnClickMethodName = methodname;
    }
    
    #region vairiables
    [SerializeField]
    private TextMesh title_ = null;
    [SerializeField]
    private TextMesh level_ = null;
    [SerializeField]
    private TextMesh price_ = null;
    [SerializeField]
    private tk2dSprite sprite_ = null;
    [SerializeField]
    private GameObject popup_ = null;
    [SerializeField]
    private GameObject btn_up_ = null;
    #endregion
}
