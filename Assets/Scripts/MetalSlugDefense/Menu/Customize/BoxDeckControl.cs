﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class BoxDeckControl : MonoBehaviour
	{

		public GameObject Item { get { return sprite_.gameObject; } }
		public tk2dSprite BoxBG { get { return box_; } }
		public void Initialize(GameObject target, string methodname, string textup,string textdown,string nameboxbg)
		{
			txt_down_.text = textdown;
			txt_up_.text = textup;
			box_.spriteId = box_.GetSpriteIdByName(nameboxbg);
			box_.gameObject.GetComponent<tk2dUIItem>().sendMessageTarget = target;
			box_.gameObject.GetComponent<tk2dUIItem>().SendMessageOnDownMethodName = methodname;
		}

		#region vairiables
		[SerializeField]
		private TextMesh txt_down_ = null;
		[SerializeField]
		private TextMesh txt_up_ = null;
		[SerializeField]
		private tk2dSprite sprite_ = null;
		[SerializeField]
		private tk2dSprite box_ = null;
		#endregion
	}
}
