﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class BoxSellControl : MonoBehaviour
	{
		public void Initialize(string title,GameObject target, string methodname,int price, string spritename, string animationname)
		{
			price_.text = price + "";
			sprite_.spriteId = sprite_.GetSpriteIdByName(spritename);
			sprite_.gameObject.GetComponent<tk2dSpriteAnimator>().Play(animationname);
			btn_buy_.GetComponent<tk2dUIItem>().sendMessageTarget = target;
			btn_buy_.GetComponent<tk2dUIItem>().SendMessageOnClickMethodName = methodname;
			title_.text = title;
		}
		public GameObject Item { get { return sprite_.gameObject; } }
		public TextMesh Price { get { return price_.GetComponent<TextMesh>(); } }
		public GameObject ButtonBuy { get { return btn_buy_; } }

		#region Variables
		[SerializeField]
		private TextMesh price_ = null;
		[SerializeField]
		private tk2dSprite sprite_ = null;
		[SerializeField]
		private GameObject btn_buy_ = null;
		[SerializeField]
		private TextMesh title_ = null;
		#endregion
	}
}