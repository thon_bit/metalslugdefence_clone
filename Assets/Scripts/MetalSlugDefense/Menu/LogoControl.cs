﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class LogoControl : MonoBehaviour {

		// Use this for initialization
		void Start () {
			FadeIn();
		}
		private void FadeIn()
		{
			GeneralSystem.CameraFadeIn(1.5f,gameObject,"FadeOut");
		}
		private void FadeOut()
		{
			Invoke("ShowFadeOut",2f);
		}
		private void ShowFadeOut()
		{
			GeneralSystem.CameraFadeOut(.5f,gameObject,"Complete");
		}
		private void Complete()
		{
			Application.LoadLevel("Title");
		}
	}
}