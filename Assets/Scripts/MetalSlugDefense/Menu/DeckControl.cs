﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YNinja.MetalSlug
{
  
    public class DeckControl : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            AnimationSystem.HideNavigation();
			Initailize();
			SetFocusBox(0);
			focus_clone_ = (GameObject)Instantiate(focus_);
            fund_.text = "" + DataSavingXml.Instance().fund;// DataSaving.LoadFund();
        }

        // Update is called once per frame
        void Update()
        {

            if (!isholding_)
            {
                HoldItem();
            }
            else
            {
                ReleaseItem();
            }
			MoveItem();
        }
        private void ReleaseItem()
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (item_hold_ != null)
                {
					int getindex = CheckLocation();
                    if (getindex >= 0)
                    {
						if(is_add_new_item_)
						{
							if (getindex == -1)
							{
								ClearItem();
							}
							else
							{
								StartCoroutine(ShowShadyFocus());
								//Switch Object
								GameObject temp = item_[getindex];
								if (temp.name != "Non")
								{
									int boxindex = GetBoxIndex(temp.name);
									box_index_ = boxindex;
									is_allow_box_[box_index_] = true;
									ConvertBox(false);
								}
								else
								{
									point_[getindex].GetChild(0).GetComponent<TextMesh>().color = Color.green;
									num_using_order_++;
								}
								//temp.transform.position = Vector3.zero;
								Destroy(temp.gameObject);
								item_[getindex] = item_hold_.gameObject;
								current_index_ = getindex;
							}
						}
						else if (current_index_ != getindex)
						{
							StartCoroutine(ShowShadyFocus());
							//Switch Object
							GameObject temp = item_[getindex];
							//temp.transform.position = point_[current_index_].position;
							item_[getindex] = item_hold_.gameObject;
							item_[current_index_] = temp;
							//Switch Color
							if (temp.name == "Non")
							{
								point_[current_index_].GetChild(0).GetComponent<TextMesh>().color = Color.red;
								point_[getindex].GetChild(0).GetComponent<TextMesh>().color = Color.green;
								temp.transform.position = point_[current_index_].position;
							}
							else
							{
								MoveOnItween(temp, point_[current_index_].position);
								SetOnSarubia(temp);
							}
						}
                       // item_hold_.transform.position = point_[getindex].position;
						MoveOnItween(item_hold_.gameObject, point_[getindex].position);
						SetOnSarubia(item_hold_.gameObject);
						item_hold_.GetComponent<tk2dSpriteAnimator>().Play(anim_idle_name);
                    }
                    else
					{
						if (getindex == -1 && !is_add_new_item_)
						{
							//item_hold_.transform.position = point_[current_index_].position;
							ResetLaseBack();
						}
						else
						{
							if (is_add_new_item_)
							{
								ClearItem();
							}
							else if (num_using_order_ > 1)
							{
								ClearItem();
								num_using_order_--;
							}
							else
							{
								ResetLaseBack();
							}
						}
					}
                    isholding_ = false;
					scroll_.ScrollEnable(true);
					if (is_add_new_item_)
						is_add_new_item_ = false;
                }
            }
        }
		private void ResetLaseBack()
		{
			MoveOnItween(item_hold_.gameObject, point_[current_index_].position);
			SetOnSarubia(item_hold_.gameObject);
			if(anim_idle_name!="")
				item_hold_.GetComponent<tk2dSpriteAnimator>().Play(anim_idle_name);
		}
		private void ClearItem()
		{
			Destroy(item_hold_.gameObject);
			if (!is_add_new_item_)
			{
				GameObject temp = CreateNonReplace();
				temp.transform.position = point_[current_index_].transform.position;
				item_[current_index_] = temp;
				point_[current_index_].GetChild(0).GetComponent<TextMesh>().color = Color.red;
			}
			ConvertBox(false);
			is_allow_box_[box_index_] = true;
		}
		private IEnumerator ShowShadyFocus()
		{
			if (!is_add_new_item_)
			{
				Vector3 pos = point_[current_index_].position;
				pos.y = pos.y + 11;
				focus_clone_.transform.position = pos;
				focus_clone_.SetActive(true);
			}
			yield return new WaitForSeconds(.2f);
			if(focus_clone_.activeSelf)
				focus_clone_.SetActive(false);
			focus_.SetActive(false);
		}
        private void HoldItem()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Transform temp = GeneralSystem.ClickOnObject(Camera.main);
                if (temp != null)
                {
                    if (item_.Contains(temp.gameObject))
                    {
                        for (int i = 0; i < item_.Count; i++)
                        {
                            if (temp.gameObject == item_[i])
                            {
                                current_index_ = i;
                            }
                        }
                        ShowFocusBox(temp.name);
                        item_hold_ = temp;
                        isholding_ = true;
						//set animation move
						tk2dSpriteAnimator tkanim = temp.GetComponent<tk2dSpriteAnimator>();
						anim_idle_name = tkanim.CurrentClip.name;
						tkanim.Play(SoldierType.GetType(temp.name).MoveAnimation);
						ChangeScale(temp.gameObject);
                    }
                    else if (temp.name == "btBack")
                    {
                        AnimationSystem.ScaleButton(temp.gameObject, "GoToButtonBack", gameObject);
                    }
					else if (temp.name == "btCustomize")
					{
                        AnimationSystem.ScaleButton(temp.gameObject, "GoToButtonCustomize", gameObject);
					}
					else if (temp.name == "btShop")
					{
                        AnimationSystem.ScaleButton(temp.gameObject, "GoToButtonShop", gameObject);
					}
                }
            }
        }
        private void GoToButtonBack()
        {
            SaveOrder();
            level_load_ = "Customize";
            CustomizeControl.currentlevel_ = area_select_;
            AnimationSystem.ShowNavigation("BackBT", gameObject);
        }
        private void GoToButtonCustomize()
        {
            SaveOrder();
            level_load_ = "Customize";
            CustomizeControl.currentlevel_ = area_select_;
            AnimationSystem.ShowNavigation("BackBT", gameObject);
        }
        private void GoToButtonShop()
        {
            SaveOrder();
            level_load_ = "Shop";
            ShopScreen.temp_levelname = area_select_;
            AnimationSystem.ShowNavigation("BackBT", gameObject);
        }
		private void ClickOnBox()
		{
			GameObject g = GeneralSystem.ClickOnObject3D(Camera.main).gameObject;
			int getindex = 0;
			for (int i = 0; i < scroll_.box_.Count; i++)
			{
				if (scroll_.box_[i].gameObject == g.transform.parent.gameObject)
				{
					getindex = i;
				}
			}
			if (is_allow_box_[getindex])
			{
				string boxname = g.transform.parent.name;
				CreateNewItem(boxname);
				box_index_ = getindex;
				is_allow_box_[getindex] = false;
				ConvertBox(true);
				scroll_.ScrollEnable(false);
			}
		}
		private int GetBoxIndex(string itemname)
		{					 
			for (int i = 0; i < scroll_.box_.Count; i++)
			{
				string boxname = scroll_.box_[i].name;
				boxname = boxname.Substring(0, boxname.LastIndexOf('_'));
				if (boxname == itemname)
				{
					return i;
				}
			}
			return box_index_;
		}
		private void CreateNewItem(string boxname)
		{
            string itemname = boxname.Substring(0, boxname.LastIndexOf('_'));
            SoldierType type = SoldierType.GetType(itemname);
			//for (int i = 0; i < item_.Count; i++)
			//{
			//	if (item_[i].name == "Non")
			//	{
			//		current_index_ = i;
			//		Destroy(item_[i].gameObject);
			//		isholding_ = true;
			//		break;
			//	}
			//}
			//GameObject temp = item_[current_index_].gameObject;
			//Destroy(temp);
			isholding_ = true;
			is_add_new_item_ = true;
			item_hold_ = CreateItem(type.SpritName, type.MoveAnimation).transform;
			item_hold_.transform.position = new Vector3(item_hold_.transform.position.x, item_hold_.transform.position.y,  - 5);
			item_hold_.name = itemname;
			if (itemname == "Sarubia")
			{
				item_hold_.transform.localScale = new Vector3(7, 7, 0);
			}
			//current_index_ = -2;
			//item_[current_index_] = item_hold_.gameObject;
			//point_[current_index_].GetChild(0).GetComponent<TextMesh>().color = Color.green;
			if (!item_hold_.gameObject.activeSelf) item_hold_.gameObject.SetActive(true);
			Vector3 sc =new Vector3(20,20,0);
			ChangeScaleBoxcollider(item_hold_.gameObject);
			iTween.ScaleFrom(item_hold_.gameObject, iTween.Hash("scale", sc, "time", 1));
			//set animation move
			anim_idle_name = type.IdleAnimation;
		}
		private void SaveOrder()
		{
			string[] data = new string[item_.Count];
            DataSavingXml dataxml = DataSavingXml.Instance();
			for (int i = 0; i < item_.Count; i++)
			{
				if (item_[i].name != "Non")
				{
					data[i] = item_[i].name;
				}
				else
				{
					data[i] = "";
				}
			}
			//DataSaving.SaveOrder(data);
            dataxml.order.orders = data;
			List<string> remainsold = new List<string>();
			for (int i = 0; i < is_allow_box_.Count; i++)
			{
				if (is_allow_box_[i])
				{
					string name = scroll_.box_[i].gameObject.name;
					name = name.Substring(0, name.Length - 4);
					remainsold.Add(name); Debug.Log(name);
				}
			}
			if (remainsold.Count > 0)
			{
				//DataSaving.SaveRemainSoldier(remainsold.ToArray());
                dataxml.order.remains = remainsold.ToArray();
			}
			else
			{
				//PlayerPrefs.DeleteKey("MetalRemainSoldier");
                dataxml.order.remains = new string[] { };
			}
            dataxml.SaveData();
		}
		private void BackBT()
		{
            iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("GoBack", .4f);
		}
        private void GoBack()
        {
            Application.LoadLevel(level_load_);
        }
        private void MoveItem()
        {
            if (isholding_)
            {
                if (Input.GetMouseButton(0))
                {
                    Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					if (temp.y > 11)
						temp.y = 11;
					else if (temp.y < -10)
						temp.y = -10;
                    item_hold_.transform.position = new Vector3(temp.x, temp.y, 1);
					//Shadow
					int getindex = CheckLocation();
					if (getindex != current_index_ && getindex >=0)//!= -1)
					{
						if (!focus_.activeSelf) focus_.SetActive(true);
						Vector3 pos = point_[getindex].position;
						pos.y = pos.y + 11;
						focus_.transform.position = pos;
					}
					else if (is_add_new_item_ && getindex >=0)//!= -1)
					{
						if (!focus_.activeSelf) focus_.SetActive(true);
						Vector3 pos = point_[getindex].position;
						pos.y = pos.y + 11;
						focus_.transform.position = pos;
					}
					else
					{
						if (focus_.activeSelf) focus_.SetActive(false);
					}
                }
            }
        }
        private int CheckLocation()
        {
			for (int i = 0; i < point_.Length;i++ )
			{
                if (item_hold_.transform.position.y > -2f)								 
                {
                    if (item_hold_.transform.position.x > point_[i].position.x - 2f && item_hold_.transform.position.x < point_[i].position.x + 2f)
                    {
                        return i;
                    }
                }
                else
                {
                    return -2;
                }
			}
			return -1;
        }
		private void OnScroll()
		{										 
			//if (is_allow_swap_)                             // allow to swipe
			//{
			ResetPanelIndex();
			//}
		}
		private void OnRelease()
		{
			scroll_.OnRelease();
			//ResetPanelIndex();
			//Invoke("ResetPanelIndex",.3f);
		}
		private void OnClick()
		{
			scroll_.OnClick();
		}
		private void ResetPanelIndex()
		{
			int getindex = scroll_.OnScroll();
			if (box_index_ != getindex)
			{
				SetFocusBox(getindex);
				box_index_ = getindex;
			}
		}
        private void ShowFocusBox(string name)
        {
            int clickind = 0;
            for (int i = 0; i < scroll_.box_.Count; i++)
            {
                if (scroll_.box_[i].name.StartsWith(name))
                {
                    clickind = i;
                }
            }
			scroll_.ResetFucus(clickind);
            box_index_ = clickind;
            SetFocusBox(box_index_);
        }
		private void SetFocusBox(int index)
		{
			for (int i = 0; i < scroll_.box_.Count; i++)
			{
				if (i == index)
				{
					for (int j = 2; j < scroll_.box_[i].transform.childCount; j++)
					{
						scroll_.box_[i].transform.GetChild(j).gameObject.SetActive(true);
					}
					//if (is_allow_box_[i] && !scroll_.box_[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled)
					//{
					//	scroll_.box_[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
					//}
				}
				else
				{
					for (int j = 2; j < scroll_.box_[i].transform.childCount; j++)
					{
						scroll_.box_[i].transform.GetChild(j).gameObject.SetActive(false);
					}
					//if (scroll_.box_[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled)
					//{
					//	scroll_.box_[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
					//}
				}
			}
		}
		private void SetOnSarubia(GameObject sar)
		{
			if (sar.name == "Sarubia")
			{
				//Vector3 pos = new Vector3(sar.transform.position.x, sar.transform.position.y + 1.1f, sar.transform.position.z);
				//sar.transform.position = pos;
				//iTween.MoveTo(sar, iTween.Hash("position", pos, "time", 1));
				sar.transform.localScale = new Vector3(7, 7, 0);
				sar.GetComponent<BoxCollider2D>().size = new Vector2(.7f, 1);
			}
			//else if (sar.name == "YoungGirl" || sar.name == "Wolf" || sar.name == "Soldier")
			//{
			//	sar.transform.localScale = new Vector3(5, 5, 0);
			//}
		}
		private void MoveOnItween(GameObject g, Vector3 position)
		{
			if (g.name == "Sarubia")
			{
				position.y = position.y + 1.1f;
			}
			iTween.MoveTo(g, iTween.Hash("position", position, "time", .5f));
		}
		private void Initailize()
		{
			item_ = new List<GameObject>();
            string[] load_item = DataSavingXml.Instance().order.orders;//DataSaving.LoadOrder();
			GameObject item = null;
			GameObject box = null;
			Vector3 posbox = Vector3.zero;
			SoldierType type = null;
			scroll_.box_ = new List<GameObject>();
			is_allow_box_ = new List<bool>();
			for (int i = 0; i < point_.Length; i++)
			{
				if (i < load_item.Length && load_item[i] != "")
				{
                    type = SoldierType.GetType(load_item[i]);
					if (type != null)
					{
						item = CreateItem(type.SpritName, type.IdleAnimation);//type.MoveAnimation);
						item.name = load_item[i];
						item.transform.position = point_[i].position;
						point_[i].transform.GetChild(0).GetComponent<TextMesh>().color = Color.green;
						box = CreateBox(load_item[i], "Lv: "+(type.Level+1)+"/HP: "+type.Health, type.BtnRed);
						is_allow_box_.Add(false);
						if (posbox == Vector3.zero)
							posbox = box.transform.position;
						else
						{
							posbox.x += gap_;
							box.transform.position = posbox;
						}
						SetTransformSarubia(item);
						ChangeScaleBoxcollider(item);
						num_using_order_++;//encrease the number of using item in ordering
					}
					else
					{
						item = CreateNonSet();
						item.transform.position = point_[i].position;
					}
				}
				else
				{
					item = CreateNonSet();
					item.transform.position = point_[i].position;
				}
				item_.Add(item);
			}
			CheckItemRemain();
		}
		private void CheckItemRemain()
		{
            string[] remain = DataSavingXml.Instance().order.remains;//DataSaving.LoadRemainSoldier();
			Debug.Log(remain.Length);
			if (remain.Length > 0)
			{
				GameObject box;
				Vector3 pos = Vector3.zero;
				SoldierType type = null;
				for (int i = 0; i < remain.Length; i++)
				{
                    type = SoldierType.GetType(remain[i]);
					if (type != null)
					{
						box = CreateBox(remain[i], "Lv: " + (type.Level + 1) + "/HP: " + type.Health, type.BtnGreen);
						is_allow_box_.Add(true);
						box.transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
						if (scroll_.box_.Count==1)
							pos = box.transform.position;
						else
						{
							pos = scroll_.box_[scroll_.box_.Count - 2].gameObject.transform.position;
							pos.x += gap_;
							box.transform.position = pos;
						}
					}
				}
			}
		}
		private void SetTransformSarubia(GameObject item)
		{
			if (item.name == "Sarubia")
			{
				item.transform.position = new Vector3(item.transform.position.x, item.transform.position.y + 1.1f, item.transform.position.z);
				item.transform.localScale = new Vector3(7, 7, 0);
				item.GetComponent<BoxCollider2D>().size = new Vector2(.7f, 1);
			}
			//else if (item.name == "YoungGirl" || item.name == "Wolf" || item.name == "Soldier")
			//{
			//	item.transform.localScale = new Vector3(5, 5, 0);
			//	Destroy(item.GetComponent<BoxCollider2D>());
			//	item.AddComponent<BoxCollider2D>();
			//}
		}
		private GameObject CreateItem(string spritname, string anim)
		{
			GameObject temp;
			temp = (GameObject)Instantiate(template_item_);
			tk2dSprite sp = temp.GetComponent<tk2dSprite>();
			sp.spriteId = sp.GetSpriteIdByName(spritname);
			temp.GetComponent<tk2dSpriteAnimator>().Play(anim);
			if (!temp.activeSelf) temp.SetActive(true);
			return temp;
		}
		private GameObject CreateNonSet()
		{
			GameObject temp;
			temp = (GameObject)Instantiate(non_set_);
			temp.name = "Non";
			if (!temp.activeSelf) temp.SetActive(true);
			temp.GetComponent<tk2dSpriteAnimator>().Play();
			return temp;
		}
		private GameObject CreateNonReplace()
		{
			GameObject temp = null;
			foreach (GameObject g in item_)
			{
				if (g.name == "Non")
				{
					temp = (GameObject)Instantiate(g);
					temp.name = "Non";
					tk2dSpriteAnimator tkanim = temp.GetComponent<tk2dSpriteAnimator>();
					tkanim.PlayFrom(g.GetComponent<tk2dSpriteAnimator>().ClipTimeSeconds);
					break;
				}
			} 
			if (temp == null) return CreateNonSet();
			return temp;
		}
		private GameObject CreateBox(string itemname, string lvhp, string boxnamebg)
		{
			GameObject temp = (GameObject)Instantiate(box_prefab_);
			BoxDeckControl box = temp.GetComponent<BoxDeckControl>();
            temp.name = itemname + "_box";
			box.Initialize(gameObject,"ClickOnBox",itemname,lvhp,boxnamebg);
			scroll_.box_.Add(temp);
			temp.transform.position = default_pos_;
			return temp;
		}
		private void ConvertBox(bool istored)
		{

			tk2dSprite sp =	scroll_.box_[box_index_].transform.GetChild(0).GetComponent<tk2dSprite>();
			string boxname = scroll_.box_[box_index_].name;
            string itemname = boxname.Substring(0, boxname.LastIndexOf('_'));
            SoldierType type = SoldierType.GetType(itemname);
			if (istored)
			{
				sp.spriteId = sp.GetSpriteIdByName(type.BtnRed);
			}
			else
			{
				sp.spriteId = sp.GetSpriteIdByName(type.BtnGreen);
				if (!scroll_.box_[box_index_].transform.GetChild(0).GetComponent<BoxCollider>().enabled)
					scroll_.box_[box_index_].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
			}
		}
		private void ChangeScaleBoxcollider(GameObject item)
		{
			if (item.name == "YoungGirl" || item.name == "Wolf" || item.name=="YoungGirlGreen")
			{
				item.transform.localScale = new Vector3(5, 5, 0);
				Destroy(item.GetComponent<BoxCollider2D>());
				item.AddComponent<BoxCollider2D>();
			}
			//if (item.name == "Marco")
			//	item.transform.localScale = new Vector3(10, 10, 0);
		}
		private void ChangeScale(GameObject item)
		{
			if (item.name == "Marco")
			{
				item.transform.localScale = new Vector3(5, 5, 0);
			}
		}
		#region Variables
		[SerializeField]
		private GameObject template_item_ = null;
        [SerializeField]
        private Transform[] point_ = null;
		[SerializeField]
		private GameObject non_set_ = null;
		[SerializeField]
		private GameObject box_prefab_ = null;
		[SerializeField]
		private ScrollBoxControl scroll_ = null;
		[SerializeField]
		private Vector3 default_pos_ = Vector3.zero;
		[SerializeField]
		private GameObject focus_ = null;
		[SerializeField]
		private TextMesh fund_ = null;
		private GameObject focus_clone_ = null;
		private bool isholding_ = false;
		private Transform item_hold_ = null;
		private float gap_ = 7;
		private List<GameObject> item_ = null;
		private int current_index_;
		private int box_index_;
		private string level_load_;
		public static string area_select_;
		private List<bool> is_allow_box_ = null;
		private bool is_add_new_item_ = false;
		private Vector3 target_swith_ = Vector3.zero;
		private int num_using_order_ = 0;
		private string anim_idle_name = "";
		#endregion
	}
}