﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class AreaSelectControl : MonoBehaviour
    {
        /*
         * used in AreaSelect Scene
         */ 
        // Use this for initialization
        void Start()
		{
            Initialize();   // call initialize
			if(!is_from_menu_)
			{
				EnableAreas(false);
			}
        }
        void OnEnable()
        {
            ClickControl.OnClick += OnClickButton;      // register method with event click on button
            Debug.Log("sdf"); 

        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton;      // unregister method with event click on button
        }
        /// <summary>
        /// method to initialize
        /// </summary>
        private void Initialize()
        {
            ClickControl.Initialize(Camera.main);           // initialize instance of ClickControl with camera
            AnimationSystem.HideNavigation();               // animation to hide navigation animation
            foreach (GameObject bt in btstage)
            {
                AnimationSystem.PunchItemRotation(bt);   // animation to rotate object of stage 1 on the map
            }
            //AnimationSystem.PunchItemRotation(btstage1_);   // animation to rotate object of stage 1 on the map
            //AnimationSystem.PunchItemRotation(btstage2_);   // animation to rotate object of stage 2 on the map
            AudioSingleton.Instance().PlayBackground(0);    // play background music index 0
            //CheckAvailableStage();
        }
        /// <summary>
        /// method used when player clicked
        /// </summary>
        /// <param name="hit"> object of button </param>
        private void OnClickButton(Transform hit)
        {
            if (hit != null && is_allow_)       // check hit null or not and allow to click
            {
                if (hit.gameObject.name.StartsWith("bt")) is_allow_ = false;        // set is_allow to false when button's name start with bt
                switch (hit.transform.name)             // condition of button's name
                {
                    case "btBack": AnimationSystem.ScaleButton(hit.gameObject, "BackOperation", gameObject); break;                    // click on button back , set level_name to Title
                    //case "btStage1": AnimationSystem.ScaleButton(hit.gameObject, "Stage1Operation", gameObject); break;                // set level_name to StageSelect
                    //case "btStage2": AnimationSystem.ScaleButton(hit.gameObject, "Stage2Operation", gameObject); break;             // set level_name to StageSelect
                    //case "btStage3": AnimationSystem.ScaleButton(hit.gameObject, "Stage3Operation", gameObject); break;             // set level_name to StageSelect
                    case "btShop": AnimationSystem.ScaleButton(hit.gameObject, "ShopOperation", gameObject); break;
                }
                if (hit.transform.name.StartsWith("btStage"))
                {
                    area_index_ = int.Parse(hit.transform.name.Substring(hit.transform.name.Length - 1))-1;
                    AnimationSystem.ScaleButton(hit.gameObject, "StageOperation", gameObject);             // set level_name to StageSelect
                }
            }
        }
        private void StageOperation()
        {
            StageControl.levelname_ = "BattleNormal";      // store battle scene's name to play in StageControl
            level_name_ = "StageSelect";
            EnableAreas(false);
            is_allow_ = true;
        }
        private void CheckAvailableStage()
        {
            DataSavingXml data = DataSavingXml.Instance();
            for (int i = 1;i< btstage.Length; i++)
            {
                bool[] clearstage1 = data.areas.area[i-1].savedstage;
                bool show = true;
                foreach (bool stage in clearstage1)
                {
                    Debug.Log(stage);
                    if (!stage)
                    {
                        show = false;
                    }
                }

                if (show)
                {
                    btstage[i].SetActive(true);
                    if (data.areas.area[i].isnewarea)
                    {
                        btstage[i].transform.GetChild(0).gameObject.SetActive(false);
                        GameObject temp = new GameObject("new");
                        temp.transform.parent = btstage[i].transform;
                        temp.transform.localPosition = new Vector3(1, 1, 0);
                        temp.transform.localScale = new Vector3(10, 10, 1);
                        temp.AddComponent<SpriteRenderer>().sprite = new_icon_;
                        data.areas.area[i].isnewarea = false;
                    }
                }
                else
                {
                    btstage[i].SetActive(false);
                }
            }
            data.SaveData();
            //bool[] clearstage1 = DataSaving.LoadStage1();
            
        }
        private void BackOperation()
        {
			level_name_ = "Menu";//"Title";
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);            // play animation to show navigation and call method LoadLevel after complete animation 
        }
        private void ShopOperation()
        {
            level_name_ = "Shop";//"Title";
            ShopScreen.back_name_ = "World";
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);            // play animation to show navigation and call method LoadLevel after complete animation
        }
        //private void Stage1Operation()
        //{
        //    area_index_ = 0;
        //    StageControl.levelname_ = "BattleNormal";      // store battle scene's name to play in StageControl
        //    level_name_ = "StageSelect";
        //    EnableAreas(false);
        //    is_allow_ = true;
        //    //AnimationSystem.ShowNavigation("LoadLevel", gameObject);            // play animation to show navigation and call method LoadLevel after complete animation
        //}
        //private void Stage2Operation()
        //{
        //    area_index_ = 1;
        //    StageControl.levelname_ = "BattleBoss";        // store battle boss scene's name to play in StageControl
        //    level_name_ = "StageSelect";
        //    EnableAreas(false);
        //    is_allow_ = true;
        //    //AnimationSystem.ShowNavigation("LoadLevel", gameObject);            // play animation to show navigation and call method LoadLevel after complete animation
        //}
        public void EnableAreas(bool isshow)
        {
            ChangeTopSprite(isshow);
            ScaleBackground(isshow);
            if (!isshow)
            {
                if (btstage != null)
                {
                    foreach (GameObject area in btstage)
                    {
                        area.SetActive(isshow);
                    }
                }
            }
            if (stage_ != null)
            {
                stage_.SetActive(!isshow);
                gameObject.SetActive(isshow);
            }
        }
        private void EnableBtStage()
        {
            foreach (GameObject area in btstage)
            {
                area.SetActive(true);
            }
        }
        private void ScaleBackground(bool issmall)
        {
            if (issmall)
            {
                iTween.ScaleTo(background_, iTween.Hash("x", default_scale_.x
                                                        , "y", default_scale_.y
                                                        , "time", .2f
                                                        , "easetype", iTween.EaseType.linear
                                                        ,"oncomplete","EnableBtStage"
                                                        ,"oncompletetarget",gameObject));
                iTween.MoveTo(background_, iTween.Hash("x", default_pos.x
                                                        , "y", default_pos.y
                                                        , "time", .2f
                                                        , "easetype", iTween.EaseType.linear
                                                        ));
            }
            else
            {
                iTween.ScaleTo(background_, iTween.Hash("x", to_scale_.x
                                                        , "y", to_scale_.y
                                                        , "time", .2f, "easetype"
                                                        , iTween.EaseType.linear
                                                        ));
                iTween.MoveTo(background_, iTween.Hash("x", background_point_[area_index_].x
                                                        , "y", background_point_[area_index_].y
                                                        , "time", .2f
                                                        , "easetype", iTween.EaseType.linear
                                                        ));
            }
        }
        private void ChangeTopSprite(bool isarea)
        {
			//area_top_.SetActive(isarea);
			//stage_top_.SetActive(!isarea);
			if (isarea)
			{
				top_bar_.GetComponent<SpriteRenderer>().sprite = area_top_;
			}
			else
			{
				top_bar_.GetComponent<SpriteRenderer>().sprite = stage_top_;
			}
        }
        //private void Stage3Operation()
        //{
        //    area_index_ = 2;
        //    StageControl.levelname_ = "BattleNormal";        // store battle boss scene's name to play in StageControl
        //    level_name_ = "StageSelect";
        //    EnableAreas(false);
        //    is_allow_ = true;
        //    // AnimationSystem.ShowNavigation("LoadLevel", gameObject);            // play animation to show navigation and call method LoadLevel after complete animation
        //}
        /// <summary>
        /// method to load to new scene
        /// </summary>
        private void LoadLevel()
        {
            // shake camera
            iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("NewLevel", .4f);    // call method NewLevel after .4f delay
        }
        /// <summary>
        /// method to LoadLevel
        /// </summary>
        private void NewLevel()
        {
            Application.LoadLevel(level_name_);     // load level depend on level_name
        }
		#region Variable
        public static bool isfirst_ = true;
        public static int area_index_ = 0;
		public static bool is_from_menu_ = true;
		[SerializeField]
        private GameObject[] btstage= null;        // gameobject of button stage 1
        //[SerializeField]
        //private GameObject btstage2_ = null;        // gameobject of button stage 2
        [SerializeField]
        private Sprite new_icon_ = null;
        [SerializeField]
        private GameObject stage_ = null;
        #region Top_Sprite
		[SerializeField]
		private GameObject top_bar_ = null;
        [SerializeField]
        private Sprite area_top_ = null;
        [SerializeField]
        private Sprite stage_top_ = null;
        #endregion
        [SerializeField]
        private GameObject background_ = null;
        [SerializeField]
        private Vector2 default_scale_ = Vector2.zero;
        [SerializeField]
        private Vector2 to_scale_ = Vector2.zero;
        [SerializeField]
        private Vector2 default_pos = Vector2.zero;
        [SerializeField]
        private Vector2[] background_point_ = null;
        private bool is_allow_ = true;              // boolean set player to click or can not click
		private string level_name_ = "";            // name of level for loading
		#endregion
	}
}