﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace YNinja.MetalSlug
{
    public class ShopScreen : MonoBehaviour
    {
        /*
         * used in shop scene
         */ 
		void Awake()
		{
			item_ = new List<GameObject>();         // init list item
			price_ = new List<TextMesh>();          // init list price
			bt_buy_ = new List<GameObject>();       // init button buy
            shop_item_ = new List<string>() { "Bazooka", "Miner", "Marco", "Tarma", "Fio", "Eri", "Shield"
                                            , "Sarubia","Arabian","Berserker","Mummy","SwordMan"
                                            ,"Licker","Scientist","YoungGirl","Wolf","YoungGirlGreen","Salvaldor"};
		}
        // Use this for initialization
        void Start()
        {
            ClickControl.Initialize(Camera.main);               // init instance of ClickControl with main camera
            AnimationSystem.HideNavigation();                   // animation of hide navigate
           // fund_.text = DataSaving.LoadFund()+"";              // assign text of fund 
            fund_.text = DataSavingXml.Instance().fund+"";
            Initialize();                                       // call method to initialize
			if (isbuyitem_ == null)                             // not buy item
			{
				isbuyitem_ = new bool[scroll_box_.box_.Count];  // instace arry of buy item with number of scroll box
				for (int i = 0; i < isbuyitem_.Length; i++)     // loop by length of isbuyitem
					isbuyitem_[i] = false;                      // set to false
			}
			CheckBuying();                                      // call method to check buying
			RefreshItemValue();                                 // call method to refresh value of item
			SetAvailableBT();                                   // call method to set avaliable button buying
			SetCurrenAnim();                                    // call method to set animation 
            //ReplaceNewTexture();
        }
        void OnEnable()
        {
            ClickControl.OnClick += OnClickButton;              // register method OnClickButton with OnClick
        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton;              // unregister method OnClickButton with OnClick
        }

        #region Test New Texture
        private void ReplaceNewTexture()
        {
            for (int i = 0; i < texture_name_.Length; i++)
            {
                tk2dSprite sprite = scroll_box_.box_[i].GetComponent<BoxSellControl>().Item.GetComponent<tk2dSprite>();
                sprite.spriteId = sprite.GetSpriteIdByName(texture_name_[i]);
                sprite.scale = scale_new_;
                sprite.gameObject.GetComponent<tk2dSpriteAnimator>().Stop();
            }
        }
        #endregion

        /// <summary>
        /// method used when player click
        /// </summary>
        /// <param name="hit">object of button</param>
        private void OnClickButton(Transform hit)
        {
            if (hit != null)
            {
                if (hit.transform.name == "btBack")                             // click on button back
                {
                    AnimationSystem.ScaleButton(hit.gameObject, "BackOperation", gameObject);
                }
                else if (hit.transform.name == "btCustomize")                   // click on button Customize
                {
                    AnimationSystem.ScaleButton(hit.gameObject, "CustomizeOperation", gameObject);
                }
                else if (is_allow_confirm_)                                     // click on button buy
                {
                    if (hit.transform.name == "bt_Ok")                          // click on button ok
                    {
                        AnimationSystem.ScaleButton(hit.gameObject, "OkOperation", gameObject);
                    }
                    else if (hit.transform.name == "bt_Cancel")                 // click on button cancel
                    {
                        AnimationSystem.ScaleButton(hit.gameObject, "CancelOperation", gameObject);
                    }
                }
            }
        }
        private void OkOperation()
        {
            ShowComfirmBox("");                                     // call method to show confirmbox
            if (DataSavingXml.Instance().fund >= sold_type_[index_].ShopPrice)                       // check total fun greater than 600
            {
                BuyItem();                                          // call method to buy item
            }
            else                                                    // less than 600
            {
                StartCoroutine(ShowDialog());                       // show dialog not enough fund
            }
        }
        private void CancelOperation()
        {
            ShowComfirmBox("");                                      // call method show confirm box
                   
        }
        private void BuyOperation()
        {
            ShowComfirmBox("" + sold_type_[index_].ShopPrice);                                 // show confirm box with price 600
				
        }
        private void BackOperation()
        {
            MenuControl.levelname_ = temp_levelname;                    // set battle name to MenuControl
            if (back_name_ != null)
            {
                next_scene = back_name_;//"Menu";                                        // set scene name to load Menu
                AnimationSystem.ShowNavigation("LoadLevel", gameObject);    // animation show navigate and call method loadlevel
            }
        }
        private void CustomizeOperation()
        {
            CustomizeControl.currentlevel_ = temp_levelname;             // set battle name to CustomizeControl
            next_scene = "Customize";                                   // set scene name to load Customize
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);    // animation show navigate and call method loadlevel
               
        }
		private void OnScroll()
		{
			//if (is_allow_swap_)                             // allow to swipe
			//{
				ResetPanelIndex();
			//}
		}
		private void OnRelease()
		{
			scroll_box_.OnRelease();
			//ResetPanelIndex();
			//Invoke("ResetPanelIndex",.3f);
		}
		private void OnClick()
		{
			scroll_box_.OnClick();
		}
		private void ResetPanelIndex()
		{
			int getindex = scroll_box_.OnScroll();      //return index of current box 
			if (getindex != index_)                     // index of current and old index not equal                               
			{
				index_ = getindex;                      // set current index
				RefreshItemValue();                     // call method refresh value of current box
				SetCurrenAnim();                        // call  method set current animation
				SetAvailableBT();                       // set available button
			}
		}
        /// <summary>
        /// method to click item
        /// </summary>
        /// <returns></returns>
		private	IEnumerator ClickItem()
		{
			Transform hit = GeneralSystem.ClickOnObject3D(Camera.main);     //get button buy
			if (hit != null && is_allow_swap_)                              // can click
			{
                if (bt_buy_.Contains(hit.gameObject))                       // check button in list
                {
                    bt_clicked_ = hit;                                      // set bt_clicked
                    AnimationSystem.ScaleButton(hit.gameObject);//, "BuyOperation", gameObject);
					BuyOperation();
			    }
			}
			return null;
		}
		/// <summary>
		/// method to buy item
		/// </summary>
		private void BuyItem()
		{
            DataSavingXml data = DataSavingXml.Instance();
			bt_clicked_.GetComponent<SpriteRenderer>().sprite = bt_purchase_;       // change sprite button buy to button purchased
			bt_clicked_.gameObject.collider.enabled = false;                        // disable collider
			//int fund = DataSaving.LoadFund();                                       // load total fund
            int fund = data.fund;
            fund -= sold_type_[index_].ShopPrice;                                                            // sub total fund with 600
			fund_.text = fund + "";                                                 // set lable of fund
			//DataSaving.SaveFund(fund);                                              // save total fund
            data.fund = fund;
            isbuyitem_[index_] = true;                                              // set index of item buying to true
			//DataSaving.SaveCustomize(isbuyitem_);                                   // save data to Customize
            data.customize.items = isbuyitem_;
            is_allow_swap_ = true;                                                  // allow to swap
            //string[] order = DataSaving.LoadOrder();                                // load order of deck
            string[] order = data.order.orders;
            bool isfull = true;
            //Debug.Log(order.Length +" "+shop_item_.Count);
            for (int i = 0; i < order.Length; i++)
            {
                if (order[i] == "")
                {
                    order[i] = item_name_[index_];                                  // set name of item to order
                    isfull = false;
                    break;
                }
            }
            //Debug.Log(isfull);
            if (isfull)
            {
                data.order.remains = SaveRemain(data.order.remains,item_name_[index_]);
            }
            //Debug.Log(data.order.remains.Length);
            //DataSaving.SaveOrder(order);
            data.order.orders = order;
            data.SaveData();// save order of item
		}
        private string[] SaveRemain(string[] remain,string name)
        {
            string[] items = null;
            
            if (remain.Length < 0)
            {
                items = new string[] {name};
            }
            else
            {
                items=new string[remain.Length+1];
                for (int i = 0; i < items.Length; i++)
                {
                    if (i == items.Length - 1)
                    {
                        items[i] = name;
                    }
                    else
                    {
                        items[i] = remain[i];
                    }
                }

            }
            return items;
        }
        /// <summary>
        /// method to set avaliable button
        /// </summary>
		private void SetAvailableBT()
		{
			for (int i = 0; i < bt_buy_.Count; i++)
			{
				if (i == index_ && !isbuyitem_[i])                          // compare index
				{
					bt_buy_[i].GetComponent<Collider>().enabled = true;     // enable collider of availble button
				}
				else
				{
					bt_buy_[i].GetComponent<Collider>().enabled = false;        // disable collider of other button
				}
					
			}
		}
        /// <summary>
        /// method to refresh value of current item
        /// </summary>
		private void RefreshItemValue()
		{
			info_distand_.text = "STAND.ATK : " + sold_type_[index_].AttackDistance;        // set attack distance
			info_speed_.text = "SPEED.ATK : " + sold_type_[index_].Speed;                   // set speed
			info_demage_.text = "DEMAGE : " + sold_type_[index_].Damage;                    // set damage
			item_hp_.text ="HP : "+ sold_type_[index_].Health;                              // set hp 
		}
        /// <summary>
        ///  method to set current animation
        /// </summary>
		private void SetCurrenAnim()
		{
           
                for (int i = 0; i < item_.Count; i++)
                {
                    if (i > 2)
                    {
                        if (i == index_)                    // compare index
                        {
                            item_[i].GetComponent<tk2dSpriteAnimator>().Play(anim_clip_[i]);        // play animation of current item
                        }
                        else
                        {
                            item_[i].GetComponent<tk2dSpriteAnimator>().Play(anim_clip_[i]);         // stop play animation of other item
                            item_[i].GetComponent<tk2dSpriteAnimator>().Stop();
                        }
                    }
                }
		}
        /// <summary>
        /// show dialog 
        /// </summary>
        /// <returns></returns>
		private IEnumerator ShowDialog()
		{
			is_allow_swap_ = false;                             // can not swap
			pop_up_window_.SetActive(true);                     // show pop up
			AnimationSystem.ShowDialogByScale(pop_up_window_);  // animaton of show dialog
			yield return new WaitForSeconds(1);                 // delay 1s
			pop_up_window_.SetActive(false);                    // hide pop up
			is_allow_swap_ = true;                              // can swap
		}
        /// <summary>
        /// method to show confirm box
        /// </summary>
        /// <param name="amount"></param>
		private void ShowComfirmBox(string amount)
		{
			if (comfirm_box_.activeSelf)            // confirm box is active
			{
				comfirm_box_.SetActive(false);      // hide confirm box
				is_allow_swap_ = true;              // can swap
				is_allow_confirm_ = false;          // can not confirm
				scroll_box_.ScrollEnable(true); // enable scroll
			}
			else                                   // confirm box is not active
			{
				AnimationSystem.ShowDialogByScale(comfirm_box_,"AllowConfirm",gameObject);          // show dialog
				comfirm_box_.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<TextMesh>().text =": buy for "+ amount+" ?";        // set text of confirm box
				comfirm_box_.SetActive(true);       // enable box
				is_allow_swap_ = false;             // can not swap
				scroll_box_.ScrollEnable(false); // disable scroll
			}
		}
        /// <summary>
        /// 
        /// </summary>
        private void LoadLevel()
        {
            iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
            Invoke("NewLevel", .4f);
        }
        /// <summary>
        /// method to load level
        /// </summary>
        private void NewLevel()
        {
            Application.LoadLevel(next_scene); // load next scene
        }
        /// <summary>
        /// method to check button buy
        /// </summary>
        private void CheckBuying()
        {
			//isbuyitem_ = DataSaving.LoadCustomize();        // load data customize
            isbuyitem_ = DataSavingXml.Instance().customize.items;
            // Debug.Log(isbuyitem_.Length);
            for (int i = 0; i < isbuyitem_.Length; i++)
			{
				if (isbuyitem_[i])              // index i , already buy
				{
					bt_buy_[i].GetComponent<SpriteRenderer>().sprite = bt_purchase_;    // change button to purchased
					bt_buy_[i].gameObject.collider.enabled = false;                     // disable collider
				}
			}
        }
        /// <summary>
        ///  method to initialize
        /// </summary>
		public void Initialize()
		{
            item_name_ = new List<string>();        // list of item
            GameObject temp=null;                        // gameobject temp
            Vector3 pos = Vector3.zero;             // position box
            //foreach (string name in shop_item_)
            //{
            for(int i=0;i<shop_item_.Count;i++)
            {
                string name = shop_item_[i];
                item_name_.Add(name);
                SoldierType type = SoldierType.GetType(name);
                temp = CreateBox(type.BoxTitle,type.ShopPrice, type.SpritName, type.IdleAnimation);       // creat box bazooka
                if(name=="Bazooka")
                {
                    pos = temp.transform.position;              // position of box bozooka
                }
				else if (name == "Sarubia")
				{
					Vector3 pos1 = item_[item_.Count - 1].transform.position;
					pos1.y = pos1.y + 1;
					item_[item_.Count - 1].transform.position = pos1;
                }
                else if (name == "YoungGirl" || name=="Wolf" || name=="YoungGirlGreen")
                {
                    item_[i].GetComponent<tk2dSprite>().scale = new Vector3(-5f, 5f, 0f);    
                }
                temp.transform.position = pos;
                SetInfo(type.IdleAnimation, type);      // set infomation of bazooka
                pos.x += gap_;   
            }
            //item_name_.Add("Bazooka");              // add item soldier bazooka
            //item_name_.Add("Miner");                // add item miner
            //item_name_.Add("Marco");
            
            //temp = CreateBox("Bazooka Soldier",SoldierData.price_bazooka_, "bazooka_stand1", "Bazooka_Idle");       // creat box bazooka
            //pos = temp.transform.position;              // position of box bozooka
            //temp.transform.position = pos;
            //SetInfo("Bazooka_Idle",new Bazooka());      // set infomation of bazooka
            //pos.x += gap_;                              // set gap of each box
            //temp = CreateBox( "Miner Soldier", SoldierData.price_mine_, "Miner_Run_4", "Miner_Run");            // create box miner
            //temp.transform.position = pos;              // position of box miner
            //SetInfo("Miner_Run",new Miner());           // set infomation of miner
            //pos.x += gap_;                              // set gap of each box
            //temp = CreateBox("Marco", SoldierData.price_mine_, "marco_sleep1", "Marco_Sleep");            // create box miner
            //temp.transform.position = pos;              // position of box miner
            //SetInfo("Marco_Sleep", new Marco());           // set infomation of mine
        }
        /// <summary>
        /// method to set infomation
        /// </summary>
        /// <param name="anim"></param>
        /// <param name="soldtype"></param>
		private void SetInfo(string anim,SoldierType soldtype)
		{
			if (anim_clip_ == null)
			{
				anim_clip_ = new List<string>();
				sold_type_ = new List<SoldierType>();
			}
			anim_clip_.Add(anim);           // add animation clip
			sold_type_.Add(soldtype);       // add type
		}
        /// <summary>
        /// method to create box
        /// </summary>
        /// <param name="title"></param>
        /// <param name="price"></param>
        /// <param name="spritename"></param>
        /// <param name="animationname"></param>
        /// <returns></returns>
		private GameObject CreateBox(string title, int price, string spritename, string animationname)
		{
			GameObject temp = (GameObject)Instantiate(box_prefab_);             // instance from prefab box_prefab
			BoxSellControl box = temp.GetComponent<BoxSellControl>();           // add component BoxSellControl
			box.Initialize(title,this.gameObject, "ClickItem", price, spritename, animationname);       // initialize box
			item_.Add(box.Item);                    //add item
			scroll_box_.box_.Add(temp);             // add box
			price_.Add(box.Price);                  // add price
			bt_buy_.Add(box.ButtonBuy);             // add button buy
			temp.transform.position = default_pos_; // set position
			return temp ;
		}
        /// <summary>
        /// method to allow confirm
        /// </summary>
		private void AllowConfirm()
		{
			is_allow_confirm_ = true;       // set is_allow_confirm to true
		}
		#region Variables
		public static string temp_levelname = "";               // levelname
        public static string back_name_ = "Menu";
        [SerializeField]
        private TextMesh fund_ = null;                          // label of fund
        [SerializeField]
        private Sprite bt_purchase_ = null;                     // sprite of button purchase
        [SerializeField]
		private Vector3 default_pos_ = Vector3.zero;            // default position of box
		[SerializeField]
		private	ScrollBoxControl scroll_box_ = null;            // object of sellboxcontrol
		[SerializeField]
		private GameObject box_prefab_ = null;                  // prefab of box
		[SerializeField]
		private TextMesh item_hp_ = null;                       // label of hp
		[SerializeField]
		private TextMesh info_distand_ = null;                  // lable of distance
		[SerializeField]
		private TextMesh info_speed_ = null;                    // label of speed
		[SerializeField]
		private TextMesh info_demage_ = null;                   // label of damage
		[SerializeField]
		private GameObject comfirm_box_ = null;                 // label of confirm box
		[SerializeField]
		private GameObject pop_up_window_ = null;               // label of pop up
        [SerializeField]
        private string[] texture_name_ = null;
        [SerializeField]
        private Vector3 scale_new_ = Vector3.zero;
        
        private bool is_allow_confirm_ = false;                 // allow to confirm
        private List<string> item_name_= null;                  // list item
        private string next_scene = "";                         // name of next scene
		private Transform bt_clicked_;                          // button click
		private bool is_allow_swap_ = true;                     // allow to swap
		private List<GameObject> item_ = null;                  // list of item
		private List<TextMesh> price_ = null;                   // list of price
		private List<GameObject> bt_buy_;                       // list of button buy
		private List<string> anim_clip_ = null;                 // list of name of animtion clip
		private float gap_ = 25;                                // gap
		private int index_;                                     // index of current box
		private bool[] isbuyitem_;                              // array of buyitem
		private List<SoldierType> sold_type_ = null;            // list of soldier type
        private List<string> shop_item_ = null;
        #endregion
	}
}