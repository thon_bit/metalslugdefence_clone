﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace YNinja.MetalSlug
{
    public class CustomizeControl : MonoBehaviour
    {
		void Awake()
		{
			DeclareList();
		}
        // Use this for initialization
        void Start()
        {
            ClickControl.Initialize(Camera.main);
            AudioSingleton.Instance().PlayBackground(3);
            AnimationSystem.HideNavigation();
            Initialize();
			if (scroll_box_.box_.Count > 1)
			{
				index_ = 1;
				RefreshItemValue(true);
			}
			index_ = 0;
			RefreshItemValue(true);
			SetCurrenAnim();
        }
        // Update is called once per frame
        void Update()
        {
            MoveBackground();
        }
        void OnEnable()
        {
            ClickControl.OnClick += OnClickButton;
        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton;
        }
        private void OnClickButton(Transform hit)
        {
            if (hit != null && enable_click_)
            {
                if (hit.transform.name == "btBack")
                {
                    AnimationSystem.ScaleButton(hit.gameObject, "BackOperation", gameObject);
                }
                else if (hit.transform.name == "btDeck")
                {
                    AnimationSystem.ScaleButton(hit.gameObject, "DeckOperation", gameObject);
                }
                else if (hit.transform.name == "btShop")
                {
                    AnimationSystem.ScaleButton(hit.gameObject, "ShopOperation", gameObject);
                }
            }
        }
        private void BackOperation()
        {
            levelname_ = "Menu";
            MenuControl.levelname_ = currentlevel_;
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
            enable_click_ = false;
        }
        private void DeckOperation()
        {
            levelname_ = "Deck";
            DeckControl.area_select_ = currentlevel_;
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
            enable_click_ = false;
        }
        private void ShopOperation()
        {
            levelname_ = "Shop";
            ShopScreen.temp_levelname = currentlevel_;
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
            enable_click_ = false;
        }
		private IEnumerator ClickItem()
		{
			if (enable_click_ )
			{
				Transform hit = GeneralSystem.ClickOnObject3D(Camera.main);
                if (hit.transform.name == "btUp")
                {
                    AnimationSystem.ScaleButton(hit.gameObject);
                    for (int i = 0; i < bt_up_.Count; i++)
                    {
                        if (hit.gameObject == bt_up_[i])
                        {
                            BuyItem(i);
                        }
                    }
                }
			}
            yield return null;
		}
		
		private void OnScroll()
		{
			
			ResetPanelIndex();
			
		}
		private void OnRelease()
		{
			scroll_box_.OnRelease();
			//ResetPanelIndex();
			//Invoke("ResetPanelIndex",.3f);
		}
		private void OnClick()
		{
			scroll_box_.OnClick();
		}
		private void ResetPanelIndex()
		{
			int getindex = scroll_box_.OnScroll();
			if (getindex != index_)
			{
				DisableLevelPopup();
				index_ = getindex;
				RefreshItemValue(false);
				SetCurrenAnim();
			}
		}
        private void LoadLevel()
        {
            if (levelname_ != "")
            {
                iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
                Invoke("NewLevel", .4f);
            }
        }
        private void NewLevel()
        {
			Debug.Log("Save Lv");
			SavedUpgrade();
            Application.LoadLevel(levelname_);
        }
		private void SavedUpgrade()
		{
			DataSavingXml data = DataSavingXml.Instance();
			data.fund = data_fund_;
			data.SaveData();
			for (int i = 0; i < sold_type_.Count; i++)
			{
				sold_type_[i].Health = data_hp_[i];
				sold_type_[i].Level = data_lv_[i];
			}
		}
		private void MoveBackground()
        {
            Vector3 current = background_.transform.position;
            Vector3 targetpos = new Vector3(target_, current.y, current.z);
            current = Vector3.MoveTowards(current, targetpos, Time.deltaTime * speed_);
            background_.transform.position = current;
            if (Vector3.Distance(current, targetpos) <= 2)
            {
                if (target_ == maxx_)
                {
                    target_ = minx_;
                }
                else
                {
                    target_ = maxx_;
                }
            }
        }
		private void BuyItem(int index)
		{
            if (index_ == index)
            {
				//DataSavingXml data = DataSavingXml.Instance();
				//if (data.fund >= sold_type_[index_].Price)
				int price = DefinePrice(index);
				if(data_fund_ >= price )
                {
                    index_ = index;
                    enable_click_ = false;
					data_fund_ -= price;
					data_lv_[index_] += 1;
					data_hp_[index_] += 2;
					//int fund = data.fund;
					//fund -= sold_type_[index_].Price;
					//data.fund = fund;
					//data.SaveData();
					//sold_type_[index_].Health += 2;
					//sold_type_[index_].Level += 1;
                    RefreshItemValue(true);
					if(anim_up_[index_]!="")
						item_[index].GetComponent<tk2dSpriteAnimator>().Play(anim_up_[index_]);
                    up_level_pop_up_[index].SetActive(true);
                    iTween.MoveFrom(up_level_pop_up_[index], iTween.Hash("y", up_ps_[index_].y - 2
                                                                , "time", .3f, "easetype", iTween.EaseType.linear));//, "oncomplete", "Disable","oncompletetarget",gameObject));
                    Invoke("Stop", 1f);
                }
                else
                {
                    StartCoroutine(ShowPopUp());
                }
            }
		}
		private int DefinePrice(int index)
		{
			float price_temp = data_price_[index];//sold_type_[index].Price;
			for (int i = 0; i < data_lv_[index]; i++)
			{
				price_temp += (price_temp * .25f);
			}
			return Mathf.RoundToInt(price_temp);
		}
		private void SetCurrenAnim()
		{
			for(int i=0;i< item_.Count;i++)
			{
				if (i == index_)
				{
					item_[i].GetComponent<tk2dSpriteAnimator>().Play(anim_clip_[i]);
				}
				else
				{
					item_[i].GetComponent<tk2dSpriteAnimator>().Play(anim_clip_[i]);
					item_[i].GetComponent<tk2dSpriteAnimator>().Stop();
				}
			}
		}
		private void Stop()
		{
			SetCurrenAnim();
			up_level_pop_up_[index_].SetActive(false);
			enable_click_ = true;
		}
		private void RefreshItemValue(bool isbuy)
		{
			//if (isbuy)
			//{
			funds_.text = "" + data_fund_;//DataSavingXml.Instance().fund;//DataSaving.LoadFund();
			//}
			info_distand_.text = "STAND.ATK : "+sold_type_[index_].AttackDistance;
			info_speed_.text = "SPEED.ATK : "+sold_type_[index_].Speed;
			info_demage_.text = "DEMAGE : " + sold_type_[index_].Damage;
			item_hp_.text = "HP : " + data_hp_[index_];//sold_type_[index_].Health;
			level_[index_].text = "" + (data_lv_[index_]+1);//(sold_type_[index_].Level + 1);
            //funds_.text = "" + DataSavingXml.Instance().fund;//DataSaving.LoadFund();
			price_[index_].text = "" + DefinePrice(index_);//sold_type_[index_].Price;
			//if (isbuy)
			//{
			//	string s = "( +2 )";
			//	item_hp_.transform.GetChild(0).transform.GetComponent<TextMesh>().text = s;
			//}
		}
		private IEnumerator ShowPopUp()
		{
			pop_up_.SetActive(true);
			AnimationSystem.ShowDialogByScale(pop_up_);
			enable_click_ = false;
			scroll_box_.ScrollEnable(false);
			yield return new WaitForSeconds(1);
			enable_click_ = true;
			pop_up_.SetActive(false);
			scroll_box_.ScrollEnable(true);
		}
        private void DisableLevelPopup()
        {
            foreach (GameObject g in up_level_pop_up_)
            {
                g.SetActive(false);
            }
        }
        private void Initialize()
        {
			GameObject temp;
			Vector3 pos;
            SoldierType type = new Soldier(false);
            temp = CreateBox(type.BoxTitle, type.Level, type.Price, type.SpritName, type.IdleAnimation);
			pos = temp.transform.position;
			pos.x += gap_;
            SetInfor(type.IdleAnimation, type.WinAnimation, type);
            bool[] isbuy = DataSavingXml.Instance().customize.items;
            if (isbuy != null)
            {
                for (int i = 0; i < isbuy.Length; i++)
                {
                    if (isbuy[i])
                    {
                        type = SoldierType.GetType(new_item_[i]);
                        temp = CreateBox(type.BoxTitle, type.Level, type.Price, type.SpritName, type.IdleAnimation);
                        temp.transform.position = pos;
                        pos.x += gap_;
                        SetInfor(type.IdleAnimation, type.WinAnimation, type);
						if (sold_type_[sold_type_.Count-1].GetType().Name == "Sarubia")
						{
							Vector3 pos1 = item_[item_.Count - 1].transform.position;
							pos1.y = pos1.y + 2;
							item_[item_.Count - 1].transform.position = pos1;
						}
                        //if (sold_type_[i].GetType() == typeof(YoungGirl) || sold_type_[i].GetType() == typeof(Wolf))
                        //{
                        //    item_[i].GetComponent<tk2dSprite>().scale = new Vector3(-5f, 5f, 0f);    
               
                        //}
                    }
                }
            }
			// The plus number for up level
			plus_hp_ = item_hp_.transform.GetChild(0).GetComponent<TextMesh>();
			plus_hp_.text = "( +2 )";
			plus_hp_.gameObject.SetActive(true);
			//The set for load data
			DataSavingXml data = DataSavingXml.Instance();
			data_hp_ = new List<float>();
			data_lv_ = new List<int>();
			for (int i = 0; i < sold_type_.Count; i++)
			{
				data_hp_.Add(sold_type_[i].Health);
				data_lv_.Add(sold_type_[i].Level);
                if (sold_type_[i].GetType() == typeof(YoungGirl) || sold_type_[i].GetType() == typeof(Wolf) || sold_type_[i].GetType() == typeof(YoungGirlGreen))
                {
                    item_[i].GetComponent<tk2dSprite>().scale = new Vector3(-5f, 5f, 0f);

                }
			}
			data_price_ = data.army.price;		  
			data_fund_ = data.fund;
		}
		private void SetInfor(string anim_IDle, string uplevel, SoldierType soldtype)
		{
			if (anim_clip_ == null)
			{
				anim_clip_ = new List<string>();
				anim_up_ = new List<string>();
				sold_type_ = new List<SoldierType>();
			}
			anim_clip_.Add(anim_IDle);
			anim_up_.Add(uplevel);
			sold_type_.Add(soldtype);
		}
        private GameObject CreateBox(string title,int level,int price,string spritename,string animationname)
        {
            GameObject temp = (GameObject)Instantiate(box_prefab_);
            BoxLevelControl box =temp.GetComponent<BoxLevelControl>();
            //price = 600 + (600*(5/100));
            box.Initialize(this.gameObject,"ClickItem",title, level, price, spritename, animationname);
            item_.Add(box.Item);
            level_.Add(box.Level);
            price_.Add(box.Price);
            bt_up_.Add(box.ButtonUp);
            up_level_pop_up_.Add(box.PopUp);
			scroll_box_.box_.Add(temp);
            temp.transform.position = default_pos_;
			up_ps_.Add(box.PopUp.transform.position);
            return temp;
        }
		private void DeclareList()
		{
			target_ = maxx_;
			item_ = new List<GameObject>();
			price_ = new List<TextMesh>();
			level_ = new List<TextMesh>();
			bt_up_ = new List<GameObject>();
			up_level_pop_up_ = new List<GameObject>();
			up_ps_ = new List<Vector3>();
			scroll_box_.box_ = new List<GameObject>();
            new_item_ = new List<string>() { "Bazooka", "Miner", "Marco", "Tarma", "Fio", "Eri"
                                        , "Shield", "Sarubia", "Arabian", "Berserker" ,"Mummy","SwordMan"
                                        ,"Licker","Scientist","YoungGirl","Wolf","YoungGirlGreen","Salvaldor"};
		}
       
        #region Variables
        public static string currentlevel_ = "";        
        [SerializeField]
        private GameObject background_ = null;
        [SerializeField]
        private float minx_ = 0;
        [SerializeField]
        private float maxx_ = 0;
        [SerializeField]
        private float speed_ = 2;
		private List<GameObject> item_ = null;
		[SerializeField]
		private TextMesh funds_ = null;
		[SerializeField]
		private GameObject pop_up_ = null;
		[SerializeField]
		private TextMesh item_hp_ = null;
		[SerializeField]
		private TextMesh info_distand_ = null;
		[SerializeField]
		private TextMesh info_speed_ = null;
		[SerializeField]
		private TextMesh info_demage_ = null;
        [SerializeField]
        private Vector3 default_pos_=Vector3.zero;
        [SerializeField]
        private GameObject box_prefab_ = null;
        [SerializeField]
        private float gap_ = 0;
		private List<string> anim_clip_ = null;
		private List<string> anim_up_ = null;
		[SerializeField]
		private ScrollBoxControl scroll_box_ = null;
		private List<SoldierType> sold_type_ = null;
		private string levelname_ = "";
		private float target_ = 0;
		private Vector3[] swap_posi_;
		private List<Vector3> up_ps_;
		private List<TextMesh> price_ = null;
		private List<TextMesh> level_ = null;
		private bool enable_click_ = true;
		private List<GameObject> bt_up_ = null;
		private int index_;
		private float width_;
		private List<GameObject> up_level_pop_up_ = null;
        private List<string> new_item_ = null;
		private TextMesh plus_hp_ = null;
		private List<int> data_lv_ = null;
		private List<float> data_hp_ = null;
		private int[] data_price_ = null;
		private int data_fund_;
        #endregion
    }
}