﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class MenuControl : MonoBehaviour
    {
        void Awake()
        {
            target = maxx_;
        }
        // Use this for initialization
        void Start()
        {
			if(IsInvoking())
				CancelInvoke();
            ClickControl.Initialize(Camera.main);
            AudioSingleton.Instance().PlayBackground(4);
            AnimationSystem.HideNavigation();
            //PlayerPrefs.DeleteAll();
			Invoke("SetSpriteBG", time_change_sprite_);
			Invoke("ShowBTAnimation",.5f);
			Vector3 pos = bt_sortie_.transform.position;
			pos.x = 60;
			bt_sortie_.transform.position = pos;
			pos.y = bt_custom_.transform.position.y;
			bt_custom_.transform.position = pos;
			is_blink_bg_ = true;
        }

		private void ShowBTAnimation()
		{
			AnimationSystem.MoveItemTo(bt_sortie_, "x", 12, .25f, 0,iTween.EaseType.linear);
			AnimationSystem.MoveItemTo(bt_custom_, "x", 12, .25f, .2f,iTween.EaseType.linear);
		}
        // Update is called once per frame
        void Update()
        {
            MoveBackground();
			if (is_blink_bg_)
				BlinkBG();
        }
        void OnEnable()
        {
            ClickControl.OnClick += OnClickButton;
        }
        void OnDisable()
        {
            ClickControl.OnClick -= OnClickButton;
        }
        private void OnClickButton(Transform hit)
        {
            if (hit != null)
            {
                string methodname = null;
                switch (hit.transform.name)
                {
                    case "btSortie": methodname = "SortieOperation"; break;
                    case "btBack": methodname = "BackOperation"; break;
                    case "btShop": methodname = "ShopOperation"; break;
                    case "btCustomize": methodname = "CustomizeOperation"; break;
                }
                if(methodname!=null)
                    AnimationSystem.ScaleButton(hit.gameObject, methodname, gameObject);
            }
        }
        private void SortieOperation()
        {
			levelname_ = "World";
			AreaSelectControl.is_from_menu_ = true;
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
           
        }
        private void BackOperation()
        {
			levelname_ = "Title";//"AreaSelect";
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
           
        }
        private void ShopOperation()
        {
            ShopScreen.temp_levelname = levelname_;
            levelname_ = "Shop";
            ShopScreen.back_name_ = "Menu";
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
           
        }
        private void CustomizeOperation()
        {
            CustomizeControl.currentlevel_ = levelname_;
            levelname_ = "Customize";
            AnimationSystem.ShowNavigation("LoadLevel", gameObject);
           
        }
        private void LoadLevel()
        {
            if (levelname_ != "")
            {
                iTween.ShakePosition(Camera.main.gameObject, iTween.Hash("x", 1f, "time", .4f));
                Invoke("NewLevel", .4f);
            }
        }
        private void NewLevel()
        {
			if (IsInvoking())
				CancelInvoke();
            Application.LoadLevel(levelname_);
        }
        private void MoveBackground()
        {
            Vector3 current = background_.transform.position;
            Vector3 targetpos = new Vector3(target, current.y, current.z);
            current = Vector3.MoveTowards(current, targetpos, Time.deltaTime * speed_);
            background_.transform.position = current;
            if (Vector3.Distance(current, targetpos) <= 0)
            {
                if (target == maxx_)
                {
                    target = minx_;
                }
                else
                {
                    target = maxx_;
                }
            }
        }
		private void SetSpriteBG()
		{
			if (num_sprite_ < menu_spr_bg_.Length-1)
			{
				num_sprite_++;
			}
			else
			{
				num_sprite_ = 0;
			}
			menu_box_.sprite = menu_spr_bg_[num_sprite_];
			//StartCoroutine(EnableBlink());
			Invoke("SetSpriteBG",time_change_sprite_);
		}
		IEnumerator EnableBlink()
		{
			is_blink_bg_ = true;
			yield return new WaitForSeconds(.5f);
			is_blink_bg_ = false;
		}
		private void BlinkBG()
		{
			menu_box_.color = new Color(menu_box_.color.r, menu_box_.color.g, menu_box_.color.b, Mathf.PingPong(Time.time * .5f, 1f));
		}
		#region Variables
		public static string levelname_ = "";
        private float target = 0;
        [SerializeField]
        private GameObject background_ = null;
        [SerializeField]
        private float minx_ = 0;
        [SerializeField]
        private float maxx_ = 0;
        [SerializeField]
        private float speed_ = 2;
		[SerializeField]
		private SpriteRenderer menu_box_ = null;
		[SerializeField]
		private Sprite[] menu_spr_bg_ = null;
		private int num_sprite_ = 0;
		[SerializeField]
		private float time_change_sprite_ = 1.5f;
		[SerializeField]
		private GameObject bt_sortie_ = null;
		[SerializeField]
		private GameObject bt_custom_ = null;
		private bool is_blink_bg_ = false;
		#endregion
	}
}