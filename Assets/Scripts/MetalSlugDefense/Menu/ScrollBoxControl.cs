﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YNinja.MetalSlug
{
	public class ScrollBoxControl : MonoBehaviour
	{
		void Awake()
		{
			box_ = new List<GameObject>();
		}
		// Use this for initialization
		void Start()
		{
			if (box_.Count > 1)
			{
				gap_ = box_[1].transform.position.x - box_[0].transform.position.x;
				if (hiden_box_prefab_ != null && is_use_hiden_)
				{
					InitializeHiden();
				}
				float visiblearea = 0;
				for (int i = 0; i < box_.Count; i++)
				{
					box_[i].transform.parent = scroll_area_.contentContainer.transform;
					visiblearea += length_per_unit_;//2.45f;
				}
				scroll_area_.ContentLength = visiblearea;
				scroll_area_.VisibleAreaLength = visible_length_;//2.1f
				main_panel_pos_ = box_[0].transform.position;
				index_ = 0;
				SetHidenWithTk();
				if (!is_use_drag_)
				{
					scroll_area_.enabled = false;
				}
			}
			else 
			{
				scroll_area_.enabled = false;
			}
			SetButtonDirection();
		}
		// Update is called once per frame
		void Update()
		{
            //Debug.Log(is_move_swap_);
			//if (is_move_swap_)
			//{

			//	for (int i = 0; i < box_.Count; i++)
			//	{
			//		Vector3 cur = box_[i].transform.position;
			//		box_[i].transform.position = Vector3.MoveTowards(cur, swap_posi_[i], swipe_speed_ * Time.deltaTime);
			//	}
			//	if (box_[0].transform.position == swap_posi_[0])
			//	{
			//		is_move_swap_ = false;
			//	}
			//}
			if (is_use_drag_)
			{
				if (is_set_focus_)
				{
					is_check_speed_ = false;
					Vector3 pos = scroll_area_.contentContainer.transform.position;
					pos = Vector3.MoveTowards(pos, target_contant_pos_, swipe_speed_ * Time.deltaTime);
					scroll_area_.contentContainer.transform.position = pos;
					if (pos == target_contant_pos_)
					{
						is_set_focus_ = false;
						scroll_area_.enabled = true;
						is_release_ = false;
					}
				}
				if (is_check_speed_)
				{
					float spd = Mathf.Abs((current_contant_pos_.x - star_contain_pos_.x) / Time.deltaTime);
					star_contain_pos_ = current_contant_pos_;
					//Debug.Log(spd);
					if (is_release_ && spd <= 5)
					{
						ResetFucus();
						is_check_speed_ = false;
					}
				}
				else if (!scroll_area_.enabled)
				{
					scroll_area_.enabled = true;
					is_release_ = false;
				}
			}
			
		}
		
		//public int OnSwipe(SwipeGesture g)
		//{
		//	//Debug.Log("Time "+g.StartTime);
		//	//Debug.Log("Velocity "+g.Velocity);
		//	//Debug.Log("Cal " + (g.Velocity / g.StartTime));
		//	//Debug.Log("Speed" + (swipe_speed_ * Time.deltaTime));
		//	if (g.StartSelection != null)
		//	{
		//		if (box_.Count > 1)
		//		{
		//			switch (g.Direction)
		//			{
		//				case FingerGestures.SwipeDirection.Left:
		//					if (IsCanSwitchLeft()) SwitchPanel(false);
		//					break;
		//				case FingerGestures.SwipeDirection.Right:
		//					if (IsCanSwitchRight()) SwitchPanel(true);
		//					break;
		//			}
		//		}
		//	}
		//	return index_;
		//}
		//public IEnumerator GoToPanel(int count, bool isright)
		//{
		//	if (!is_move_swap_)
		//	{
		//		for (int i = 0; i < count; i++)
		//		{
		//			SwitchPanel(isright);
		//			yield return new WaitForSeconds(.1f);
		//		}
		//	}
		//}
        public void SwitchPanel(bool isright)
        {
             if (hiden_1_ != null)
                SetHiden(isright);
            swap_posi_ = new Vector3[box_.Count];
            if (!isright)
            {
                for (int i = 0; i < box_.Count; i++)
                {
                    Vector3 pos = box_[i].transform.position;
                    pos.x = pos.x - gap_;
                    swap_posi_[i] = pos;
                }
                index_++;
            }
            else
            {
                for (int i = 0; i < box_.Count; i++)
                {
                    Vector3 pos = box_[i].transform.position;
                    pos.x = pos.x + gap_;
                    swap_posi_[i] = pos;
                }
                index_--;
            }
            SetButtonDirection();
           //     is_move_swap_ = true;
        }
		private void SetButtonDirection()
		{
			if (bt_next_ != null && bt_pre_ != null)
			{
				if (IsCanSwitchLeft())
				{
					if (!bt_next_.activeSelf)
					{
						bt_next_.SetActive(true);
					}
				}
				else if (bt_next_.activeSelf)
				{
					bt_next_.SetActive(false);
				}
				if (IsCanSwitchRight())
				{
					if (!bt_pre_.activeSelf)
					{
						bt_pre_.SetActive(true);
					}
				}
				else if (bt_pre_.activeSelf)
				{
					bt_pre_.SetActive(false);
				}
			}
		}
		private void SetHiden()
		{
			if (hiden_1_ != null && hiden_2_ != null)
			{
				if (IsCanSwitchLeft())
				{
					if (!hiden_2_.activeSelf)
					{							 
						hiden_2_.SetActive(true);
					}
				}
				else if (hiden_2_.activeSelf)
				{
					hiden_2_.SetActive(false);
				}
				if (IsCanSwitchRight())
				{
					if (!hiden_1_.activeSelf)
					{
						hiden_1_.SetActive(true);
					}
				}
				else if (hiden_1_.activeSelf)
				{
					hiden_1_.SetActive(false);
				}
			}
		}
		void SetHiden(bool isright)
		{
			if (!isright)
			{
				hiden_1_.transform.parent = box_[index_].transform;
				Vector3 pos1 = box_[index_].transform.GetChild(0).transform.position;
				pos1.z = 1.5f;
				hiden_1_.transform.position = pos1;
				if (!hiden_1_.activeSelf) hiden_1_.SetActive(true);
				if (index_ < box_.Count - 2)
				{
					hiden_2_.transform.parent = box_[index_ + 2].transform;
					pos1.x = box_[index_ + 2].transform.GetChild(0).transform.position.x;
					hiden_2_.transform.position = pos1;
					if (!hiden_2_.activeSelf) hiden_2_.SetActive(true);
				}
				else if (hiden_2_.activeSelf) hiden_2_.SetActive(false);
				if (hiden_3_ != null)
				{
					if (index_ > 0)
					{
						hiden_3_.transform.parent = box_[index_ - 1].transform;
						pos1.x = box_[index_ - 1].transform.GetChild(0).transform.position.x;
						hiden_3_.transform.position = pos1;
						if (!hiden_3_.activeSelf) hiden_3_.SetActive(true);
					}
					else if (hiden_3_.activeSelf) hiden_3_.SetActive(false);
				}
			}
			else
			{
				hiden_2_.transform.parent = box_[index_].transform;
				Vector3 pos1 = box_[index_].transform.GetChild(0).transform.position;
				pos1.z = 1.5f;
				hiden_2_.transform.position = pos1;
				if (!hiden_2_.activeSelf) hiden_2_.SetActive(true);
				if (index_ > 1)
				{
					hiden_1_.transform.parent = box_[index_ - 2].transform;
					pos1.x = box_[index_ - 2].transform.GetChild(0).transform.position.x;
					hiden_1_.transform.position = pos1;
					if (!hiden_1_.activeSelf) hiden_1_.SetActive(true);
				}
				else if (hiden_1_.activeSelf) hiden_1_.SetActive(false);
				if (hiden_3_ != null)
				{
					if (box_.Count >= 3 && index_ < box_.Count - 1)
					{
						hiden_3_.transform.parent = box_[index_ + 1].transform;
						pos1.x = box_[index_ + 1].transform.GetChild(0).transform.position.x;
						hiden_3_.transform.position = pos1;
						if (!hiden_3_.activeSelf) hiden_3_.SetActive(true);
					}
					else if (hiden_3_.activeSelf) hiden_3_.SetActive(false);
				}
			}
			
		}
		private void SetHidenWithTk()
		{
			if (is_use_hiden_)//is_use_drag_)
			{
				SetButtonDirection();
				if (box_[index_].transform.position.x > main_panel_pos_.x)
				{
					//Right
					if (index_ > 0)
					{
						hiden_1_.transform.parent = box_[index_ - 1].transform;
						Vector3 pos1 = box_[index_ - 1].transform.GetChild(0).transform.position;
						pos1.z = 1.5f;
						hiden_1_.transform.position = pos1;
						if (!hiden_1_.activeSelf) hiden_1_.SetActive(true);
						if (index_ < box_.Count - 1)
						{
							hiden_2_.transform.parent = box_[index_ + 1].transform;
							pos1.x = box_[index_ + 1].transform.GetChild(0).transform.position.x;
							hiden_2_.transform.position = pos1;
							if (!hiden_2_.activeSelf) hiden_2_.SetActive(true);

						}
						else
						{
							if (hiden_2_.activeSelf) hiden_2_.SetActive(false);
						}
					}
				}
				else
				{
					if (index_ < box_.Count - 1)
					{
						hiden_1_.transform.parent = box_[index_ + 1].transform;
						Vector3 pos1 = box_[index_ + 1].transform.GetChild(0).transform.position;
						pos1.z = 1.5f;
						hiden_1_.transform.position = pos1;
						if (!hiden_1_.activeSelf) hiden_1_.SetActive(true);
						if (index_ > 0)
						{
							hiden_2_.transform.parent = box_[index_ - 1].transform;
							pos1.x = box_[index_ - 1].transform.GetChild(0).transform.position.x;
							hiden_2_.transform.position = pos1;
							if (!hiden_2_.activeSelf) hiden_2_.SetActive(true);
						}
						else
						{
							if (hiden_2_.activeSelf) hiden_2_.SetActive(false);
						}
					}

				}

			}
		}
		private bool IsCanSwitchLeft()
		{
			if (index_ == box_.Count - 1)
				return false;
			return true;
		}
		private bool IsCanSwitchRight()
		{
			if (index_ == 0)
				return false;
			return true;
		}
		
		public void Initialize()
		{
			GameObject temp;
			temp = CreateBox("Bazooka Soldier", SoldierData.level_bazooka_, SoldierData.price_bazooka_, "bazooka_stand1", "Bazooka_Idle");
			Vector3 pos = temp.transform.position;
			temp.transform.position = pos;
			pos.x += gap_;
			temp = CreateBox("Mine Thump", SoldierData.level_mine_, SoldierData.price_mine_, "Mine_Run1", "Miner_Run");
			temp.transform.position = pos;
		}
		private void InitializeHiden()
		{
			hiden_1_ = CreateHiden("HidenLeft");
			hiden_2_ = CreateHiden("HidenRight");
			Vector3 pos = hiden_1_.transform.position;
			pos.x = pos.x - gap_;
			pos.z = 1.5f;
			hiden_1_.transform.position = pos;
			hiden_1_.transform.parent = box_[1].transform;
			pos.x = pos.x + gap_ * 2;
			hiden_2_.transform.position = pos;
			if (hiden_1_.activeSelf) hiden_1_.SetActive(false);
			if (!hiden_2_.activeSelf) hiden_2_.SetActive(true);
			//if (box_.Count >= 3)
			//{
			//	hiden_3_ = CreateHiden("HidenFront");
			//	pos.x = pos.x + gap_;
			//	hiden_3_.transform.position = pos;
			//}
			hiden_box_prefab_ = null;
		}
		private GameObject CreateBox(string title, int level, int price, string spritename, string animationname)
		{
			GameObject temp = (GameObject)Instantiate(box_prefab_);
			BoxLevelControl box = temp.GetComponent<BoxLevelControl>();
			box.Initialize(this.gameObject, "ClickItem", title, level, price, spritename, animationname);
			item_.Add(box.Item);
			price_.Add(box.Price);
			bt_up_.Add(box.ButtonUp);
			up_level_pop_up_.Add(box.PopUp);
			box_.Add(temp);
			temp.transform.position = default_pos_;
			return temp;
		}
		private GameObject CreateHiden(string name)
		{
			GameObject temp;
			temp = (GameObject)Instantiate(hiden_box_prefab_);
			temp.name = name;
			temp.transform.position = box_[0].transform.GetChild(0).transform.position;
			return temp;
		}

		#region Using tk2D toolkit for scrolling

		public int OnScroll()
		{
			current_contant_pos_ = scroll_area_.contentContainer.transform.position;
			return CheckPanelFocus(); 
		}
		public void OnRelease()
		{
			is_release_ = true;
			//Invoke("ResetFucus", .1f);
		}
		public void OnClick()
		{
			star_contain_pos_ = scroll_area_.contentContainer.transform.position;
			is_check_speed_ = true;
		}
		public void ResetFucus(int index)
		{
			index_ = index;
			ResetFucus();
		}
		public void ScrollEnable(bool isenable)
		{
			if (scroll_area_.allowSwipeScrolling != isenable)
			{
				scroll_area_.allowSwipeScrolling = isenable;
			}
		}
		private void ResetFucus()
		{
			if (!is_set_focus_)
			{
				float tarx;
				bool isreset = false;
				target_contant_pos_ = scroll_area_.contentContainer.transform.position;
				if (box_[index_].transform.position.x > main_panel_pos_.x )//&& index_!=0)
				{
					//Right
					tarx = box_[index_].transform.position.x - main_panel_pos_.x;
					target_contant_pos_.x -= tarx;
					isreset = true;
				}
				else// if (box_[index_].transform.position.x < main_panel_pos_.x )//&& index_!=box_.Count)
				{
					tarx = main_panel_pos_.x - box_[index_].transform.position.x;
					target_contant_pos_.x += tarx;
					isreset = true;	
				}
				if (isreset)
				{
					scroll_area_.StopAllCoroutines();
					scroll_area_.enabled = false;
					is_set_focus_ = true;
				}
				else
				{
					is_check_speed_ = false;
					is_release_ = false;
					if (!scroll_area_.enabled)
						scroll_area_.enabled = true;	
				}
			}
		}
		private int CheckPanelFocus()
		{
			if(box_.Count>0)
			{
				float distand = Vector2.Distance(box_[0].transform.position, main_panel_pos_);
				float distand2;
				int selectindex = 0;
				for (int i = 1; i < box_.Count; i++)
				{
					distand2 = Vector2.Distance(box_[i].transform.position, main_panel_pos_);
					//Debug.Log("Distand " + distand + " Dis2 " + distand2+" box "+ i);
					if (distand2 < distand)
					{
						selectindex = i;
						distand = distand2;
					}
				}
				if (index_ != selectindex)
				{
					index_ = selectindex;
					//hiden_1_.transform.position = box_[index_].transform.position;
					//CheckToSetHiden();	
					SetHidenWithTk();
					return index_;
				}
			}
			return index_;
		}
		private void CheckToSetHiden()
		{
			if (box_[index_].transform.position.x > main_panel_pos_.x)
			{
				//Right
				SetHiden(true);
			}
			else// if (index_ != box_.Count - 1 && box_[index_].transform.position.x < main_panel_pos_.x)
			{
				SetHiden(false);
			}
		}
		#endregion

		#region Variablse
		public List<GameObject> box_;
		//public bool is_move_swap_ = false;
		[HideInInspector]
		public int index_ = 0;
		[SerializeField]
		private float gap_ = 20;
		[SerializeField]
		private GameObject bt_next_ = null;
		[SerializeField]
		private GameObject bt_pre_ = null;
		[SerializeField]
		private float swipe_speed_ = 150;
		[SerializeField]
		private GameObject box_prefab_ = null;
		[SerializeField]
		private Vector3 default_pos_ = Vector3.zero;
		[SerializeField]
		private GameObject hiden_box_prefab_ = null;
		private GameObject hiden_2_ = null;
		private GameObject hiden_1_ = null;
		private GameObject hiden_3_ = null;
		private Vector3[] swap_posi_ = null;
		private List<GameObject> item_ = null;
		private List<TextMesh> price_ = null;
		private List<GameObject> up_level_pop_up_ = null;
		private List<GameObject> bt_up_ = null;

		[SerializeField]
		private tk2dUIScrollableArea scroll_area_ = null;
		private bool is_set_focus_ = false;
		private Vector3 star_contain_pos_;
		private bool is_check_speed_=false;
		private Vector3 main_panel_pos_;
		private Vector3 target_contant_pos_;
		private Vector3 current_contant_pos_;
		private bool is_release_ = false;
		[SerializeField]
		private bool is_use_drag_ = true;
		[SerializeField]
		private float visible_length_ = 2.1f;
		[SerializeField]
		private float length_per_unit_ = 2.42f;
		[SerializeField]
		private bool is_use_hiden_ = true;
		#endregion
	}
}
