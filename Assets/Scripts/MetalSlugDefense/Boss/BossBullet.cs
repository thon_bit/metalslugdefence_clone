﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class BossBullet : MonoBehaviour
    {
        void Awake()
        {
            origin_ = transform.localPosition;
        }
        // Use this for initialization
        void Start()
        {
           
        }
        void OnEnable()
        {
            Invoke("Refresh", life_time_);
			is_attack_ = true;
        }
        // Update is called once per frame
        void Update()
        {
           if (is_attack_)
			{
				transform.Translate(Vector3.left * Time.deltaTime * speed_);
				CheckSoldier();
			}
        }
        private void Refresh()
		{
			if (IsBigBullet())
			{					   
				fires_ = (GameObject)Instantiate(gameObject.transform.GetChild(0).gameObject);
				fires_.transform.position = gameObject.transform.position;
				fires_.transform.parent = gameObject.transform.parent.transform;
				if (!fires_.activeSelf) fires_.SetActive(true);
				fires_.GetComponent<tk2dSpriteAnimator>().Play("Fires_Shoot_Big");
				Destroy(fires_, .6f);
			}
           // RefreshPosition();
            //gameObject.SetActive(false);
			Destroy(gameObject);
			is_attack_ = false;
			//Invoke("Refresh", time_interwal_);
        }
        private void CheckSoldier()
        {
            
            foreach (GameObject g in MetalSlugMainGame.instand_.player_soldier_)
            {
                if (g != null && Vector3.Distance(g.transform.position, transform.position) <= 4f)
                {
                    SoldierControl sc = g.GetComponent<SoldierControl>();
                    if (IsBigBullet())
                        sc.RefreshLife(3);
                    else
                        sc.RefreshLife(1);
                    sc.IsDie();
                    Refresh();
                    return;
                }
            }
			if (!IsBigBullet())
			{
				GameObject home = MetalSlugMainGame.instand_.player_home_;
				if (home != null)
				{
					if (Vector3.Distance(transform.position, home.transform.position) <= 4f)
					{
						SoldierHome sh = home.GetComponent<SoldierHome>();
						sh.RefreshLife(1);
						if (sh.IsDie())
						{
							BossControl.instance.CompleteMission();
						}
						else
						{
							Refresh();
						}
						return;
					}
				}
			}
        }

		private bool IsBigBullet()
		{
			if (gameObject.transform.childCount > 0)
				return true;
			return false;
		}
        private void RefreshPosition()
        {
            transform.localPosition = origin_;
        }
        private Vector3 origin_ = Vector3.zero;
        [SerializeField]
        private float speed_ = 10f;
        [SerializeField]
        public float life_time_ = 3f;
		private GameObject fires_;
		private bool is_attack_ = false;
    }
}