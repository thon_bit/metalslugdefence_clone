﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class BossBase:SoldierHome
    {
        protected override void Die()
        {
            GameObject particle = (GameObject)Instantiate(Resources.Load("Particles/Fires"));// the fire of destroyed tower
            particle.transform.localScale = new Vector3(3, 3, 0);
			particle.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -5);
            particle.GetComponent<ParticleSystem>().startSize = 15;
			Destroy(particle, 1.6f); // Destroy the fire
            BossControl.instance.Dead();
        }
    }
}