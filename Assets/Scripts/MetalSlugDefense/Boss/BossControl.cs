﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace YNinja.MetalSlug
{
    public class BossControl : MonoBehaviour
    {

        void Awake()
        {
            instance = this;
        }
        // Use this for initialization
        void Start()
        {									  
            Invoke("AttackLaser", 5f);
        }
        // Update is called once per frame
        void Update()
        {
            if (ismoving)
            {
				transform.position = Vector3.MoveTowards(transform.position, new Vector3(19.8f, transform.position.y, transform.position.z), Time.deltaTime * speed_moving_);
				if (Vector3.Distance(transform.position, new Vector3(19.8f, transform.position.y, transform.position.z)) <= 0f)
                {
                    ismoving = false;
                }
            }
        }
		public void CompleteMission()
		{
			DisableFire();
			isdead_ = true;
			ismoving = false;
			MetalSlugMainGame.instand_.ShowDialog(Side.ENEMY);
		}
        private void AttackLaser()
        {
            if (!isdead_)
            {
				ShootBullet();
				Invoke("AttackLaser", time_interwal_);//5f);
				if(!fires_.activeSelf) fires_.SetActive(true);
				tk2dSpriteAnimator anim = fires_.GetComponent<tk2dSpriteAnimator>();
				anim.Play("Fires_Shoot_Big");
				anim = weapon_.GetComponent<tk2dSpriteAnimator>();
				anim.Play();
				Invoke("DisableFire", .6f);
            }
        }
		private void ShootBullet()
		{
			GenerateBullet(bullet1_);
			GenerateBullet(bullet2_);
			GenerateBullet(bullet3_);
		}
		private void GenerateBullet(GameObject tem)
		{
			GameObject bullet = (GameObject)Instantiate(tem);
			bullet.SetActive(true);
			bullet.transform.parent = tem.transform.parent.transform;
			bullet.transform.position = tem.transform.position;
		}
		
		private void DisableFire()
		{
			fires_.SetActive(false);
		}
        public void Dead()
        {
            isdead_ = true;
            chain_.SetActive(false);
            weapon_.SetActive(false);
            body_.GetComponent<tk2dSpriteAnimator>().Stop();
            body_.GetComponent<tk2dSprite>().spriteId = body_.GetComponent<tk2dSprite>().GetSpriteIdByName("boss_1_die");
       }
		#region Variable
		public static BossControl instance = null;
		[HideInInspector]
		public float time_interwal_;
        [SerializeField]
        private GameObject bullet1_ = null;
        [SerializeField]
        private GameObject bullet2_ = null;
        [SerializeField]
        private GameObject bullet3_ = null;
        [SerializeField]
        private GameObject body_ = null;
        [SerializeField]
        private GameObject chain_ = null;
        [SerializeField]
        private GameObject weapon_ = null;
		[SerializeField]
		private GameObject fires_ = null;
		private bool isdead_ = false;
		private bool ismoving = true;
		private float speed_moving_ = 4;
		#endregion
	}
}