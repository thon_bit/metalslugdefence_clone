﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public delegate void ClickEventHandler(Transform hit);
    public class ClickControl : MonoBehaviour
    {
        public static event ClickEventHandler OnClick = delegate { };

        void Update()
        {
            if (cam_ != null)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Transform hit = GeneralSystem.ClickOnObject(cam_);
                    OnClick(hit);
                }
            }
        }

        public static void Initialize(Camera cam)
        {
            if (instance_ == null)
            {
                GameObject temp = new GameObject("ClickEvent");
                instance_= temp.AddComponent<ClickControl>();
                instance_.cam_ = cam;
                Object.DontDestroyOnLoad(instance_);
            }
            else
            {
                instance_.cam_ = cam;
            }
        }
        private static ClickControl instance_ = null;
        private Camera cam_ = null;
    }
}