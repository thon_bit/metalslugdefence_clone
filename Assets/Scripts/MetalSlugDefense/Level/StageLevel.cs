﻿using UnityEngine;
using System.Collections;

public class StageLevel{

    public StageLevel(string[] type, int[] hp,float[] interval)
    {
        TypeSoldier = type;
        SoldierHp = hp;
        SoldierInterval = interval;

        IsBoss = false;
    }
    public StageLevel(bool boss, float bossinter,int bosshp)
    {
        BossInterval = bossinter;
        IsBoss = boss;
		BossHp = bosshp;
    }
    public float BossInterval { get; set; }
    public bool IsBoss { get; set; }
	public int BossHp { get; set; }
    
    public string[] TypeSoldier { get; set; }
    public int[] SoldierHp { get; set; }
    public float[] SoldierInterval { get; set; }
}
