﻿using UnityEngine;
using System.Collections;

namespace YNinja.MetalSlug
{
	public class LevelControl
	{
		public LevelControl(int area,int stage)
		{
			area_id_ = area;
			stage_id_ = stage;
			switch (area_id_)
			{
				case 0: Area1(); break;
				case 1: Area2(); break;
				case 2: Area3(); break;
				case 3: Area4(); break;
				case 4: Area5(); break;
				//case 4: break;
			}
		}

        public StageLevel EnemyDescription { get; set; }
		//public bool IsShowEnemy
		//{
		//	get { return is_show_enemy_; }
		//}
		//public float EnemyTimeInterwal
		//{
		//	get { return enemy_time_interwal_; }
		//}
		//public bool IsShowTank
		//{
		//	get { return is_show_tank_; }
		//}
		//public float TankTimeInterwal
		//{
		//	get { return tank_time_interwal_; }
		//}
		//public bool IsShowBoss
		//{
		//	get { return is_show_boss_; }
		//}
		//public float BossTimeInterwal
		//{
		//	get { return boss_time_interwal_; }
		//}

		//private void VeryEasy ()
		//{
		//	is_show_enemy_ = true;
		//	enemy_time_interwal_ = 5;
		//	is_show_tank_ = false;
		//}
		//private void Easy()
		//{
		//	is_show_enemy_ = true;
		//	enemy_time_interwal_ = 2.5f;
		//	is_show_tank_ = false;
		//}
		//private void Normal()
		//{
		//	is_show_enemy_ = false;
		//	is_show_tank_ = true;
		//	tank_time_interwal_ = 10;
		//}
		//private void Hard()
		//{
		//	is_show_enemy_ = false;
		//	is_show_tank_ = true;
		//	tank_time_interwal_ = 5f;
		//}
		//private void Boss()
		//{
		//	is_show_enemy_ = false;
		//	is_show_tank_ = false;
		//	is_show_boss_ = true;
		//	boss_time_interwal_ = 5;
		//}

		private void Area1()
		{
			switch (stage_id_)
			{ 
				case 0:
					//VeryEasy();
					EnemyDescription=new StageLevel(new string[]{"Soldier","Scientist"},new int[]{2,2},new float[]{15,7});
                    break;
				case 1:
					//Easy();
                    EnemyDescription = new StageLevel(new string[] { "Soldier","Fio","Scientist" }, new int[] {1,3,1}, new float[] { 10,18,13 });
					break;
				case 2:
					//Normal();
                    EnemyDescription = new StageLevel(new string[] { "Soldier","Tank","Licker" }, new int[] { 1,3,2 }, new float[] {10,23,13  });
					break;
				case 3:
					//Hard();
                    EnemyDescription = new StageLevel(new string[] { "Fio","Tarma","Tank","Licker" }, new int[] { 2,2,3,2 }, new float[] { 7,9,12,15 });
					break;
			}
		}
        private void Area2()
		{
			switch (stage_id_)
			{
				case 0:
					EnemyDescription = new StageLevel(true, 8,10);
					break;
				case 1:
					EnemyDescription = new StageLevel(true, 6,12);
					break;
				case 2:
					EnemyDescription = new StageLevel(true, 4,15);
					break;
				case 3:
					//Boss();
                    EnemyDescription = new StageLevel(true, 3,20);
					break;
			}
		}
		private void Area3()
		{
			switch (stage_id_)
			{
				case 0:
                    //VeryEasy();
                    //is_show_tank_ = true;
                    //tank_time_interwal_ = 12;
                    EnemyDescription = new StageLevel(new string[] { "Eri","Tarma","Tank","Licker" }, new int[] { 3,3,5,2 }, new float[] { 8,10,21,13 });
					break;
				case 1:
                    //Easy();
                    //is_show_tank_ = true;
                    //tank_time_interwal_ = 8;
                    EnemyDescription = new StageLevel(new string[] { "Marco","Fio","Tank","Sarubia","Scientist","Licker" }, new int[] { 2,2,3,5,2,2}, new float[] { 8,6,13,18,15,21 });
					break;
				case 2:
                    EnemyDescription = new StageLevel(true, 5,20);
					break;
				case 3:
                    EnemyDescription = new StageLevel(true, 2.5f,50);
					break;
			}
		}

		private void Area4()
		{
			switch (stage_id_)
			{
				case 0:
					//VeryEasy();
					EnemyDescription = new StageLevel(new string[] { "Fio", "Scientist","Tarma","YoungGirlGreen" }, new int[] { 2, 2,3,2 }, new float[] { 15, 7,9,11 });
					break;
				case 1:
					//Easy();
					EnemyDescription = new StageLevel(new string[] { "Licker","Tank","Marco","Salvaldor", "Fio", "Scientist" }, new int[] { 3, 3, 2,2,2,2 }, new float[] { 10, 18, 13,17,8.5f,7 });
					break;
				case 2:
					//Normal();
					EnemyDescription = new StageLevel(new string[] { "Arabian","Bazooka", "Berserker","SwordMan", "Licker" ,"Sarubia"}, new int[] { 4, 3, 2,2,3,5 }, new float[] { 10, 23, 13,15,18.5f,27 });
					break;
				case 3:
					//Hard
					EnemyDescription = new StageLevel(true, 3.5f, 70);
					break;
			}
		}

		private void Area5()
		{
			switch (stage_id_)
			{
				case 0:
					//VeryEasy();
					EnemyDescription = new StageLevel(new string[] { "Soldier", "Scientist","Licker","Salvaldor","YoungGirl","Sarubia" }, new int[] { 2, 2,2,2,2,3 }, new float[] { 15, 7,9,19,11,24 });
					break;
				case 1:
					//Easy();
					EnemyDescription = new StageLevel(new string[] { "Arabian", "Fio", "Scientist","Eri","Marco","SwordMan","Berserker" }, new int[] { 2, 3, 3,3,3,3,3 }, new float[] { 10, 18, 13,21,25,8.5f,16 });
					break;
				case 2:
					//Normal();
					EnemyDescription = new StageLevel(new string[] { "Bazooka", "Tank", "Wolf", "YoungGirl", "YoungGirlGreen", "Shield", "SwordMan", "Mummy", "Arabian", "Berserker", "Licker", "Sarubia" }, new int[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4 }, new float[] { 10, 23, 13, 15, 30, 18, 33, 35, 40, 45, 11.5f, 50 });
					break;
				case 3:
					//Hard
					EnemyDescription = new StageLevel(true, 2.5f, 100);
					break;
			}
		}

		//private bool is_show_enemy_;
		//private float enemy_time_interwal_;
		//private bool is_show_tank_;
		//private float tank_time_interwal_;
		//private bool is_show_boss_;
		//private float boss_time_interwal_;

		private int area_id_;
		private int stage_id_;
	}
}

public enum LevelType
{ 
	VeryEasy,
	Easy,
	Normal,
	Hard
}