﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class Sarubia : SoldierType
    {
        public Sarubia(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
		public Sarubia(bool isenemy = true)
        {
            if (!isenemy)
            {
                this.health_ = SoldierData.solider_hp_;
            }
            else
            {
                this.health_ = 2;
            }
            this.isenemy = isenemy;
            this.damage_ = 2;
            this.distance_attack_ = 15;
            this.animation_move_ = "Sarubia_move";
            this.animation_die_ = "Sarubia_die";
            this.animation_attack_ = "Sarubia_shoot";
			this.animation_win_ = "Sarubia_idle";
			this.animation_skill = "Sarubia_idle";
            this.animation_idle = "Sarubia_idle";
			this.sprit_name_ = "tank_sarubia_idle_1";
            this.box_title_ = "Sarubia Tank";
			this.speed_ = 1.8f;
			this.price_ = SoldierData.price_soldier_;
			this.level_ = SoldierData.level_soldier_;
            this.btn_sprite_ = "bt_green_sarubai";
			this.bt_green_ = "bt_green_sarubai";
            this.bt_red_ = "bt_red_sarubai";
			this.time_interwal_ = 1.5f;
			this.btn_value_ = 90;
        }
        public override float Health
        {
            get
            {
                if (!this.isenemy)
                {
                    //if (DataSaving.HasData())
                    //{	
                    //    return DataSaving.LoadArmyHp()[0];
                    //}
                    if (DataSavingXml.HasData())
                    {
						return DataSavingXml.Instance().army.hp[8];
                    }
                }
                return base.Health;
            }
			set
			{
				if (!this.isenemy)
				{
					//if (DataSaving.HasData())
					//{
					//    float[] data = DataSaving.LoadArmyHp();
					//    data[0] = value;
					//    DataSaving.SaveArmyHp(data);
					//}
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						float[] hp = data.army.hp;
						hp[8] = value;
						data.army.hp = hp;
						data.SaveData();
					}
				}
				base.Health = value;
			}
		}
		public override int Level
		{
			get
			{
				//if (!this.isenemy)
				//{
				//    if (DataSaving.HasData())
				//    {
				//        return DataSaving.LoadArmyLevel()[0];
				//    }
				//}
				if (DataSavingXml.HasData())
				{
					return DataSavingXml.Instance().army.level[8];
				}
				return base.Level;
			}
			set
			{
				if (!this.isenemy)
				{
					//    if (DataSaving.HasData())
					//    {
					//        int [] data = DataSaving.LoadArmyLevel();
					//        data[0] = value;
					//        DataSaving.SaveArmyLevel(data);
					//    }
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						int[] lv = data.army.level;
						lv[8] = value;
						data.army.level = lv;
						data.SaveData();
					}
				}

				base.Level = value;
			}
		}
        public override int Price
        {
            get
            {
                float price_temp = DataSavingXml.Instance().army.price[8];
                for (int i = 0; i < Level; i++)
                {
                    price_temp += (price_temp * .25f);
                }
                price_ = Mathf.RoundToInt(price_temp);
                return price_;
            }
        }
        public override int ShopPrice
        {
            get { return DataSavingXml.Instance().army.price[8]; }
        }
		public override void Shoot(GameObject target, bool isskill)
		{
			GameObject bullet;
			Vector3 pos;
			bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/bullet_sarubia"));
			//bullet.transform.parent = target.transform;
			if(isenemy)
				pos = new Vector3(target.transform.position.x - 3.2f, target.transform.position.y + 2.5f, target.transform.position.z + 1);
			else
				pos = new Vector3(target.transform.position.x+3.2f, target.transform.position.y+2.5f, target.transform.position.z + 1);
			bullet.transform.position = pos;
			bullet.SetActive(false);
			bullet.GetComponent<BulletShoot>().SarubiaShoot(target.GetComponent<SoldierControl>());
			bullet.GetComponent<tk2dSpriteAnimator>().Play();
			bullet.GetComponent<tk2dSprite>().scale = new Vector3(-8, 8, 0);
			//fire
			bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/fire_sarubia"));
			pos.z = pos.z - 1;
			bullet.transform.position = pos;
			MonoBehaviour.Destroy(bullet, .6f);
		}

		public override void Skill(GameObject target)
		{

		}

        public override AtkType AttackType
        {
            get { return AtkType.RANGE; }
        }
		public override bool IsHasSkill
		{
			get { return false; }
		}
    }
}
