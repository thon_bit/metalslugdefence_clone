﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class Tank : SoldierType
    {
        public Tank(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
        public Tank(bool isenemy=false)
        {
            if (isenemy)
            {
                this.damage_ = 1;
            }
            else
            {
                this.damage_ = 2;
            }
            this.health_ = 4;
            
			this.distance_attack_ = 11f;//14;
            this.animation_move_ = "Tank_Move";
            this.animation_die_ = "Tank_Die";
            this.animation_attack_ = "Tank_Attack";
			this.animation_win_ = "Tank_Move";
			this.time_interwal_ = 1;
			this.speed_ = 2.9f;
			this.btn_value_ = 80;
        }
        public override int ShopPrice
        {
            get { return 0; }
        }
        public override void Shoot(GameObject target, bool isskill)
        {
            GameObject bullet;
            bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/bullet_tank"));
            bullet.transform.parent = target.transform;
            bullet.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + 1);
            bullet.GetComponent<BulletShoot>().Shoot(target.GetComponent<SoldierControl>());
        
        }

        public override void Skill(GameObject target)
        {
            
        }

        public override AtkType AttackType
        {
            get { return AtkType.RANGE; }
        }
		public override bool IsHasSkill
		{
			get { return false; }
		}
    }
}