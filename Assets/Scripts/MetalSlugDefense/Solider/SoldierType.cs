﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{

    public enum AtkType
    {
        MELEE,
        RANGE
    }
    public abstract class SoldierType
    {
        public virtual float Health
        {
            get
            {
                return health_;
            }
            set
            {
                health_ = value;
            }
        }
        public float Damage
        {
            get
            {
                return damage_;
            }
        }
        public string MoveAnimation
        {
            get
            {
                return animation_move_;
            }
        }
        public string DieAnimation
        {
            get
            {
                return animation_die_;
            }
        }
        public string AttackAnimation
        {
            get
            {
                return animation_attack_;
            }
        }
        public float AttackDistance
        {
            get
            {
                return distance_attack_;
            }
        }
        public string WinAnimation
        {
            get
            {
                return animation_win_;
            }
        }
        public float Speed
        {
            get { return speed_; }
        }
        public virtual int Level
        {
            get { return level_; }
            set { level_ = value; }
        }
        public virtual int Price
        {
            get
            {
                //Debug.Log(Level);
                return price_;
            }
        }
		public string SkillAnimation
		{
			get { return animation_skill; }
		}
		public string SpritName
		{
			get { return sprit_name_; }
		}
        public string BtnSprite
        {
            get { return btn_sprite_; }
        }
		public string BtnGreen
		{
			get { return bt_green_; }
		}
		public string BtnRed
		{
			get { return bt_red_; }
		}
        public string IdleAnimation
        {
            get { return animation_idle; }
        }
        public string BoxTitle
        {
            get { return box_title_; }
        }
		public float SkillDemage
		{
			get { return skill_damage_; }
		}
		public float TimeInterwal
		{
			get { return time_interwal_; }
		}
		public int BtnValue
		{
			get { return btn_value_; }
		}
        public static SoldierType GetType(string name,bool isenemy=false)
        {
            switch (name)
            {
                case "Soldier": return new Soldier(isenemy);
				case "Bazooka": return new Bazooka(isenemy);
				case "Miner": return new Miner(isenemy);
				case "Marco": return new Marco(isenemy);
				case "Tarma": return new Tarma(isenemy);
				case "Fio": return new Fio(isenemy);
				case "Eri": return new Eri(isenemy);
				case "Shield": return new SoldierShield(isenemy);
				case "Sarubia": return new Sarubia(isenemy);
                case "Berserker": return new Berserker(isenemy);
                case "Arabian": return new Arabian(isenemy);
				case "Mummy": return new Mummy(isenemy);
				case "SwordMan": return new SwordMan(isenemy);
                case "YoungGirl": return new YoungGirl(isenemy);
                case "Licker": return new Licker(isenemy);
                case "Wolf": return new Wolf(isenemy);
                case "Scientist": return new Scientist(isenemy);
				case "YoungGirlGreen": return new YoungGirlGreen(isenemy);
				case "Salvaldor": return new Salvaldor(isenemy);
            }
            return null;
        }
        public abstract int ShopPrice { get;}
        public abstract AtkType AttackType { get; }
		public abstract bool IsHasSkill { get; }
        public abstract void Shoot(GameObject target,bool isskill);
        public abstract void Skill(GameObject target);
		#region Variables
        protected string box_title_;
		protected float health_;
        protected float damage_;
        protected float distance_attack_;
        protected string animation_move_;
        protected string animation_die_;
        protected string animation_attack_;
        protected string animation_win_;
		protected string animation_skill;
        protected string animation_idle;
		protected string sprit_name_;
        protected string btn_sprite_;
		protected string bt_green_;
		protected string bt_red_;
        protected float speed_;
        protected int price_;
        protected int level_;
        protected bool isenemy;
		protected float skill_damage_;
		protected float time_interwal_;
		protected int btn_value_;
		protected bool is_has_skill_;
		#endregion
	}

}