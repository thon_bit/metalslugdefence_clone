﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class Tarma : SoldierType
    {

        public Tarma(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
        public Tarma(bool isenemy = true)
        {
            if (!isenemy)
            {
                this.health_ = SoldierData.mine_hp_;
            }
            else
            {
                this.health_ = 2;
            }
            this.isenemy = isenemy;
            this.damage_ = 1;
			this.skill_damage_ = 7;
            this.distance_attack_ = 4;
            this.animation_move_ = "Tarma_Move";
            this.animation_die_ = "Tarma_Die";
            this.animation_attack_ =  "Tarma_Shoot";
            this.animation_win_ = "Tarma_Victory";
            this.sprit_name_ = "tarma_run1";
            this.bt_green_ = "bt_green_tarma";
            this.bt_red_ = "bt_red_tarma";
            this.animation_skill = "Tarma_Shoot_S";
            this.animation_idle = "Tarma_Idle";
            this.box_title_ = "Tarma";
            this.speed_ = 3;
            this.level_ = SoldierData.level_mine_;
            this.price_ = SoldierData.price_mine_;
			this.time_interwal_ = 1;
			this.btn_value_ = 45;
        }
        public override float Health
        {
            get
            {
                if (!this.isenemy)
                {
                    //if (DataSaving.HasData())
                    //{
                    //    return DataSaving.LoadArmyHp()[3];
                    //}
                    if (DataSavingXml.HasData())
                    {
                        return DataSavingXml.Instance().army.hp[4];
                    }
                }
                return base.Health;
            }
			set
			{
				if (!this.isenemy)
				{
					//if (DataSaving.HasData())
					//{
					//    float[] data = DataSaving.LoadArmyHp();
					//    data[0] = value;
					//    DataSaving.SaveArmyHp(data);
					//}
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						float[] hp = data.army.hp;
						hp[4] = value;
						data.army.hp = hp;
						data.SaveData();
					}
				}
				base.Health = value;
			}
		}
		public override int Level
		{
			get
			{
				//if (!this.isenemy)
				//{
				//    if (DataSaving.HasData())
				//    {
				//        return DataSaving.LoadArmyLevel()[0];
				//    }
				//}
				if (DataSavingXml.HasData())
				{
					return DataSavingXml.Instance().army.level[4];
				}
				return base.Level;
			}
			set
			{
				if (!this.isenemy)
				{
					//    if (DataSaving.HasData())
					//    {
					//        int [] data = DataSaving.LoadArmyLevel();
					//        data[0] = value;
					//        DataSaving.SaveArmyLevel(data);
					//    }
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						int[] lv = data.army.level;
						lv[4] = value;
						data.army.level = lv;
						data.SaveData();
					}
				}

				base.Level = value;
			}
		}
        public override int Price
        {
            get
            {
                float price_temp = DataSavingXml.Instance().army.price[4];
                for (int i = 0; i < Level; i++)
                {
                    price_temp += (price_temp * .25f);
                }
                price_ = Mathf.RoundToInt(price_temp);
                return price_;
            }
        }
        public override void Shoot(GameObject target, bool isskill)
        {
            GameObject bullet;
            Vector3 pos;
            if (!isskill)
            {
                bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/bullet_marco"));
                pos = new Vector3(target.transform.position.x, target.transform.position.y - .5f, target.transform.position.z + 1);
                bullet.transform.parent = target.transform;
                bullet.transform.position = pos;
                bullet.GetComponent<BulletShoot>().MarcoShot(target.GetComponent<SoldierControl>(), isskill);
            }
            else
            {
                bullet = target.GetComponent<SoldierControl>().GenerateBullet("bullet_tarma_s");
                bullet.GetComponent<BulletShoot>().TarmaShootS(target.GetComponent<SoldierControl>());
            }
        }

        public override void Skill(GameObject target)
        {
            Shoot(target, true);
        }



        public override int ShopPrice
        {
            get { return DataSavingXml.Instance().army.price[4]; }
        }

        public override AtkType AttackType
        {
            get {return AtkType.RANGE; }
        }
		public override bool IsHasSkill
		{
			get { return true; }
		}
    }
}