﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class SoldierShield : SoldierType
    {
        public SoldierShield(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
        public SoldierShield(bool isenemy = true)
        {
            if (!isenemy)
            {
                this.health_ = SoldierData.solider_hp_;
            }
            else
            {
                this.health_ = 2;
            }
            this.isenemy = isenemy;
            this.damage_ = 1;
            this.distance_attack_ = 4;
            this.animation_move_ = "Shield_Move";
            this.animation_die_ = "Soldier_Die";
            this.animation_attack_ = "Shield_Attack";
            this.animation_win_ = "Soldier_Win";
            this.animation_skill = "Soldier_Boom";
            this.animation_idle = "Shield_Idle";
            this.sprit_name_ = "shield_idle1";
            this.box_title_ = "Soldier Shield";
            this.speed_ = 3;
            this.price_ = SoldierData.price_soldier_;
            this.level_ = SoldierData.level_soldier_;
            this.btn_sprite_ = "bt_green_shield";
            this.bt_green_ = "bt_green_shield";
            this.bt_red_ = "bt_red_shield";
			this.time_interwal_ = 1;
			this.btn_value_ = 35;
		}
        public override float Health
        {
            get
            {
                if (!this.isenemy)
                {
                    //if (DataSaving.HasData())
                    //{	
                    //    return DataSaving.LoadArmyHp()[0];
                    //}
                    if (DataSavingXml.HasData())
                    {
                        return DataSavingXml.Instance().army.hp[7];
                    }
                }
                return base.Health;
            }
			set
			{
				if (!this.isenemy)
				{
					//if (DataSaving.HasData())
					//{
					//    float[] data = DataSaving.LoadArmyHp();
					//    data[0] = value;
					//    DataSaving.SaveArmyHp(data);
					//}
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						float[] hp = data.army.hp;
						hp[7] = value;
						data.army.hp = hp;
						data.SaveData();
					}
				}
				base.Health = value;
			}
		}
		public override int Level
		{
			get
			{
				//if (!this.isenemy)
				//{
				//    if (DataSaving.HasData())
				//    {
				//        return DataSaving.LoadArmyLevel()[0];
				//    }
				//}
				if (DataSavingXml.HasData())
				{
					return DataSavingXml.Instance().army.level[7];
				}
				return base.Level;
			}
			set
			{
				if (!this.isenemy)
				{
					//    if (DataSaving.HasData())
					//    {
					//        int [] data = DataSaving.LoadArmyLevel();
					//        data[0] = value;
					//        DataSaving.SaveArmyLevel(data);
					//    }
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						int[] lv = data.army.level;
						lv[7] = value;
						data.army.level = lv;
						data.SaveData();
					}
				}

				base.Level = value;
			}
		}
        public override int Price
        {
            get
            {
                float price_temp = DataSavingXml.Instance().army.price[7];
                for (int i = 0; i < Level; i++)
                {
                    price_temp += (price_temp * .25f);
                }
                price_ = Mathf.RoundToInt(price_temp);
                return price_;
            }
        }
        public override int ShopPrice
        {
            get { return DataSavingXml.Instance().army.price[7]; }
        }
        public override void Shoot(GameObject target, bool isskill)
        {

        }

        public override void Skill(GameObject target)
        {
            GameObject bullet;
            bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/HandBoom"));
            bullet.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + 1);
            bullet.GetComponent<BulletShoot>().Shoot(target.GetComponent<SoldierControl>());
        }

        public override AtkType AttackType
        {
            get { return AtkType.MELEE; }
        }
		public override bool IsHasSkill
		{
			get { return true; }
		}
    }
}