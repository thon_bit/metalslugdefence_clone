﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class Fio : SoldierType
    {

        public Fio(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
        public Fio(bool isenemy = true)
        {
            if (!isenemy)
            {
                this.health_ = SoldierData.mine_hp_;
            }
            else
            {
                this.health_ = 2;
            }
            this.isenemy = isenemy;
            this.damage_ = 1;
			this.skill_damage_ = 3;
            this.distance_attack_ = 8;
            this.animation_move_ = "Fio_Move";
            this.animation_die_ = "Fio_Die";
            this.animation_attack_ = "Fio_Shoot";
            this.animation_win_ = "Fio_Victory";
            this.sprit_name_ = "fio_run1";
            this.bt_green_ = "bt_green_fio";
            this.bt_red_ = "bt_red_fio";
            this.animation_skill = "Fio_Shoot_S";
            this.animation_idle = "Fio_Idle";
            this.box_title_ = "Fio Lina";
            this.speed_ = 3.1f;
            this.level_ = SoldierData.level_mine_;
            this.price_ = SoldierData.price_mine_;
			this.time_interwal_ = 1;
			this.btn_value_ = 40;
        }
        public override float Health
        {
            get
            {
                if (!this.isenemy)
                {
                    //if (DataSaving.HasData())
                    //{
                    //    return DataSaving.LoadArmyHp()[3];
                    //}
                    if (DataSavingXml.HasData())
                    {
                        return DataSavingXml.Instance().army.hp[5];
                    }
                }
                return base.Health;
            }
			set
			{
				if (!this.isenemy)
				{
					//if (DataSaving.HasData())
					//{
					//    float[] data = DataSaving.LoadArmyHp();
					//    data[0] = value;
					//    DataSaving.SaveArmyHp(data);
					//}
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						float[] hp = data.army.hp;
						hp[5] = value;
						data.army.hp = hp;
						data.SaveData();
					}
				}
				base.Health = value;
			}
		}
		public override int Level
		{
			get
			{
				//if (!this.isenemy)
				//{
				//    if (DataSaving.HasData())
				//    {
				//        return DataSaving.LoadArmyLevel()[0];
				//    }
				//}
				if (DataSavingXml.HasData())
				{
					return DataSavingXml.Instance().army.level[5];
				}
				return base.Level;
			}
			set
			{
				if (!this.isenemy)
				{
					//    if (DataSaving.HasData())
					//    {
					//        int [] data = DataSaving.LoadArmyLevel();
					//        data[0] = value;
					//        DataSaving.SaveArmyLevel(data);
					//    }
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						int[] lv = data.army.level;
						lv[5] = value;
						data.army.level = lv;
						data.SaveData();
					}
				}

				base.Level = value;
			}
		}
        public override int Price
        {
            get
            {
                float price_temp = DataSavingXml.Instance().army.price[5];
                for (int i = 0; i < Level; i++)
                {
                    price_temp += (price_temp * .25f);
                }
                price_ = Mathf.RoundToInt(price_temp);
                return price_;
            }
        }
        public override int ShopPrice
        {
            get { return DataSavingXml.Instance().army.price[5]; }
        }
        public override void Shoot(GameObject target, bool isskill)
        {
            GameObject bullet;
            Vector3 pos;
            if (!isskill)
            {
                bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/bullet_marco"));
                pos = new Vector3(target.transform.position.x, target.transform.position.y - .5f, target.transform.position.z + 1);
                bullet.transform.parent = target.transform;
                bullet.transform.position = pos;
                bullet.GetComponent<BulletShoot>().MarcoShot(target.GetComponent<SoldierControl>(), isskill);
            }
            else
            {
                bullet = target.GetComponent<SoldierControl>().GenerateBullet("bullet_fio_s");
                bullet.GetComponent<BulletShoot>().FioShootS(target.GetComponent<SoldierControl>());
            }
        }

        public override void Skill(GameObject target)
        {
            Shoot(target, true);
        }

        public override AtkType AttackType
        {
            get { return AtkType.RANGE; }
        }
		public override bool IsHasSkill
		{
			get { return true; }
		}
    }
}