﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class Bazooka : SoldierType
    {
        public Bazooka(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
        public Bazooka(bool isenemy = true)
        {
            if (!isenemy)
            {
                this.health_ = SoldierData.bazooka_hp_;
            }
            else
            {
                this.health_ = 2;
            }
            this.isenemy = isenemy;
            this.damage_ = 1;
			this.skill_damage_ = 1;
            this.distance_attack_ = 10;
            this.animation_move_ = "Bazooka_Run";
            this.animation_die_ = "Soldier_Die";
            this.animation_attack_ = "Bazooka_Shot";
            this.animation_win_ = "Soldier_Win";
			this.animation_skill = "Bazooka_Shot";
            this.animation_idle = "Bazooka_Idle";
			this.sprit_name_ = "bazooka_stand1";
            this.box_title_ = "Bazooka Soldier";
			this.speed_ = 2.5f;
			this.price_ = SoldierData.price_bazooka_;
			this.level_ = SoldierData.level_bazooka_;
            this.btn_sprite_ = "bt_s2";
			this.bt_green_ = "bt_s2_a";
			this.bt_red_ = "bt_s2";
			this.time_interwal_ = 1;
			this.btn_value_ = 40;
        }
        public override float Health
        {
            get
            {
                if (!this.isenemy)
                {
                    //if (DataSaving.HasData())
                    //{
                    //    return DataSaving.LoadArmyHp()[1];
                    //}
                    if (DataSavingXml.HasData())
                    {
                        return DataSavingXml.Instance().army.hp[1];
                    }
                }
                return base.Health;
            }
			set
			{
				if (!this.isenemy)
				{
					//if (DataSaving.HasData())
					//{
					//    float[] data = DataSaving.LoadArmyHp();
					//    data[0] = value;
					//    DataSaving.SaveArmyHp(data);
					//}
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						float[] hp = data.army.hp;
						hp[1] = value;
						data.army.hp = hp;
						data.SaveData();
					}
				}
				base.Health = value;
			}
		}
		public override int Level
		{
			get
			{
				//if (!this.isenemy)
				//{
				//    if (DataSaving.HasData())
				//    {
				//        return DataSaving.LoadArmyLevel()[0];
				//    }
				//}
				if (DataSavingXml.HasData())
				{
					return DataSavingXml.Instance().army.level[1];
				}
				return base.Level;
			}
			set
			{
				if (!this.isenemy)
				{
					//    if (DataSaving.HasData())
					//    {
					//        int [] data = DataSaving.LoadArmyLevel();
					//        data[0] = value;
					//        DataSaving.SaveArmyLevel(data);
					//    }
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						int[] lv = data.army.level;
						lv[1] = value;
						data.army.level = lv;
						data.SaveData();
					}
				}

				base.Level = value;
			}
		}
        public override int Price
        {
            get
            {
                float price_temp = DataSavingXml.Instance().army.price[1];
                for (int i = 0; i < Level; i++)
                {
                    price_temp += (price_temp * .25f);
                }
                price_ = Mathf.RoundToInt(price_temp);
                return price_;
            }
        }
        public override int ShopPrice
        {
            get { return DataSavingXml.Instance().army.price[1]; }
        }
        public override void Shoot(GameObject target, bool isskill)
        {
            GameObject bullet;
            bullet = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/Bullets/bullet_tank"));
            bullet.transform.parent = target.transform;
            bullet.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + 1);
            bullet.GetComponent<BulletShoot>().BazookaShot(target.GetComponent<SoldierControl>());
        }
        public override void Skill(GameObject target)
        {
            Shoot(target, true);
        }

        public override AtkType AttackType
        {
            get { return AtkType.RANGE; }
        }
		public override bool IsHasSkill
		{
			get { return true; }
		}
    }
}