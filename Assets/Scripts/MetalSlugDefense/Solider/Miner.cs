﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class Miner : SoldierType
    {

        public Miner(float health, float damage, float distance, string move, string die, string attack)
        {
            this.health_ = health;
            this.damage_ = damage;
            this.distance_attack_ = distance;
            this.animation_move_ = move;
            this.animation_die_ = die;
            this.animation_attack_ = attack;
        }
        public Miner(bool isenemy = true)
        {
            if (!isenemy)
            {
                this.health_ = SoldierData.mine_hp_;
            }
            else
            {
                this.health_ = 2;
            }
            this.isenemy = isenemy;
            this.damage_ = 0;
            this.distance_attack_ = 7;
            this.animation_move_ = "Miner_Run";
            this.animation_die_ = "Wall_Destroy";
            this.animation_attack_ = "Miner_Thump";
            this.animation_win_ = "";
            this.animation_idle = "Miner_Run";
			this.sprit_name_ = "Miner_Run_4";
			this.bt_green_ = "bt_s3_a";
			this.bt_red_ = "bt_s3";
            this.box_title_ = "Miner";
			this.speed_ = 5;
			this.level_ = SoldierData.level_mine_;
			this.price_ = SoldierData.price_mine_;
            this.btn_sprite_ = "bt_red_miner";
			this.time_interwal_ = 1;
			this.btn_value_ = 20;
        }
        public override float Health
        {
            get
            {
                if (!this.isenemy)
                {
                    //if (DataSaving.HasData())
                    //{
                    //    return DataSaving.LoadArmyHp()[2];
                    //}
                    if (DataSavingXml.HasData())
                    {
                        return DataSavingXml.Instance().army.hp[2];
                    }
                }
                return base.Health;
            }
			set
			{
				if (!this.isenemy)
				{
					//if (DataSaving.HasData())
					//{
					//    float[] data = DataSaving.LoadArmyHp();
					//    data[0] = value;
					//    DataSaving.SaveArmyHp(data);
					//}
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						float[] hp = data.army.hp;
						hp[2] = value;
						data.army.hp = hp;
						data.SaveData();
					}
				}
				base.Health = value;
			}
		}
		public override int Level
		{
			get
			{
				//if (!this.isenemy)
				//{
				//    if (DataSaving.HasData())
				//    {
				//        return DataSaving.LoadArmyLevel()[0];
				//    }
				//}
				if (DataSavingXml.HasData())
				{
					return DataSavingXml.Instance().army.level[2];
				}
				return base.Level;
			}
			set
			{
				if (!this.isenemy)
				{
					//    if (DataSaving.HasData())
					//    {
					//        int [] data = DataSaving.LoadArmyLevel();
					//        data[0] = value;
					//        DataSaving.SaveArmyLevel(data);
					//    }
					if (DataSavingXml.HasData())
					{
						DataSavingXml data = DataSavingXml.Instance();
						int[] lv = data.army.level;
						lv[2] = value;
						data.army.level = lv;
						data.SaveData();
					}
				}

				base.Level = value;
			}
		}
        public override int Price
        {
            get
            {
                float price_temp = DataSavingXml.Instance().army.price[2];
                for (int i = 0; i < Level; i++)
                {
                    price_temp += (price_temp * .25f);
                }
                price_ = Mathf.RoundToInt(price_temp);
                return price_;
            }
        }
        public override int ShopPrice
        {
            get { return DataSavingXml.Instance().army.price[2]; }
        }
        public override void Shoot(GameObject target, bool isskill)
        {
			SoldierControl sc = target.GetComponent<SoldierControl>();
			sc.isstartmoving_ = false;
            MetalSlugMainGame.instand_.player_soldier_.Remove(target);
            sc.Invoke("ShowWall", .7f);
			sc.Health = this.Health;
			
        }

        public override void Skill(GameObject target)
        {
            
        }

        public override AtkType AttackType
        {
			get { return AtkType.RANGE; }
        }
		public override bool IsHasSkill
		{
			get { return false; }
		}
    }
}