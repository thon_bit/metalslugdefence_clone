﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public class SoldierData
    {
        public static int solider_hp_ = 2;
        public static int bazooka_hp_ = 2;
		public static int mine_hp_ = 2;

		public static int funds_ = 3000;
		
        public static int level_soldier_ = 1;
        public static int level_bazooka_ = 1;
		public static int level_mine_ = 1;
		
        public static int price_soldier_ = 600;
        public static int price_bazooka_ = 600;
		public static int price_mine_ = 600;
    }
}