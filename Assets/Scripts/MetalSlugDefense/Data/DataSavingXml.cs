﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Xml;


[XmlRoot("MetalSlugData")]
public class DataSavingXml{
    public int fund;
    public ArmyData army;
    public ShopData shop;
    public CustomizeData customize;
    public DeckOrderData order;
    [XmlArrayItem("Area")]
    public AreaData areas;
    
    private static DataSavingXml instace = null;
    private static DataSavingXml LoadData()
    {
        XmlSerializer xml = new XmlSerializer(typeof(DataSavingXml));
        return (DataSavingXml)xml.Deserialize(new StringReader(EncryptionData.Decrypt(File.ReadAllText(Application.streamingAssetsPath + "/Data/savedata.xml"))));
    }
    public void SaveData()
    {
        XmlSerializer xml = new XmlSerializer(typeof(DataSavingXml));
        StringWriter sw = new StringWriter();
        xml.Serialize(sw, this);
        File.WriteAllText(Application.streamingAssetsPath + "/Data/savedata.xml",EncryptionData.Encrypt( sw.ToString()));
        //AssetDatabase.Refresh();
    }
    private void SetData(DataSavingXml data)
    {
        this.fund = data.fund;
        this.shop = data.shop;
        this.customize = data.customize;
        this.army = data.army;
        this.order = data.order;
        this.areas = data.areas;
    }
    public static bool HasData()
    {
        if (File.Exists(Path.Combine(Application.streamingAssetsPath, "Data/savedata.xml")))
        {
            return true;
        }
        return false;
    }
    public static void DeleteAll()
    {
        if (File.Exists(Path.Combine(Application.streamingAssetsPath , "Data/savedata.xml")))
        {
            File.Delete(Path.Combine(Application.streamingAssetsPath, "Data/savedata.xml"));
        }
    }
    public static DataSavingXml Instance()
    {
        instace = DataSavingXml.LoadData();
        return instace;
    }
}

public class ShopData
{
    [XmlArrayItem("ArmyItem")]
    public bool[] army;
}
public class ArmyData
{
    [XmlArrayItem("HPSoldier")]
    public float[] hp;
    [XmlArrayItem("LevelSoldier")]
    public int[] level;
    [XmlArrayItem("PriceSoldier")]
    public int[] price;
}
public class CustomizeData
{
    [XmlArrayItem("SoldierItem")]
    public bool[] items;
}
public class DeckOrderData
{
    [XmlArrayItem("SoldierOrder")]
    public string[] orders;
    [XmlArrayItem("SoldierRemain")]
    public string[] remains;
}
public class AreaData
{
    [XmlArrayItem("Stage")]
    public StageData[] area;
}
public class StageData
{
    public bool isnewarea;
    [XmlArrayItem("ClearStage")]
    public bool[] savedstage;
    [XmlArrayItem("NewStage")]
    public bool[] newstage;
}