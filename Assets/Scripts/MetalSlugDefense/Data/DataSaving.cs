﻿using UnityEngine;
using System.Collections;
namespace YNinja.MetalSlug
{
    public static class DataSaving
    {
        public static void SaveFund(int value)
        {
            PlayerPrefsX.SetIntArray("MetalFund", value);
        }
        public static int LoadFund()
        {
            return PlayerPrefsX.GetIntArray("MetalFund")[0];
        }
        public static void SaveShopingArmy(params bool[] army)
        {
            PlayerPrefsX.SetBoolArray("MetalShoping", army);
        }
        public static bool[] LoadShopingArmy()
        {
            return PlayerPrefsX.GetBoolArray("MetalShoping");
        }
        public static void SaveCustomize(params bool[] item)
        {
            PlayerPrefsX.SetBoolArray("MetalCustomize", item);
        }
        public static bool[] LoadCustomize()
        {
            return PlayerPrefsX.GetBoolArray("MetalCustomize");
        }

        public static void SaveArmyLevel(params int[] level)
        {
            PlayerPrefsX.SetIntArray("MetalLevel", level);
        }
        public static int[] LoadArmyLevel()
        {
            return PlayerPrefsX.GetIntArray("MetalLevel");
        }

        public static void SaveArmyHp(params float[] hp)
        {
            PlayerPrefsX.SetFloatArray("MetalHp", hp);
        }
        public static float[] LoadArmyHp()
        {
			return PlayerPrefsX.GetFloatArray("MetalHp");
        }

        public static void SaveOrder(params string[] soldier)
        {
            PlayerPrefsX.SetStringArray("MetalOrder", soldier);
        }
        public static string[] LoadOrder()
        {
            return PlayerPrefsX.GetStringArray("MetalOrder");
        }
		public static void SaveAllSoldier(string[] soldier)
		{
			PlayerPrefsX.SetStringArray("MetalAllSoldier", soldier);
		}
		public static string[] LoadAllSoldier()
		{
			return PlayerPrefsX.GetStringArray("MetalAllSoldier");	
		}
		public static void SaveRemainSoldier(string[] soldier)
		{
			PlayerPrefsX.SetStringArray("MetalRemainSoldier", soldier);			
		}
		public static string[] LoadRemainSoldier()
		{
			return PlayerPrefsX.GetStringArray("MetalRemainSoldier");
		}
		public static void SaveStage1(params bool[] stage)
		{
			PlayerPrefsX.SetBoolArray("MetalStage1", stage);
		}
		public static bool[] LoadStage1()
		{
			return PlayerPrefsX.GetBoolArray("MetalStage1");
		}
		public static void SaveStage2(params bool[] stage)
		{
			PlayerPrefsX.SetBoolArray("MetalStage2", stage);
		}
		public static bool[] LoadStage2()
		{
			return PlayerPrefsX.GetBoolArray("MetalStage2");
		}

        public static void SaveNewStage2(params bool[] stage)
        {
            PlayerPrefsX.SetBoolArray("MetalNewStage2", stage);
        }
        public static bool[] LoadNewStage2()
        {
            return PlayerPrefsX.GetBoolArray("MetalNewStage2");
        }
        public static void SaveNewStage1(params bool[] stage)
        {
            PlayerPrefsX.SetBoolArray("MetalNewStage1", stage);
        }
        public static bool[] LoadNewStage1()
        {
            return PlayerPrefsX.GetBoolArray("MetalNewStage1");
        }

        public static bool HasData()
        {
            return PlayerPrefs.HasKey("MetalFund");
        }

    }
}